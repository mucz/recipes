// Acceptor.hh
// Acceptor 负责接收新的连接
#ifndef ACCEPTOR_HH
#define ACCEPTOR_HH
#include "../Reactor/Channel.hh"
#include "Socket.hh"

class Acceptor : noncopyable{
public:
    typedef std::function<void(int sockfd,const InetAddress&)> NewConnectionCallback;
    Acceptor(EventLoop* loop,const InetAddress& listenAddr);

    void setNewConnectionCallback(const NewConnectionCallback& cb){ newConnectionCallback_ = cb; }
    bool listenning() const {return listenning_; }

    void listen();

private:
    void handleRead();

    EventLoop* loop_;
    Socket acceptSocket_;
    Channel acceptChannel_;
    NewConnectionCallback newConnectionCallback_;
    bool listenning_;
};

#endif //ACCEPTOR_HH
