#include "Callbacks.hh"

void defaultConnectionCallback(const TcpConnectionPtr& conn){
    (void) conn;
}

void defaultMessageCallback(const TcpConnectionPtr&,Buffer* buf,Timestamp receiveTime){
    buf->retrieveAll();
}