// TcpConnection.cc
#include <assert.h>
#include "TcpConnection.hh"
#include "Socket.hh"
#include "../Reactor/EventLoop.hh"
#include "../Reactor/Channel.hh"
TcpConnection::TcpConnection(EventLoop* loop,
                  const std::string& name,
                  int sockfd, // 哪里来的？
                  const InetAddress& localAddr,
                  const InetAddress& peerAddr)
    :   loop_(loop),
        name_(name),
        state_(kConnecting),
        socket_(std::make_unique<Socket>(sockfd)),
        channel_(std::make_unique<Channel>(loop,sockfd)),
        localAddr_(localAddr),
        peerAddr_(peerAddr)
    {
        // fprintf(stdout,"TcpConnection::ctor %s at %p fd = %d\n",name_.c_str(),this,sockfd);
        channel_->setReadCallback(std::bind(&TcpConnection::handleRead,this,std::placeholders::_1));
        channel_->setWriteCallback(std::bind(&TcpConnection::handleWrite,this));
        channel_->setCloseCallback(std::bind(&TcpConnection::handleClose,this));
        channel_->setErrorCallback(std::bind(&TcpConnection::handleError,this));
    }

TcpConnection::~TcpConnection(){
    fprintf(stdout,"TcpConnection::dtor %s at %p fd = %d",name_.c_str(),this,channel_->fd());
}


void TcpConnection::send(Buffer& buf){
  if(state_ == kConnected){
    if(loop_->isInLoopThread()){
      send(buf.peek(),buf.readableBytes());
      buf.retrieveAll();
    } else {
      loop_->runInLoop(std::bind(&TcpConnection::sendInLoop,this,buf.retrieveAllAsString()));
    }
  }
}

void TcpConnection::send(Buffer* buf){
  if(state_ == kConnected){
    if(loop_->isInLoopThread()){
      send(buf->peek(),buf->readableBytes());
      buf->retrieveAll();
    } else {
      loop_->runInLoop(std::bind(&TcpConnection::sendInLoop,this,buf->retrieveAllAsString()));
    }
  }
}

void TcpConnection::send(const void* data, int len){ 
  send(std::string_view(static_cast<const char*>(data),len));
}

void TcpConnection::send(const std::string_view& message){
    if(state_ == kConnected){
        if(loop_->isInLoopThread()){
            sendInLoop(std::string(message));
        } else {
            loop_->runInLoop(std::bind(&TcpConnection::sendInLoop,this,std::string(message)));
        }
    }
}

void TcpConnection::sendInLoop(const std::string& message){
    loop_->assertInLoopThread();
    ssize_t nwrote = 0;
    if(!channel_->isWriting() && outputBuffer_.empty()){ // nothing in ouput queue, try writing directly, avoid using buffer
        nwrote = ::write(channel_->fd(),message.data(),message.size());
        if(nwrote >= 0){
            if(static_cast<size_t>(nwrote) < message.size()){
                // fprintf(stdout,"TcpConnection::sendInLoop: write more data\n"); // data not fully writter, the rest will be appended to outputBuffer
            } else if(writeCompleteCallback_){
                loop_->queueInLoop(std::bind(writeCompleteCallback_,shared_from_this()));
            }
        } else { // nwrote < 0 -> error
            nwrote = 0;
            if(errno != EWOULDBLOCK){
                fprintf(stderr,"TcpConnection::senInLoop nwrote < 0\n");
                abort();
            }
        }
    }
    assert(nwrote>=0 && nwrote <= static_cast<ssize_t>(message.size()));
    if(static_cast<size_t>(nwrote) < message.size()){
        size_t oldLen = outputBuffer_.readableBytes();
        if(highWaterMarkCallback_ && oldLen < highWaterMark_ && oldLen + (message.size()-nwrote) >= highWaterMark_){
          loop_->queueInLoop(std::bind(highWaterMarkCallback_,shared_from_this(),oldLen + (message.size()-nwrote)));
        }
        outputBuffer_.append(message.data()+nwrote,message.size()-nwrote);
        if(!channel_->isWriting()){
            channel_->enableWriting();
        }
    }
}

void TcpConnection::shutdown(){
    if(state_ == kConnected){
        setState(kDisconnecting);
        loop_->runInLoop(std::bind(&TcpConnection::shutdownInLoop,this));
    }
}

void TcpConnection::shutdownInLoop(){
    loop_->assertInLoopThread();
    if(!channel_->isWriting()){
        socket_->shutdownWrite();
    }
}

void TcpConnection::setTcpNoDelay(bool on){
    socket_->setTcpNoDelay(on);
}

void TcpConnection::connectEstablished(){
    loop_->assertInLoopThread();
    assert(state_ == kConnecting);
    setState(kConnected);
    channel_->enableReading();
    connectionCallback_(shared_from_this());
}

void TcpConnection::connectDestroyed(){
    loop_->assertInLoopThread();
    assert(state_ == kDisconnecting || state_ ==kConnected);
    setState(kDisconnected);
    channel_->disableAll();
    connectionCallback_(shared_from_this()); // 在销毁时也会进行回调
    loop_->removeChannel(channel_.get());
}


void TcpConnection::handleRead(Timestamp receiveTime)
{
  int savedErrno = 0;
  ssize_t n = inputBuffer_.readFd(channel_->fd(), &savedErrno);
  if (n > 0) {
    messageCallback_(shared_from_this(), &inputBuffer_, receiveTime);
  } else if (n == 0) {
    handleClose();
  } else {
    errno = savedErrno;
    fprintf(stderr,"TcpConnection::handleRead\n");
    handleError();
  }
}

void TcpConnection::handleWrite()
{
  loop_->assertInLoopThread();
  if (channel_->isWriting()) {
    ssize_t n = ::write(channel_->fd(),
                        outputBuffer_.peek(),
                        outputBuffer_.readableBytes());
    if (n > 0) {
      outputBuffer_.retrieve(n);
      if (outputBuffer_.readableBytes() == 0) { // data in buffer all sent -> writeComplete
        channel_->disableWriting();
        if (writeCompleteCallback_) { 
          loop_->queueInLoop(
              std::bind(writeCompleteCallback_, shared_from_this()));
        }
        if (state_ == kDisconnecting) { // conn->shutdown has been called
          shutdownInLoop();
        }
      } else {
        fprintf(stdout,"I am going to write more data\n");
      }
    } else { // n<=0
      fprintf(stderr,"TcpConnection::handleWrite\n");
    }
  } else { // channel_->isWriting()==false // ? when this will happen?
    fprintf(stdout,"Connection is down, no more writing\n");
  }
}

void TcpConnection::handleClose()
{
  loop_->assertInLoopThread();
  fprintf(stdout,"TcpConnection::handleClose state = %d \n", state_);
  assert(state_ == kConnected || state_ == kDisconnecting);
  // we don't close fd, leave it to dtor, so we can find leaks easily.
  channel_->disableAll();
  // must be the last line
  closeCallback_(shared_from_this());
}

void TcpConnection::handleError()
{
  int err = sockets::getSocketError(channel_->fd());
  (void) err;
}
