#ifndef CONNECTOR
#define CONNECTOR
#include "noncopyable.hh"
#include "InetAddress.hh"
#include <memory>
#include <functional>



class EventLoop;
class Channel;

class Connector : noncopyable{
public:
    typedef std::function<void(int sockfd)> NewConnectionCallback;

    Connector(EventLoop* loop, const InetAddress& serverAddr);
    ~Connector();

    void setNewConnectionCallback(const NewConnectionCallback& cb){ newConnectionCallback_ = cb;}

    void start();
    void restart();
    void stop();

    const InetAddress& serverAddress() const {return serverAddr_; }

private:

    enum States { kDisconnected = 0,kConnecting,kConnected};
    static const int kMaxRetryDelaysMs = 30 * 1000;
    static const int kIniRetryDelaysMs = 500;

    void setState(States s) {state_ = s; }
    void startInLoop();
    void stopInLoop();
    void connect();
    void connecting(int sockfd);
    void handleWrite();
    void handleError();
    void retry(int sockfd);
    int removeAndResetChannel();
    void resetChannel();

    EventLoop* loop_;
    InetAddress  serverAddr_;
    bool connect_;
    States state_;
    std::unique_ptr<Channel> channel_;
    NewConnectionCallback newConnectionCallback_;
    int retryDelayMs_;
    // std::weak_ptr<Timer> timer_;
};

#endif /* CONNECTOR */
