// Callbacks.hh

#ifndef CALLBACKS
#define CALLBACKS

#include <functional>
#include <memory>
#include "../Reactor/Timestamp.hh"
#include "Buffer.hh"

class Buffer;
class TcpConnection;
class EventLoop;

typedef std::shared_ptr<TcpConnection> TcpConnectionPtr;
typedef std::function<void()> TimerCallback;
typedef std::function<void(const TcpConnectionPtr&)> ConnectionCallback;
typedef std::function<void(const TcpConnectionPtr&, Buffer *buf, Timestamp receiveTime)> MessageCallback;
typedef std::function<void(const TcpConnectionPtr&)> WriteCompleteCallback;
typedef std::function<void(const TcpConnectionPtr&)> CloseCallback;
typedef std::function<void(const TcpConnectionPtr&, size_t)> HighWaterMarkCallback;
typedef std::function<void(EventLoop*)> ThreadInitCallback;


void defaultConnectionCallback(const TcpConnectionPtr& conn);

void defaultMessageCallback(const TcpConnectionPtr&,Buffer* buf,Timestamp receiveTime);



#endif /* CALLBACKS */
