#include "Connector.hh"
#include "../Reactor/Channel.hh"
#include "../Reactor/EventLoop.hh"
#include "SocketsOps.hh"

const int Connector::kMaxRetryDelaysMs;
const int Connector::kIniRetryDelaysMs;
Connector::Connector(EventLoop* loop, const InetAddress& serverAddr)
    :   loop_(loop),
        serverAddr_(serverAddr),
        connect_(false),
        state_(kDisconnected),
        retryDelayMs_(kIniRetryDelaysMs)
    {

    }

Connector::~Connector()
{
    assert(!channel_);
}

void Connector::start(){
    connect_ = true;
    loop_->runInLoop(std::bind(&Connector::startInLoop,this));
}

void Connector::startInLoop(){
    loop_->assertInLoopThread();
    assert(state_ == kDisconnected);
    if(connect_){
        connect();
    } else {
        // bug
    }
}

void Connector::connect(){
    int sockfd = sockets::createNonblockingOrDie();
    int ret = sockets::connect(sockfd,serverAddr_.getSockAddrInet());
    int savedErrno = (ret==0) ? 0 : errno;
    switch (savedErrno)
    {
        case 0:
        case EINPROGRESS:
        case EINTR:
        case EISCONN:
        connecting(sockfd);
        break;

        case EAGAIN:
        case EADDRINUSE:
        case EADDRNOTAVAIL:
        case ECONNREFUSED:
        case ENETUNREACH:
        retry(sockfd);
        break;

        case EACCES:
        case EPERM:
        case EAFNOSUPPORT:
        case EALREADY:
        case EBADF:
        case EFAULT:
        case ENOTSOCK:
        // LOG_SYSERR << "connect error in Connector::startInLoop " << savedErrno;
        sockets::close(sockfd);
        break;

        default:
        // LOG_SYSERR << "Unexpected error in Connector::startInLoop " << savedErrno;
        sockets::close(sockfd);
        // connectErrorCallback_();
        break;
    }
}

void Connector::restart(){
    loop_->assertInLoopThread();
    setState(kDisconnected);
    retryDelayMs_ = kIniRetryDelaysMs;
    connect_ = true;
    startInLoop();
}

void Connector::stop(){
    connect_ = false;
    loop_->queueInLoop(std::bind(&Connector::stopInLoop,this));
}

void Connector::stopInLoop(){
    loop_->assertInLoopThread();
    if(state_ == kConnecting){
        setState(kDisconnected);
        int sockfd = removeAndResetChannel();
        retry(sockfd);
    }
}

void Connector::connecting(int sockfd){
    setState(kConnecting);
    assert(!channel_);
    auto tmp = std::make_unique<Channel>(loop_,sockfd);
    std::swap(tmp,channel_);
    channel_->setWriteCallback(std::bind(&Connector::handleWrite,this));
    channel_->setErrorCallback(std::bind(&Connector::handleError,this)); 

    channel_->enableWriting();
}

int Connector::removeAndResetChannel(){
    channel_->disableAll();
    channel_->remove();
    int sockfd = channel_->fd();
    loop_->queueInLoop(std::bind(&Connector::resetChannel,this));
    return sockfd;
}

void Connector::resetChannel(){
    channel_.reset();
}

void Connector::handleWrite(){
    if(state_ == kConnecting){
        int sockfd = removeAndResetChannel();
        int err = sockets::getSocketError(sockfd);
        if(err){
            fprintf(stderr,"Connector::handleWrite - SO_ERROR %d \n",err);
            retry(sockfd);
        } else if(sockets::isSelfConnect(sockfd)){
            fprintf(stderr,"Connector::Self connect\n");
            retry(sockfd);
        } else {
            setState(kConnected);
            if(connect_){
                newConnectionCallback_(sockfd);
            } else {
                sockets::close(sockfd);
            }
        }

    } else {
        assert(state_ == kDisconnected);
    }
}

void Connector::handleError(){
    fprintf(stderr,"Connector::handleEroor state = %d\n",state_);
    if(state_==kConnecting){
        int sockfd = removeAndResetChannel();
        int err = sockets::getSocketError(sockfd);
        fprintf(stderr,"Connector::handleError - SO_ERROR %d \n",err);
        retry(sockfd);
    }
}

void Connector::retry(int sockfd){
    sockets::close(sockfd);
    setState(kDisconnected);
    if(connect_){
        fprintf(stderr,"Connector::retry connecting to %s in %d milliseconds.\n",serverAddr_.toHostPort().c_str(),retryDelayMs_);
        loop_->runAfter(retryDelayMs_/1000.0,std::bind(&Connector::startInLoop,this));
        retryDelayMs_ = std::min(retryDelayMs_*2,kMaxRetryDelaysMs);
    } else {
        fprintf(stderr,"Connector::retry Do not connect\n");
    }
}