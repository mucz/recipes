// 测试Acceptor的功能，监听9981端口，链接到达后向它发送一个字符串随后断开连接
#include "../Acceptor.hh"
#include "../../Reactor/EventLoop.hh"
#include "../InetAddress.hh"
#include "../SocketsOps.hh"
#include <stdio.h>

void newConnection(int sockfd, const InetAddress& peerAddr){
    printf("newConnection() : accepted a new connection from %s\n",peerAddr.toHostPort().c_str());
    ::write(sockfd,"How are you?\n",13);
    sockets::close(sockfd);
}

int main(){ 
    printf("main() : pid = %d \n",getpid());
    InetAddress listenAddr(9981);
    EventLoop loop;
    Acceptor accpetor(&loop,listenAddr);
    accpetor.setNewConnectionCallback(newConnection);
    accpetor.listen();
    loop.loop();
}