// 测试TcpClient 连接本地localhost 9981 端口并发送信息
#include "../TcpClient.hh"
#include "../TcpConnection.hh"
#include "../TcpServer.hh"
#include "../../Reactor/EventLoop.hh"
#include <iostream>
#include <string>

std::string message = "Hello\n";

void onConnection(const TcpConnectionPtr& conn){ 
    if(conn->connected()){
        std::cout<<"onConnection(), tid = "<<CurrentThread::tid()<<" new connection "<<conn->name()<<"from "<<conn->peerAddress().toHostPort()<<std::endl;
        conn->send(message);
    } else {
        std::cout<<"onConnection(), tid = "<<CurrentThread::tid()<<" connection is down "<<conn->name()<<std::endl;
    }
}

void onMessage(const TcpConnectionPtr& conn, Buffer* buf,Timestamp receiveTime){
    std::cout<<"onMessage(): tid= "<< CurrentThread::tid() << " received "<< buf->readableBytes() << " bytes from connection "<<conn->name()<<" at "<<receiveTime.toString() <<std::endl;
    std::cout<<"onMessage()"<<buf->retrieveAllAsString()<<std::endl;
}

int main(){
    EventLoop loop;
    InetAddress serverAddr("localhost",9981);
    TcpClient client(&loop,serverAddr);

    client.setConnectionCallback(onConnection);
    client.setMessageCallback(onMessage);
    client.enableRetry();
    client.connect();
    loop.loop();
}