// test06.cc
// chargen服务 测试onWriteComplete
#include "../../Reactor/EventLoop.hh"
#include "../TcpServer.hh"
#include "../InetAddress.hh"
#include "../TcpConnection.hh"
#include <stdio.h> 
#include <iostream> 
#include <string>

std::string message;
void onConnection(const TcpConnectionPtr& conn){ 
    if(conn->connected()){
        std::cout<<"onConnection(), tid = "<<CurrentThread::tid()<<" new connection "<<conn->name()<<"from "<<conn->peerAddress().toHostPort()<<std::endl;
        conn->send(message);
    } else {
        std::cout<<"onConnection(), tid = "<<CurrentThread::tid()<<" connection is down "<<conn->name()<<std::endl;
    }
}

void onMessage(const TcpConnectionPtr& conn, Buffer* buf,Timestamp receiveTime){
    std::cout<<"onMessage(): tid= "<< CurrentThread::tid() << " received "<< buf->readableBytes() << " bytes from connection "<<conn->name()<<" at "<<receiveTime.toString() <<std::endl;
    buf->retrieveAll();
}

void onWriteComplete(const TcpConnectionPtr& conn){ 
    conn->send(message);
}

int main(int argc,char* argv[]){
    std::cout<<"main(): pid = "<<getpid()<<std::endl;

    std::string line;
    for(int i = 33;i<127;++i) {
        line.push_back(char(i));
    }
    line+=line;

    for(size_t i = 0; i<127-33;++i){
        message+=line.substr(i,72)+'\n';
    }

    InetAddress listenAddr(9981);
    EventLoop loop;

    TcpServer server(&loop,listenAddr);
    server.setConnectionCallback(onConnection);
    server.setMessageCallback(onMessage);
    server.setWriteCompleteCallback(onWriteComplete);
    server.setThreadNum(2);
    server.start();
    loop.loop();
}
