// test03.cc
// 在9981端口实现discard服务
#include "../../Reactor/EventLoop.hh"
#include "../TcpServer.hh"
#include "../InetAddress.hh"
#include "../TcpConnection.hh"
#include <stdio.h> 
#include <iostream>

void onConnection(const TcpConnectionPtr& conn){ 
    if(conn->connected()){
        std::cout<<"onConnection(), tid = "<<CurrentThread::tid()<<" new connection "<<conn->name()<<"from "<<conn->peerAddress().toHostPort()<<std::endl;
    } else {
        std::cout<<"onConnection(), tid = "<<CurrentThread::tid()<<" connection is down "<<conn->name()<<std::endl;
    }
}

void onMessage(const TcpConnectionPtr& conn, Buffer* buf,Timestamp receiveTime){
    std::cout<<"onMessage(): tid= "<< CurrentThread::tid() << " received "<< buf->readableBytes() << " bytes from connection "<<conn->name()<<" at "<<receiveTime.toString() <<std::endl;
    std::cout<<"onMessage() : " << buf->retrieveAllAsString()<<std::endl;
}

int main(){
    std::cout<<"main(): pid = "<<getpid()<<std::endl;

    InetAddress listenAddr(9981);
    EventLoop loop;

    TcpServer server(&loop,listenAddr);
    server.setConnectionCallback(onConnection);
    server.setMessageCallback(onMessage);
    server.setThreadNum(3);
    server.start();
    loop.loop();
}