// test05.cc
// 发送两次数据，测试TcpConnection::send()
#include "../../Reactor/EventLoop.hh"
#include "../TcpServer.hh"
#include "../InetAddress.hh"
#include "../TcpConnection.hh"
#include <stdio.h> 
#include <iostream> 
#include <string>
std::string message1;
std::string message2;

void onConnection(const TcpConnectionPtr& conn){ 
    if(conn->connected()){
        std::cout<<"onConnection(), tid = "<<CurrentThread::tid()<<" new connection "<<conn->name()<<"from "<<conn->peerAddress().toHostPort()<<std::endl;
        conn->send(message1);
        conn->send("\n");
        conn->send(message2);
        conn->shutdown();
    } else {
        std::cout<<"onConnection(), tid = "<<CurrentThread::tid()<<" connection is down "<<conn->name()<<std::endl;
    }
}

void onMessage(const TcpConnectionPtr& conn, Buffer* buf,Timestamp receiveTime){
    std::cout<<"onMessage(): tid= "<< CurrentThread::tid() << " received "<< buf->readableBytes() << " bytes from connection "<<conn->name()<<" at "<<receiveTime.toString() <<std::endl;
    buf->retrieveAll();
}

int main(int argc,char* argv[]){
    std::cout<<"main(): pid = "<<getpid()<<std::endl;

    int len1 = 100;
    int len2 = 200;
    if(argc > 2 ){
        len1 = atoi(argv[1]);
        len2 = atoi(argv[2]);
    }
    message1.resize(len1);
    message2.resize(len2);
    std::fill(message1.begin(),message1.end(),'A');
    std::fill(message2.begin(),message2.end(),'B');

    InetAddress listenAddr(9981);
    EventLoop loop;

    TcpServer server(&loop,listenAddr);
    server.setConnectionCallback(onConnection);
    server.setMessageCallback(onMessage);
    server.setThreadNum(2);
    server.start();
    loop.loop();
}
