// 测试Acceptor的功能，监听9981和9980端口，链接到达后向它发送不同的字符串随后断开连接
#include "../Acceptor.hh"
#include "../../Reactor/EventLoop.hh"
#include "../InetAddress.hh"
#include "../SocketsOps.hh"
#include <stdio.h>

void newConnection_1(int sockfd, const InetAddress& peerAddr){
    printf("newConnection() : accepted a new connection from %s\n",peerAddr.toHostPort().c_str());
    ::write(sockfd,"How are you?\n",13);
    sockets::close(sockfd);
}
void newConnection_2(int sockfd, const InetAddress& peerAddr){
    printf("newConnection() : accepted a new connection from %s\n",peerAddr.toHostPort().c_str());
    ::write(sockfd,"I'm fine\n",9);
    sockets::close(sockfd);
}


int main(){ 
    printf("main() : pid = %d \n",getpid());
    InetAddress listenAddr_1(9981);
    InetAddress listenAddr_2(9980);
    EventLoop loop;

    Acceptor accpetor_1(&loop,listenAddr_1);
    accpetor_1.setNewConnectionCallback(newConnection_1);
    accpetor_1.listen();

    Acceptor accpetor_2(&loop,listenAddr_2);
    accpetor_2.setNewConnectionCallback(newConnection_2);
    accpetor_2.listen();
    loop.loop();
}