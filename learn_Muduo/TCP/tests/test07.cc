// 测试Connector
//  - 正常连接
//  - 连接失败时定时重试
// 服务端： nc -l localhost 9981
#include "../Connector.hh"
#include "../../Reactor/EventLoop.hh"
#include <stdio.h>

EventLoop* g_loop;

void connectCallback(int sockfd){ 
    printf("connected.\n");
    g_loop->quit();
}

int main(){
    EventLoop loop;
    g_loop = &loop;
    InetAddress addr("127.0.0.1",9981);
    Connector connector(&loop,addr);
    connector.setNewConnectionCallback(connectCallback);
    connector.start();
    loop.loop();
}