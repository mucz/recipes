// Accpetor.cc
#include "Acceptor.hh"
#include "SocketsOps.hh"
#include "InetAddress.hh"
#include "../Reactor/EventLoop.hh"

Acceptor::Acceptor(EventLoop* loop,const InetAddress& listenAddr)
    :   loop_(loop),
        acceptSocket_(sockets::createNonblockingOrDie()),
        acceptChannel_(loop,acceptSocket_.fd()),
        listenning_(false)
{
    acceptSocket_.setReuseAddr(true);
    acceptSocket_.bindAddress(listenAddr);
    acceptChannel_.setReadCallback(std::bind(&Acceptor::handleRead,this));
}

void Acceptor::listen(){
    loop_->assertInLoopThread(); // why?
    listenning_ = true;
    acceptSocket_.listen();
    acceptChannel_.enableReading();
}

void Acceptor::handleRead(){
    loop_->assertInLoopThread();
    InetAddress peerAddr(0);
    int connfd = acceptSocket_.accept(&peerAddr); // Acceptor accepts
    if(connfd >= 0){
        if(newConnectionCallback_){
            newConnectionCallback_(connfd,peerAddr);
        } else {
            sockets::close(connfd);
        }
    }
} 