// TcpServer.cc
#include "TcpServer.hh"
#include "Acceptor.hh"
#include "../Reactor/EventLoop.hh"
#include "../Reactor/EventLoopThreadPool.hh"
#include "SocketsOps.hh"
#include "TcpConnection.hh"
#include <stdio.h> // snprintf
#include <functional>

TcpServer::TcpServer(EventLoop* loop, const InetAddress& listenAddr)
:   loop_(loop),
    name_(listenAddr.toHostPort()),
    acceptor_(std::make_unique<Acceptor>(loop,listenAddr)),
    threadPool_(std::make_shared<EventLoopThreadPool>(loop)),
    nextConnId_(1)
{
    acceptor_->setNewConnectionCallback(std::bind(&TcpServer::newConnection,this,std::placeholders::_1,std::placeholders::_2));
}

TcpServer::~TcpServer() { }

void TcpServer::setThreadNum(int numThreads){
    assert(0 <= numThreads);
    threadPool_->setThreadNum(numThreads);
}

void TcpServer::start(){
    if(started_.getAndSet(1)==0){
        threadPool_->start(threadInitCallback_);
    
        if(!acceptor_->listenning()){
            loop_->runInLoop(std::bind(&Acceptor::listen,acceptor_.get()));
        }
    }
}

void TcpServer::newConnection(int sockfd,const InetAddress& peerAddr){
    loop_->assertInLoopThread();
    char buf[32];
    snprintf(buf,sizeof(buf),"#%d",nextConnId_);
    ++nextConnId_;
    std::string connName = name_ + buf;

    fprintf(stdout,"TcpServer::newConneciton %s - %s new connnection from %s \n",name_.c_str(),connName.c_str(),peerAddr.toHostPort().c_str());

    InetAddress localAddr(sockets::getLocalAddr(sockfd));
    EventLoop* ioLoop = threadPool_->getNextLoop();
    TcpConnectionPtr conn(std::make_shared<TcpConnection>(ioLoop,connName,sockfd,localAddr,peerAddr));
    connections_[connName] = conn;
    conn->setConnectionCallback(connectionCallback_);
    conn->setMessageCallback(messageCallback_);
    conn->setWriteCompleteCallback(writeCompleteCallback_);
    conn->setCloseCallback(std::bind(&TcpServer::removeConnection,this,std::placeholders::_1));
    ioLoop->runInLoop(std::bind(&TcpConnection::connectEstablished,conn));
}

void TcpServer::removeConnection(const TcpConnectionPtr& conn){
    loop_->runInLoop(std::bind(&TcpServer::removeConnectionInLoop,this,conn));
}

void TcpServer::removeConnectionInLoop(const TcpConnectionPtr& conn){
    loop_->assertInLoopThread();
    fprintf(stdout,"TcpServer::removeConnection %s - connnection %s \n",name_.c_str(),conn->name().c_str());

    size_t n = connections_.erase(conn->name());
    assert(n == 1); (void)n;
    EventLoop* ioLoop = conn->getLoop();
    ioLoop->queueInLoop(std::bind(&TcpConnection::connectDestroyed,conn));
}
