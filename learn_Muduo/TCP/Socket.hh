// Socket.hh
#ifndef SOCKET_HH
#define SOCKET_HH
#include "noncopyable.hh"
class InetAddress;

class Socket : noncopyable{
public: 
    explicit Socket(int sockfd) : sockfd_(sockfd) { }
    ~Socket();

    int fd() { return sockfd_; }

    // bind and listen will abort if address in use
    void bindAddress(const InetAddress& localaddr);
    void listen();
    int accept(InetAddress* peeraddr);

    void shutdownWrite();

    // Enable/disable TCP_NODELAY (Nagle's algorithm)
    void setTcpNoDelay(bool on);

    // Enable/disable SO_REUSEADDR
    void setReuseAddr(bool on);

private:
    const int sockfd_;
};
#endif // SOCKET_HH