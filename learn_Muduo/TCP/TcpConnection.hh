// TcpConnection.hh
#ifndef TCPCONNECTION
#define TCPCONNECTION
#include "noncopyable.hh"
#include <string>
#include "InetAddress.hh"
#include "../Reactor/Timestamp.hh"
#include "Callbacks.hh"
#include "Buffer.hh"
#include <any>

class Channel;
class EventLoop;
class Socket;

class TcpConnection : noncopyable,
                      public std::enable_shared_from_this<TcpConnection>
{
public:
    TcpConnection(EventLoop* loop,
                  const std::string& name,
                  int sockfd, 
                  const InetAddress& localAddr,
                  const InetAddress& peerAddr);
    ~TcpConnection();

    EventLoop* getLoop() { return loop_;}
    const std::string& name() {return name_; }
    const InetAddress& localAddress() { return localAddr_; }
    const InetAddress& peerAddress() { return peerAddr_; }
    bool connected() const { return state_ == kConnected; }


    // void send(const std::string& message);
    void send(const std::string_view& message);
    void send(const void* data,int len);
    void send(Buffer& buf);
    void send(Buffer* buf);

    void shutdown();
    void setTcpNoDelay(bool on);
   
    void setConnectionCallback(const ConnectionCallback& cb)  { connectionCallback_ = cb; }
    void setMessageCallback(const MessageCallback& cb)  { messageCallback_ = cb; }
    void setWriteCompleteCallback(const WriteCompleteCallback& cb)  { writeCompleteCallback_ = cb; }
    void setCloseCallback(const CloseCallback& cb)  { closeCallback_ = cb; }
    void setHighWaterCallback (const HighWaterMarkCallback& cb, const size_t highWaterMark)  { highWaterMarkCallback_ = cb; highWaterMark_ = highWaterMark; } 

    // called once by TcpServer once 
    void connectEstablished(); 
    void connectDestroyed();

    void setContext(const std::any& context) { context_ = context; }
    const std::any& getContext() const { return context_;}

    enum StateE {kConnecting, kConnected, kDisconnecting, kDisconnected, };
    
    Buffer& inputBuffer() {return inputBuffer_; }
    Buffer& outputBuffer() {return outputBuffer_; }

private:

    void setState(StateE s){state_ = s;}

    void handleRead(Timestamp receiveTime);
    void handleWrite();
    void handleError();
    void handleClose();

    void sendInLoop(const std::string& message);
    void shutdownInLoop();

    EventLoop* loop_;
    std::string name_;
    StateE state_;
    std::unique_ptr<Socket> socket_;
    std::unique_ptr<Channel> channel_;
    InetAddress localAddr_;
    InetAddress peerAddr_;
    ConnectionCallback connectionCallback_;
    MessageCallback messageCallback_;
    WriteCompleteCallback writeCompleteCallback_;
    CloseCallback closeCallback_;
    HighWaterMarkCallback highWaterMarkCallback_;
    size_t highWaterMark_;

    Buffer inputBuffer_;
    Buffer outputBuffer_;

    std::any context_;
};

#endif /* TCPCONNECTION */
