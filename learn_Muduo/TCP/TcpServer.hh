// TcpServer.hh
// 管理acceptor 获得的TcpConnection 
#ifndef TCP_SERVER_HH
#define TCP_SERVER_HH
#include "noncopyable.hh"
#include <string>
#include <map>
#include "Callbacks.hh"
#include "InetAddress.hh"
#include "../Thread/Thread.hh"
class Acceptor;
class EventLoop;
class EventLoopThreadPool;

class TcpServer : noncopyable{
public: 
    TcpServer(EventLoop* loop,const InetAddress& listenAddr);
    ~TcpServer();

    void setThreadNum(int numThreads);
    void start();
    
    void setConnectionCallback(const ConnectionCallback& cb) { connectionCallback_ = cb; }
    void setMessageCallback(const MessageCallback& cb ){ messageCallback_ = cb; }
    void setWriteCompleteCallback(const WriteCompleteCallback& cb){ writeCompleteCallback_ = cb; }

    void setThreadInitCallback(const ThreadInitCallback& cb){threadInitCallback_ = cb; }

    std::shared_ptr<EventLoopThreadPool> threadPool() { return threadPool_; }
private:

    void newConnection(int sockfd,const InetAddress& peerAddr);
    void removeConnection(const TcpConnectionPtr& conn);
    void removeConnectionInLoop(const TcpConnectionPtr& conn);

    typedef std::map<std::string,TcpConnectionPtr> ConnectionMap;

    EventLoop* loop_;
    const std::string name_;
    ConnectionCallback connectionCallback_;
    MessageCallback messageCallback_;
    WriteCompleteCallback writeCompleteCallback_;
    ThreadInitCallback threadInitCallback_;
    std::unique_ptr<Acceptor> acceptor_;
    std::shared_ptr<EventLoopThreadPool> threadPool_;
    AtomicInt32 started_;
    int nextConnId_;
    ConnectionMap connections_;
};
#endif //TCP_SERVER_HH