// 一个无限容量的Blokcing Queue
#include "Thread/Mutex.hh"
#include <deque>

class BlockingQueue : noncopyable{
private:
    MutexLock mutex_;
    Condition cond_;
    std::deque<int> queue;
public:
    BlockingQueue():mutex_(), cond_(mutex_) {}
    int dequeue(); 
    void enqueue(int x);
};

int BlockingQueue::dequeue(){
    MutexLockGuard lock(mutex_);
    while(queue.empty()){
        cond_.wait();
    }
    assert(!queue.empty());
    int top = queue.front();
    queue.pop_front();
    return top;
}

void BlockingQueue::enqueue(int x){
    {
    MutexLockGuard lock(mutex_);
    queue.push_back(x);
    }
    cond_.notify();
    // 这里使用notify()而不是notifyAll()为了
    //    - 在数据激增时高效唤起其他线程
    //    - 不引起惊群现象
}