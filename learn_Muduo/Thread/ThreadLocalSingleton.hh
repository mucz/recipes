#ifndef THREADLOCALSINGLETON
#define THREADLOCALSINGLETON
#include "noncopyable.hh"
#include <assert.h>
#include <pthread.h>

template<typename T>
class ThreadLocalSingleton : noncopyable{ 
public:

    ThreadLocalSingleton() = delete;
    ~ThreadLocalSingleton() = delete;
    
    static T& instance(){
        if(!t){
            //  FIXME : use unique_ptr?
            t = new T();
            deleter_.set(t);
        }
        return *t;
    }
    static T* pointer() { return t;}

private:
    static void destructor(void* obj){
        assert(obj == t);
        typedef char T_must_be_complete_type[sizeof(T)==0? -1 : 1];
        T_must_be_complete_type dummy; (void) dummy;
        delete t;
        t = nullptr;
    }    

    struct Deleter{
        Deleter() { pthread_key_create(&pkey_,&ThreadLocalSingleton::destructor); }
        ~Deleter(){ pthread_key_delete(pkey_); }
        void set(T* newObj) {
            assert(pthread_getspecific(pkey_) ==NULL);
            pthread_setspecific(pkey_,newObj);
        }
        pthread_key_t pkey_;
    };

    static __thread T* t;
    static Deleter deleter_;
};

template<typename T>
__thread T* ThreadLocalSingleton<T>::t = nullptr;

template <typename T> 
typename ThreadLocalSingleton<T>::Deleter ThreadLocalSingleton<T>::deleter_;


#endif /* THREADLOCALSINGLETON */
