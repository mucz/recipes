#include "Thread.hh"

struct ThreadData{
    typedef Thread::ThreadFunc ThreadFunc;
    ThreadFunc func_;
    std::string name_;
    pid_t* tid_;
    CountDownLatch* latch_;

    ThreadData(ThreadFunc func,
                const std::string& name,
                pid_t* tid,
                CountDownLatch* latch)
            : func_(std::move(func)),
            name_(name),
            tid_(tid),
            latch_(latch){}
        
    void runInThread(){ 
        *tid_ = CurrentThread::tid();
        tid_ = NULL;
        latch_->countDown();
        latch_ = NULL;

        CurrentThread::t_threadName = name_.empty() ? "MyThread" : name_.c_str();
        ::prctl(PR_SET_NAME,CurrentThread::t_threadName);
        try
        {
            func_();
            CurrentThread::t_threadName = "Finished";
        }
        catch(const std::exception& e)
        {
            CurrentThread::t_threadName = "crashed";
            fprintf(stderr, "exception caught in Thread %s\n ", name_.c_str());
            fprintf(stderr, "reason : %s\n", e.what());
            abort();
        }
        
    }
};

// 每个新线程的开始函数，解包数据为ThreadData类型，调用其中的函数指针
void* startThread(void* obj){
    ThreadData* data = static_cast<ThreadData*>(obj);
    data->runInThread();
    delete data;
    return NULL;
}

Thread::Thread(ThreadFunc func, const std::string& n)
    : started_(false),
        joined_(false),
        pthreadId_(0),
        tid_(0),
        func_(std::move(func)),
        name_(n),
        latch_(1)
    {
        setDefaultName();
    }

Thread::~Thread(){ 
    if(started_ && !joined_ ){ // 如果joined_=true 那么说明会有别的线程会回收这个线程
        pthread_detach(pthreadId_);
    }
}

void Thread::setDefaultName(){
    int num = numCreated_.incrementAndGet();
    if(name_.empty()){
        char buf[32];
        snprintf(buf,sizeof(buf),"Thread%d",num);
        name_ = buf;
    }
}

void Thread::start(){ 
    assert(!started_);
    started_ = true;
    ThreadData* data = new ThreadData(func_,name_,&tid_,&latch_);
    if(pthread_create(&pthreadId_,NULL, &startThread,data)){
        started_ = false;
        delete data;
        fprintf(stderr,"Failed in pthread_create!");
        abort();
    } else {
        latch_.wait();
        assert(tid_>0);
    }
}

int Thread::join(){ 
    assert(started_);
    assert(!joined_);
    joined_ = true;
    return pthread_join(pthreadId_,NULL);
}

AtomicInt32 Thread::numCreated_; // 静态成员变量实例化
