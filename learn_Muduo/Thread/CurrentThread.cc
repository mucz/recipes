#include "CurrentThread.hh"

namespace CurrentThread
{
    __thread int t_cachedTid = 0;
    __thread char t_tidString[32];
    __thread int t_tidStringLength = 6;
    __thread const char* t_threadName = "unknown";

    int tid(){
        // __builtin_expect((x),0)表示 x 的值为假的可能性更大。
        // 通过这种方式，编译器在编译过程中，
        // 会将可能性更大的代码紧跟着起面的代码，从而减少指令跳转带来的性能上的下降。
        if(__builtin_expect(t_cachedTid == 0, 0)){
            cacheTid();
        }
        return t_cachedTid;
    }  

    pid_t gettid(){
        return static_cast<pid_t>(::syscall(SYS_gettid));
    };

    void cacheTid(){ 
        if(t_cachedTid == 0){
            t_cachedTid = gettid();
            t_tidStringLength = snprintf(t_tidString,sizeof t_tidString, "%5d", t_cachedTid);
            // s : string buffer
            // n : max number of bytes 
            // f : format
        }
    }
    bool isMainThread(){ 
        return tid() == ::getpid();
    }

    void sleepUsec(int64_t usec){
        struct timespec ts = { 0, 0};
        static const int kMicroSecondsPerSecond = 1000* 1000;
        ts.tv_sec = static_cast<time_t> (usec / kMicroSecondsPerSecond);
        ts.tv_nsec = static_cast<long> (usec % kMicroSecondsPerSecond * 1000);
        ::nanosleep(&ts,NULL);
    }
}
