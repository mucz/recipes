// 线程私有的变量声明和相关函数定义
#ifndef CURRENT_THREAD_HH
#define CURRENT_THREAD_HH

#include <string>
#include <stdint.h>
#include <sys/syscall.h>
#include <unistd.h>
#include "noncopyable.hh"

namespace CurrentThread
{
    // internal
    // __thread是GCC内置的线程局部存储设施
    // __thread变量每一个线程有一份独立实体，各个线程的值互不干扰。
    extern __thread int t_cachedTid; // 这里是声明，那么定义在哪里？
    extern __thread char t_tidString[32];
    extern __thread int t_tidStringLength;
    extern __thread const char* t_threadName;

    inline const char* tidString(){ return t_tidString; }
    inline int tidStringLength() { return t_tidStringLength; }
    inline const char* name() { return t_threadName; }

    void cacheTid();
    int tid(); 
    void sleepUsec(int64_t usec);
    bool isMainThread();

    // std::string stackTrace(bool demangle){
    // 有些复杂 暂时没写
    // }
}
#endif // CURRENT_THREAD_HH