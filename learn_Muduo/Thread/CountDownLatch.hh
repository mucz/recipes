// 倒计时是一种常用的同步手段  用于： 
//    主线程等待多个子线程完成初始化
//    多个子线程等待主线程的“起跑命令” 
#ifndef COUNT_DOWN_LATCH_HH
#define COUNT_DOWN_LATCH_HH

#include "Mutex.hh"

class CountDownLatch : noncopyable{
public:
    explicit CountDownLatch(int count);
    void wait();
    void countDown();
    int getCount() const; 
private:
    mutable MutexLock mutex_;
    Condition cond_;
    int count_;
};

#endif // COUNT_DOWN_LATCH_HH