// 实现线程相关的常用函数
#ifndef THREAD_HH
#define THREAD_HH

#include <sys/types.h>
#include <sys/prctl.h>
#include "noncopyable.hh"
#include <functional>
#include "CountDownLatch.hh"

template<typename T>
class AtomicIntegerT : noncopyable{
    public:
        AtomicIntegerT(): value_(0){}

        T get() { return __sync_val_compare_and_swap(&value_,0,0); }
        T getAndAdd(T x){ return __sync_fetch_and_add(&value_,x); }
        T addAndGet(T x){ return getAndAdd(x)+x; }
        T incrementAndGet() { return addAndGet(1); }
        T decrementAndGet() { return addAndGet(-1); }
        void add(T x) { getAndAdd(x); }
        void increment() { incrementAndGet(); }
        void decrement() { decrementAndGet(); }
        T getAndSet(T newValue){ return __sync_lock_test_and_set(&value_,newValue); }

    private:
        volatile T value_;
};

typedef AtomicIntegerT<int32_t> AtomicInt32;

class Thread: noncopyable{
public:
    typedef std::function<void()> ThreadFunc;
    
    explicit Thread(ThreadFunc func, const std::string& name = std::string());
    ~Thread();

    void start();
    int join();

    bool started() const {return started_;}
    pid_t tid() const {return tid_;}
    const std::string& name() const {return name_;}
    static int numCreated() {return numCreated_.get();}

private:
    void setDefaultName();
    
    bool started_;
    bool joined_;
    pthread_t pthreadId_;
    pid_t tid_;
    ThreadFunc func_;
    std::string name_;
    CountDownLatch latch_;

    static AtomicInt32 numCreated_; // static 保证了所有线程有不同的默认名字
};

#endif // THREAD_HH