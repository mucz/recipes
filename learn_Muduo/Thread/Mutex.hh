// 采用RAII手法封装的pthread_mutex锁，用于构建高级同步设施
// 参考Muduo第二章
#ifndef MUTEX_LOCK_HH
#define MUTEX_LOCK_HH

#include <pthread.h>
#include <assert.h>
#include "CurrentThread.hh" 

class MutexLock : noncopyable{
    public:
        MutexLock()
        :mutex_(),holder_(0)
        {
            pthread_mutex_init(&mutex_,NULL);
        }

        ~MutexLock()
        {
            assert(holder_ == 0);
            pthread_mutex_destroy(&mutex_);
        }

        bool isLockedByThisThread(){
            return holder_ == CurrentThread::tid();
        }
        
        void assertLocked(){
            assert(isLockedByThisThread());
        }
        
        void lock(){
            pthread_mutex_lock(&mutex_);
            holder_ = CurrentThread::tid();
        }

        void unlock(){
            holder_ = 0;
            pthread_mutex_unlock(&mutex_);
        }

        pthread_mutex_t* getPthreadMutex(){
            return &mutex_;
        }
    private:
        pthread_mutex_t mutex_;
        pid_t holder_; 
};

class MutexLockGuard : noncopyable{
    public:
        explicit MutexLockGuard(MutexLock& mutex)
        :mutex_(mutex) 
        {
            mutex_.lock();
        }
        
        ~MutexLockGuard(){
            mutex_.unlock();
        }
    private:
        MutexLock& mutex_;
};

#define MutexLockGuard(x) static_assert(false,"Missing mutex guard variable name!")
// 避免使用LockGuard时忘记加变量名


class Condition : noncopyable{
    public:
        explicit Condition(MutexLock& mutex)
        :mutex_(mutex)
        {
            pthread_cond_init(&pcond_,NULL);
        }

        ~Condition(){
            pthread_cond_destroy(&pcond_);
        }

        void wait(){
            pthread_cond_wait(&pcond_,mutex_.getPthreadMutex());
        }

        void notify(){
            pthread_cond_signal(&pcond_);
        }
        
        void notifyAll(){
            pthread_cond_broadcast(&pcond_);
        }

    private:
        MutexLock& mutex_;
        pthread_cond_t pcond_;

};
#endif //MUTEX_LOCK_HH