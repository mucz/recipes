#include "CountDownLatch.hh"

CountDownLatch::CountDownLatch(int count)
:mutex_(), //注意顺序
cond_(mutex_), // mutex_作为cond_的构造参数
count_(count) {}

void CountDownLatch::wait(){
    MutexLockGuard lock(mutex_);
    while(count_>0){
        cond_.wait();
    }
}

void CountDownLatch::countDown(){
    MutexLockGuard lock(mutex_);
    --count_;
    if(count_==0){
        cond_.notifyAll(); //通知所有等待者，所以要用notifyAll()
    }
}

int CountDownLatch::getCount() const {
    MutexLockGuard lock(mutex_);
    return count_;
}