// 一个TCP中继的例子，作为socks4a例子的雏形
#include "tunnel.hh"

EventLoop* g_loop;
std::map<std::string,TunnelPtr> g_tunnels;

void onServerConnection(const TcpConnectionPtr& conn){
    if(conn->connected()){
        conn->setTcpNoDelay(true);
    } else {
        assert(g_tunnels.find(conn->name())!= g_tunnels.end());
        auto it = g_tunnels.find(conn->name());
        if(it != g_tunnels.end()){
            it->second->disconnect();
            g_tunnels.erase(it);
        }
    }
} 

void onServerMessage(const TcpConnectionPtr& conn, Buffer* buf,Timestamp receiveTime){
    if(g_tunnels.count(conn->name())==0){
        if(buf->readableBytes() > 32){
            conn->shutdown();
        } else if(buf->readableBytes() > 8){
            const char* begin = buf->peek() + 8;
            const char* end = buf->peek()+ buf->readableBytes();
            const char* where = std::find(begin,end,'\0');
            if(where!=end){
                char ver = buf->peek()[0];
                char cmd = buf->peek()[1];
                const void* port = buf->peek()+2;
                const void* ip = buf->peek() +4;
                sockaddr_in addr;
                bzero(&addr,sizeof(addr));
                addr.sin_family = AF_INET;
                addr.sin_port = *static_cast<const in_port_t*>(port);
                addr.sin_addr.s_addr = *static_cast<const uint32_t*>(ip);
                InetAddress serverAddr(addr);
                bool socks4a = sockets::networkToHost32(addr.sin_addr.s_addr) < 256;
                if(ver == 4 && cmd == 1 && !socks4a){
                    TunnelPtr tunnel(std::make_shared<Tunnel>(g_loop,serverAddr,conn));
                    tunnel->setup();
                    tunnel->connect();
                    g_tunnels[conn->name()]= tunnel;
                    buf->retrieveUntil(where+1);
                    char response[] = "\000\x5aUVWXYZA";
                    conn->send(response,8);
                } else {
                    conn->shutdown();
                }
            }
        }
    } else if(conn->getContext().has_value()){
        const TcpConnectionPtr clientConn = std::any_cast<TcpConnectionPtr> (conn->getContext());
        clientConn->send(buf);
    }
}

int main(int argc, char* argv[]){
    if(argc < 2){
        fprintf(stderr,"Usage: %s <listen_port>\n",argv[0]);
        exit(1);
    }
    uint16_t acceptport = static_cast<uint16_t>(atoi(argv[1]));
    InetAddress listenAddr(acceptport);
    EventLoop loop;
    g_loop = &loop;

    TcpServer server(&loop,listenAddr);
    server.setConnectionCallback(onServerConnection);
    server.setMessageCallback(onServerMessage);
    server.start();
    loop.loop();
}