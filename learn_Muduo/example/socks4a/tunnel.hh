#ifndef TUNNEL
#define TUNNEL
#include "noncopyable.hh"
#include <memory>
#include <functional>
#include "Reactor/EventLoop.hh"
#include "TCP/TcpServer.hh"
#include "TCP/TcpConnection.hh"
#include "TCP/TcpClient.hh"

class Tunnel : noncopyable,
               public std::enable_shared_from_this<Tunnel> {
    public: 
        Tunnel(EventLoop* loop,const InetAddress& serverAddr,const TcpConnectionPtr& serverConn)
        :   client_(loop,serverAddr),
            serverConn_(serverConn)
        {
            fprintf(stdout,"Tunnel %s <-> %s\n",serverConn->peerAddress().toHostPort().c_str(),serverAddr.toHostPort().c_str());
        }

        ~Tunnel() { fprintf(stdout,"~Tunnel \n"); } 

        void setup(){ 
            using namespace std::placeholders;
            client_.setConnectionCallback(std::bind(&Tunnel::onClientConnection,shared_from_this(),_1));
            client_.setMessageCallback(std::bind(&Tunnel::onClientMessage,shared_from_this(),_1,_2,_3));
            // serverConn_.setHighWaterCallback(std::bind(&Tunnel::onHighWaterMark,(shared_from_this()),kServer,_1,_2),1024*1024);
        }

        void connect() { client_.connect();}
        void disconnect() { client_.disconnect();}

    private:
        void teardown(){ 
            client_.setConnectionCallback(defaultConnectionCallback);
            client_.setMessageCallback(defaultMessageCallback);
            if(serverConn_){
                serverConn_->setContext(std::any());
                serverConn_->shutdown();
            }
        }

        void onClientConnection(const TcpConnectionPtr& conn){
            using namespace std::placeholders;
            if(conn->connected()){
                conn->setTcpNoDelay(true);
                serverConn_->setContext(conn);
                if(!serverConn_->inputBuffer().empty()){
                    conn->send(serverConn_->inputBuffer());
                }
            } else {
                teardown();
            }
        }

        void onClientMessage(const TcpConnectionPtr& conn,Buffer* buf,Timestamp receiveTime){
            if(serverConn_){
                serverConn_->send(buf);
            } else {
                buf->retrieveAll();
                abort();
            }
        }

        TcpClient client_;
        TcpConnectionPtr serverConn_;
};
typedef std::shared_ptr<Tunnel> TunnelPtr;

#endif /* TUNNEL */
