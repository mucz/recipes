// 一个TCP中继的例子，作为socks4a例子的雏形
#include "tunnel.hh"

EventLoop* g_loop;
InetAddress* g_serverAddr;
std::map<std::string,TunnelPtr> g_tunnels;


void onServerConnection(const TcpConnectionPtr& conn){
    if(conn->connected()){
        conn->setTcpNoDelay(true);
        TunnelPtr tunnel = std::make_shared<Tunnel>(g_loop,*g_serverAddr,conn);
        tunnel->setup();
        tunnel->connect();
        g_tunnels[conn->name()] = tunnel;
    } else {
        assert(g_tunnels.find(conn->name())!= g_tunnels.end());
        g_tunnels[conn->name()]->disconnect();
        g_tunnels.erase(conn->name());
    }
} 

void onServerMessage(const TcpConnectionPtr& conn, Buffer* buf,Timestamp receiveTime){
    if(conn->getContext().has_value()){
        const TcpConnectionPtr clientConn = std::any_cast<TcpConnectionPtr> (conn->getContext());
        clientConn->send(buf);
    }
}

int main(int argc, char* argv[]){
    if(argc < 3){
        fprintf(stderr,"Usage: %s <serv_port> <listen_port>\n",argv[0]);
        exit(1);
    }
    uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
    InetAddress serverAddr("127.0.0.1",port);
    g_serverAddr = &serverAddr;
    uint16_t acceptport = static_cast<uint16_t>(atoi(argv[2]));
    InetAddress listenAddr(acceptport);
    EventLoop loop;
    g_loop = &loop;

    TcpServer server(&loop,listenAddr);
    server.setConnectionCallback(onServerConnection);
    server.setMessageCallback(onServerMessage);
    server.start();
    loop.loop();
}