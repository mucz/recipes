#ifndef DISPATCHER
#define DISPATCHER
#include "codec.hh"
#include "down_pointer_cast.hh"
#include <memory>
#include <functional>
#include <map>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/message.h>

typedef std::shared_ptr<google::protobuf::Message> MessagePtr;

class Callback : noncopyable{
    public:
        virtual ~Callback() = default;
        virtual void onMessage(const TcpConnectionPtr&,
                               const MessagePtr& message,
                               Timestamp) const = 0;
};

template <typename T>
class CallbackT : public Callback{
    static_assert(std::is_base_of<google::protobuf::Message,T>::value,
                  "T must be derived from gpb::Message");
    public:

        typedef std::function<void(const TcpConnectionPtr&,
                                   const std::shared_ptr<T>&,
                                   Timestamp)> ProtobufMessageTCallback;
        CallbackT(const ProtobufMessageTCallback& callback)
        :    callback_(callback)
        {

        }

        void onMessage(const TcpConnectionPtr& conn, 
                       const MessagePtr& message,
                       Timestamp receiveTime) const override{
            std::shared_ptr<T> concrete = down_pointer_cast<T>(message); // base->derived
            assert(concrete != nullptr);
            callback_(conn,concrete,receiveTime);
        }
    private:
        ProtobufMessageTCallback callback_;
};

class ProtobufDispatcher{
    public:
        typedef std::function<void(const TcpConnectionPtr&,
                                   const MessagePtr&,
                                   Timestamp)> ProtobufMessageCallback;
        explicit ProtobufDispatcher(const ProtobufMessageCallback& defaultCb)
            :   defaultCallback_(defaultCb)
        {
        }

        void onProtobufMessage(const TcpConnectionPtr& conn,
                               const MessagePtr& message, 
                               Timestamp receiveTime) const{ 
            auto it = callbacks_.find(message->GetDescriptor());
            if(it != callbacks_.end()){
                it->second->onMessage(conn,message,receiveTime);
            } else {
                defaultCallback_(conn,message,receiveTime);
            }
        }       

        template<typename T>
        void registerMessageCallback(const typename CallbackT<T>::ProtobufMessageTCallback& callback){
            std::shared_ptr<CallbackT<T>> pd = std::make_shared<CallbackT<T>>(callback);
            callbacks_[T::descriptor()] = pd;
        }

        private:
            typedef std::map<const google::protobuf::Descriptor* ,std::shared_ptr<Callback>> CallbackMap;

            CallbackMap callbacks_;
            ProtobufMessageCallback defaultCallback_;
};
#endif /* DISPATCHER */
