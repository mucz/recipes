#include "codec.hh"
#include <assert.h>
#include <google/protobuf/descriptor.h>
#include <zlib.h>

void ProtobufCodec::fillEmptyBuffer(Buffer* buf, const google::protobuf::Message& message){
    assert(buf->empty());

    const std::string& typeName = message.GetTypeName();
    int32_t nameLen = static_cast<int32_t>(typeName.size()+1);
    buf->appendInt32(nameLen);
    buf->append(typeName.c_str(),nameLen);

    int byte_size = message.ByteSize();
    buf->ensureWritableBytes(byte_size);

    uint8_t* start = reinterpret_cast<uint8_t*>(buf->beginWrite());
    uint8_t* end = message.SerializeWithCachedSizesToArray(start);
    assert(end - start == byte_size); 

    buf->hasWritten(byte_size);

    int32_t checkSum = static_cast<int32_t>(
        ::adler32(1,
                  reinterpret_cast<const Bytef*>(buf->peek()),
                  static_cast<int>(buf->readableBytes())));
    buf->appendInt32(checkSum);
    assert(buf->readableBytes() == sizeof(nameLen) + nameLen + byte_size + sizeof(checkSum));
    buf->prependInt32(buf->readableBytes());
}

namespace
{
  const std::string kNoErrorStr = "NoError";
  const std::string kInvalidLengthStr = "InvalidLength";
  const std::string kCheckSumErrorStr = "CheckSumError";
  const std::string kInvalidNameLenStr = "InvalidNameLen";
  const std::string kUnknownMessageTypeStr = "UnknownMessageType";
  const std::string kParseErrorStr = "ParseError";
  const std::string kUnknownErrorStr = "UnknownError";
}

const std::string& ProtobufCodec::errorCodeToString(ErrorCode errorCode)
{
  switch (errorCode)
  {
   case kNoError:
     return kNoErrorStr;
   case kInvalidLength:
     return kInvalidLengthStr;
   case kCheckSumError:
     return kCheckSumErrorStr;
   case kInvalidNameLen:
     return kInvalidNameLenStr;
   case kUnknownMessageType:
     return kUnknownMessageTypeStr;
   case kParseError:
     return kParseErrorStr;
   default:
     return kUnknownErrorStr;
  }
}
void ProtobufCodec::defaultErrorCallback(const TcpConnectionPtr& conn,
                                         Buffer* buf,
                                         Timestamp,
                                         ErrorCode errorCode){
  fprintf(stderr,"ProtobufCodec::defaultErrorCallback - %s\n",errorCodeToString(errorCode).c_str());
  if (conn && conn->connected())
  {
    conn->shutdown();
  }
}

void ProtobufCodec::onMessage(const TcpConnectionPtr& conn,
                              Buffer* buf,
                              Timestamp receiveTime){
    while(buf->readableBytes() >= kMinMessageLen + kHeaderLen ){
        const int32_t len = buf->peekInt32();
        if(len > kMaxMessageLen || len<kMinMessageLen){
            errorCallback_(conn,buf,receiveTime,kInvalidLength);
            break;
        } else if (buf->readableBytes() >= static_cast<size_t>(len+kHeaderLen)){
            ErrorCode error = kNoError;
            MessagePtr message = parse(buf->peek()+kHeaderLen,len,&error);
            if(error ==kNoError && message){
                messageCallback_(conn,message,receiveTime);
                buf->retrieve(kHeaderLen+len);
            } else {
                errorCallback_(conn,buf,receiveTime,error);
                break;
            }
        } else {
            break;
        }
    }
}

int32_t asInt32(const char* buf){
    int32_t be32=0;
    ::memcpy(&be32,buf,sizeof(be32));
    return sockets::networkToHost32(be32);
}
google::protobuf::Message* ProtobufCodec::CreateMessage(const std::string& typeName){
    google::protobuf::Message* message = NULL;
    const google::protobuf::Descriptor* descriptor = 
        google::protobuf::DescriptorPool::generated_pool()->FindMessageTypeByName(typeName);
    if(descriptor){
        const google::protobuf::Message* prototype = 
            google::protobuf::MessageFactory::generated_factory()->GetPrototype(descriptor);
        if(prototype){
            message = prototype->New();
        }
    }
    return message;
}
MessagePtr ProtobufCodec::parse(const char* buf,int len ,ErrorCode* errorCode){
    MessagePtr message;
    int32_t expectedCheckSum = asInt32(buf+len-kHeaderLen);
    int32_t checkSum = static_cast<int32_t>(
        ::adler32(1,
                reinterpret_cast<const Bytef*>(buf), 
                static_cast<int>(len-kHeaderLen)));
    if(checkSum == expectedCheckSum){
        int32_t nameLen = asInt32(buf);
        if(nameLen >= 2 && nameLen <= len - 2*kHeaderLen){
            std::string typeName(buf+kHeaderLen, buf+kHeaderLen+nameLen -1 );
            message.reset(CreateMessage(typeName));
       
            if(message){
                const char* data = buf+kHeaderLen + nameLen;
                int32_t dataLen = len-nameLen - 2*kHeaderLen;
                if(message->ParseFromArray(data,dataLen)){
                    *errorCode = kNoError;
                } else {
                    *errorCode = kParseError;
                }
            } else {
                *errorCode = kUnknownMessageType;
            }
        } else {
            *errorCode = kInvalidNameLen;
        }
    } else {
        *errorCode = kCheckSumError;
    }
    return message;
}