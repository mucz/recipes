#ifndef CODEC
#define CODEC
#include "TCP/TcpClient.hh"
#include "TCP/TcpConnection.hh"
#include "TCP/TcpServer.hh"
#include "Reactor/EventLoop.hh"
#include "noncopyable.hh"
#include <functional>
#include <google/protobuf/message.h>
#include <memory>

typedef std::shared_ptr<google::protobuf::Message> MessagePtr;

class ProtobufCodec : noncopyable{
    public:
        enum ErrorCode{
            kNoError = 0,
            kInvalidLength,
            kCheckSumError,
            kInvalidNameLen,
            kUnknownMessageType,
            kParseError,
        };

        typedef std::function<void(const TcpConnectionPtr&,
                                   const MessagePtr&,
                                   Timestamp)> ProtobufMessageCallback;
        
        typedef std::function<void(const TcpConnectionPtr&,
                                   Buffer*,
                                   Timestamp,
                                   ErrorCode)> ErrorCallback;
        
        ProtobufCodec(const ProtobufMessageCallback& messageCb, const ErrorCallback& errorCb = defaultErrorCallback)
            :   messageCallback_(messageCb),
                errorCallback_(errorCb)
            {   }

    
        void onMessage(const TcpConnectionPtr& conn, Buffer* buf,Timestamp receiveTime);
        void send(const TcpConnectionPtr& conn, const google::protobuf::Message& message){
            Buffer buf;
            fillEmptyBuffer(&buf,message);
            conn->send(buf);
        }

        static const std::string& errorCodeToString(ErrorCode errorCode);
        static void fillEmptyBuffer(Buffer* buf,const google::protobuf::Message& message);
        static google::protobuf::Message* CreateMessage(const std::string& type_name);
        static MessagePtr parse(const char* buf, int len,ErrorCode* errorCode);

    private:

        ProtobufMessageCallback messageCallback_;
        ErrorCallback errorCallback_;

        static void defaultErrorCallback(const TcpConnectionPtr&,
                                         Buffer*,
                                         Timestamp,
                                         ErrorCode);
        
        const static int kHeaderLen = sizeof(int32_t); 
        const static int kMinMessageLen = 2 * kHeaderLen + 2; // nameLen + typeName + checkSum
        const static int kMaxMessageLen = 64 * 1024 * 1024;
};

#endif /* CODEC */
