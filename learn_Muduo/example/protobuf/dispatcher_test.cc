#include "dispatcher.hh"
#include "query.pb.h"
#include <iostream>

using std::cout;
using std::endl;
using namespace std;

typedef std::shared_ptr<exproto::Query> QueryPtr;
typedef std::shared_ptr<exproto::Answer> AnswerPtr;

void test_dynamic_pointer_cast()
{
  ::std::shared_ptr<google::protobuf::Message> msg(new exproto::Query);
  ::std::shared_ptr<exproto::Query> query(dynamic_pointer_cast<exproto::Query>(msg));
  assert(msg && query);
  if (!query)
  {
    abort();
  }
}

void onQuery(const TcpConnectionPtr&,
             const QueryPtr& message,
             Timestamp)
{
  cout << "onQuery: " << message->GetTypeName() << endl;
}

void onAnswer(const TcpConnectionPtr&,
              const AnswerPtr& message,
              Timestamp)
{
  cout << "onAnswer: " << message->GetTypeName() << endl;
}

void onUnknownMessageType(const TcpConnectionPtr&,
                          const MessagePtr& message,
                          Timestamp)
{
  cout << "onUnknownMessageType: " << message->GetTypeName() << endl;
}

int main()
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  test_dynamic_pointer_cast();

  ProtobufDispatcher dispatcher(onUnknownMessageType);
  dispatcher.registerMessageCallback<exproto::Query>(onQuery);
  dispatcher.registerMessageCallback<exproto::Answer>(onAnswer);

  TcpConnectionPtr conn;
  Timestamp t;

  std::shared_ptr<exproto::Query> query(new exproto::Query);
  std::shared_ptr<exproto::Answer> answer(new exproto::Answer);
  std::shared_ptr<exproto::Empty> empty(new exproto::Empty);
  dispatcher.onProtobufMessage(conn, query, t);
  dispatcher.onProtobufMessage(conn, answer, t);
  dispatcher.onProtobufMessage(conn, empty, t);

  google::protobuf::ShutdownProtobufLibrary();
}

