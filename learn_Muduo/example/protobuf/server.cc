#include "codec.hh"
#include "dispatcher.hh"
#include "query.pb.h"
#include <iostream>
#include <google/protobuf/message.h> 
// 一个问答服务器，监听端口，对于Query类型的proto Messgae,回复 Answer后关闭连接，对于Answer类型的和其他未知类型的，输出信息后关闭连接
using namespace std;
using namespace std::placeholders;

typedef std::shared_ptr<exproto::Query> QueryPtr;
typedef std::shared_ptr<exproto::Answer> AnswerPtr;

typedef std::function<void(const TcpConnectionPtr&,
                        const MessagePtr&,
                        Timestamp)> ProtobufMessageCallback;

class QueryServer : noncopyable{
public:
    QueryServer(EventLoop* loop, const InetAddress& listenAddr)
    :   server_(loop,listenAddr),
        dispatcher_(std::bind(&QueryServer::onUnknownMessage,this,_1,_2,_3)),
        codec_(std::bind(&ProtobufDispatcher::onProtobufMessage,&dispatcher_,_1,_2,_3))
    {
        dispatcher_.registerMessageCallback<exproto::Query>(
            std::bind(&QueryServer::onQuery,this,_1,_2,_3)
        );
        dispatcher_.registerMessageCallback<exproto::Answer>(
            std::bind(&QueryServer::onAnswer,this,_1,_2,_3)
        );
        server_.setConnectionCallback(bind(&QueryServer::onConnection,this,_1));
        server_.setMessageCallback(bind(&ProtobufCodec::onMessage,&codec_,_1,_2,_3));
    }

    void start(){server_.start(); }

private:
    void onConnection(const TcpConnectionPtr& conn){
        cout << "QueryServer - " << conn->peerAddress().toHostPort() << " -> "
            << conn->localAddress().toHostPort() << " is "
            << (conn->connected() ? "UP" : "DOWN")<<endl;
    }

    void onQuery(const TcpConnectionPtr& conn,const QueryPtr& message, Timestamp receiveTime){
        cout<<"onQuery" << message->GetTypeName() << message->DebugString()<<endl;
        exproto::Answer answer;
        answer.set_id(1);
        answer.set_questioner("MUcz");
        answer.set_answerer("gitee/MUcz");
        answer.add_solution("Hello! this is answer 1");
        answer.add_solution("this is answer 2");
        codec_.send(conn,answer);
        conn->shutdown();
    }

    void onAnswer(const TcpConnectionPtr& conn,const AnswerPtr& message, Timestamp receiveTime){
        cout<<"onAnswer Message"<<message->GetTypeName()<<endl;
        conn->shutdown(); 
    }

    void onUnknownMessage(const TcpConnectionPtr& conn,const MessagePtr& message, Timestamp receiveTime){
        cout<<"unknown Message"<<message->GetTypeName()<<endl;
        conn->shutdown(); 
    }

    TcpServer server_;
    ProtobufDispatcher dispatcher_; 
    ProtobufCodec codec_;
};

int main(int argc, char* argv[]){
    cout<<"pid == " << getpid()<<endl;
    if(argc  > 1){
        EventLoop loop;
        uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
        InetAddress serverAddr(port);
        QueryServer server(&loop,serverAddr);
        server.start();
        loop.loop();
    } else {
        cout << "Usage: "<<argv[0]<<" <port> "<<endl;
    }
}