#include "codec.hh"
#include "dispatcher.hh"
#include "query.pb.h"
#include <iostream>
#include <google/protobuf/message.h> 
// 一个问答服务器的客户端，向服务器发送Answer询问
// 也可以通过命令行参数最后加e，向服务器发送服务器为止的包测试反应
using namespace std;
using namespace std::placeholders;

typedef std::shared_ptr<exproto::Answer> AnswerPtr;
typedef std::shared_ptr<exproto::Empty> EmptyPtr;

typedef std::function<void(const TcpConnectionPtr&,
                        const MessagePtr&,
                        Timestamp)> ProtobufMessageCallback;

google::protobuf::Message* messageToSend;

class QueryClient : noncopyable{
public:
    QueryClient(EventLoop* loop, const InetAddress& serverAddr)
    :   client_(loop,serverAddr),
        loop_(loop),
        dispatcher_(std::bind(&QueryClient::onUnknownMessage,this,_1,_2,_3)),
        codec_(std::bind(&ProtobufDispatcher::onProtobufMessage,&dispatcher_,_1,_2,_3))
    {
        dispatcher_.registerMessageCallback<exproto::Empty>(
            std::bind(&QueryClient::onEmpty,this,_1,_2,_3)
        );
        dispatcher_.registerMessageCallback<exproto::Answer>(
            std::bind(&QueryClient::onAnswer,this,_1,_2,_3)
        );
        client_.setConnectionCallback(bind(&QueryClient::onConnection,this,_1));
        client_.setMessageCallback(bind(&ProtobufCodec::onMessage,&codec_,_1,_2,_3));
    }

    void connect() { client_.connect();}
private:
    void onConnection(const TcpConnectionPtr& conn){
        cout << "QueryClient - " << conn->peerAddress().toHostPort() << " -> "
            << conn->localAddress().toHostPort() << " is "
            << (conn->connected() ? "UP" : "DOWN")<<endl;
        if(conn->connected()){
            codec_.send(conn,*messageToSend);
        } else {
            loop_->quit();
        }
    }

    void onEmpty(const TcpConnectionPtr& conn,const EmptyPtr& message, Timestamp receiveTime){
        cout<<"onEmtpy Message"<<message->GetTypeName()<<endl;
    }

    void onAnswer(const TcpConnectionPtr& conn,const AnswerPtr& message, Timestamp receiveTime){
        cout<<"onAnswer Message"<<message->GetTypeName()<<"  "<<message->DebugString()<<endl;
    }

    void onUnknownMessage(const TcpConnectionPtr& conn,const MessagePtr& message, Timestamp receiveTime){
        cout<<"unknown Message"<<message->GetTypeName()<<endl;
    }

    TcpClient client_;
    EventLoop* loop_;
    ProtobufDispatcher dispatcher_; 
    ProtobufCodec codec_;
};

int main(int argc, char* argv[]){
    cout<<"pid == " << getpid()<<endl;
    if(argc  > 1){
        EventLoop loop;
        uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
        InetAddress serverAddr("127.0.0.1",port);
        QueryClient client(&loop,serverAddr);
        
        exproto::Query query;
        query.set_id(1);
        query.set_questioner("client");
        query.add_question("Running?");

        exproto::Empty empty;
        messageToSend = &query;

        if(argc > 2 && argv[2][0] == 'e'){
            messageToSend = &empty;
        }
        client.connect();
        loop.loop();
    } else {
        cout << "Usage: "<<argv[0]<<" <port> [e]"<<endl;
    }
}