#ifndef DOWN_CAST
#define DOWN_CAST

// 在Debug模式下使用dynamic_cast检查一次，确认无误后，在Release中使用static_pointer_cast避免RTTI开销
template<typename To, typename From>
inline ::std::shared_ptr<To> down_pointer_cast(const ::std::shared_ptr<From>& f){
    #ifndef NDEBUF
        assert(f == NULL || dynamic_cast<To*>(f.get()) != NULL);
    #endif
        return ::std::static_pointer_cast<To>(f);
}

#endif /* DOWN_CAST */
