#include "codec.hh"
#include <set>
// 在server_threaded.cc的基础上借助shared_ptr实现copy-on-reuse，减少竞争
using namespace std;

class ChatServer : noncopyable{ 
    public:
        ChatServer(EventLoop* loop,const InetAddress& serverAddr)
        :   server_(loop,serverAddr),
            codec_(std::bind(&ChatServer::onStringMessage,
                   this,
                   std::placeholders::_1,
                   std::placeholders::_2,
                   std::placeholders::_3)),
            connections_(std::make_shared<ConnectionList>()),
            mutex_()
        {
            server_.setConnectionCallback(bind(&ChatServer::onConnection,this,std::placeholders::_1));
            server_.setMessageCallback(bind(&LengthHeaderCodec::onMessage,&codec_,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
        }

        void start(){ server_.start(); }

        void setThreadNum(int numThreads){server_.setThreadNum(numThreads);}

    private:
        typedef std::set<TcpConnectionPtr> ConnectionList;
        typedef std::shared_ptr<ConnectionList> ConnectionListPtr;

        void onConnection(const TcpConnectionPtr& conn){
            cout << "ChatServer - " << conn->peerAddress().toHostPort() << " -> "
                << conn->localAddress().toHostPort() << " is "
                << (conn->connected() ? "UP" : "DOWN")<<endl;

            // copy-on-reuse
            MutexLockGuard lock(mutex_);
            if(!connections_.unique()){
                connections_= std::make_shared<ConnectionList>(*connections_);
            }
            assert(connections_.unique());

            if(conn->connected()){
                connections_->insert(conn);
            } else {
                assert(connections_->count(conn)>0);
                connections_->erase(conn);
            }
        }

        void onStringMessage(const TcpConnectionPtr& conn,const string& message, Timestamp receiveTime){
            auto connections = getConnectionList();
            for(auto it = connections->begin(); it!= connections->end() ;++it){
                codec_.send(*it,message);
            }
        }
        ConnectionListPtr getConnectionList()
        {
            MutexLockGuard lock(mutex_);
            return connections_;
        }

        TcpServer server_;
        LengthHeaderCodec codec_;
        // ConnnectionList connections_;
        ConnectionListPtr connections_;
        MutexLock mutex_;
};

int main(int argc, char* argv[]){
    cout<<"pid == " << getpid()<<endl;
    if(argc  > 1){
        EventLoop loop;
        uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
        InetAddress serverAddr(port);
        ChatServer server(&loop,serverAddr);
        if(argc > 2){
            server.setThreadNum(atoi(argv[2]));
        }
        server.start();
        loop.loop();
    } else {
        cout << "Usage: "<<argv[0]<<" <port> "<<endl;
    }
}