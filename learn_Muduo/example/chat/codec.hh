#ifndef CODEC
#define CODEC
#include "TCP/TcpClient.hh"
#include "TCP/TcpConnection.hh"
#include "TCP/TcpServer.hh"
#include "Reactor/EventLoop.hh"
#include "noncopyable.hh"
#include <functional>
#include <string>
#include <string_view>
#include <iostream>
#include <string>

// 聊天服务器使用的协议的coder&decoder
// □□□□  ■■■■■■■■■■■....
// len   <texts to send>
// int32 char 
class LengthHeaderCodec : noncopyable{
    public:
        typedef std::function<void(const TcpConnectionPtr&,
                                   const std::string&,
                                   Timestamp)> StringMessageCallback;
        explicit LengthHeaderCodec(const StringMessageCallback& cb) : messageCallback_(cb) { }
        void onMessage(const TcpConnectionPtr& conn, Buffer* buf,Timestamp receiveTime){
            while(buf->readableBytes() >= kHeaderLen ){
                int32_t len = buf->peekInt32();
                if(len > 65536 || len< 0){
                    fprintf(stderr,"Codec::onMessage invalide length\n");
                    abort();
                } else if(buf->readableBytes() >= len+kHeaderLen){
                    buf->retrieveInt32();
                    std::string message = buf->retrieveAsString(len);
                    messageCallback_(conn,message,receiveTime);
                } else {
                    break;
                }
            }
        }

        void send(TcpConnectionPtr conn, const std::string_view& message){
            Buffer buf;
            buf.append(message.data(),message.size());
            int32_t len = static_cast<int32_t>(message.size());
            buf.prependInt32(len);
            conn->send(buf);
        }
    private:
        StringMessageCallback messageCallback_;
        const static size_t kHeaderLen = sizeof(int32_t);
};

#endif /* CODEC */


