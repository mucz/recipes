#include "codec.hh"
#include <set>
// server.cc 的基础上使用了多线程，能够更好的利用多核，性能得到提升，线程间共享的对象使用锁保护
using namespace std;

class ChatServer : noncopyable{ 
    public:
        ChatServer(EventLoop* loop,const InetAddress& serverAddr)
        :   server_(loop,serverAddr),
            codec_(std::bind(&ChatServer::onStringMessage,
                   this,
                   std::placeholders::_1,
                   std::placeholders::_2,
                   std::placeholders::_3)),
            mutex_()
        {
            server_.setConnectionCallback(bind(&ChatServer::onConnection,this,std::placeholders::_1));
            server_.setMessageCallback(bind(&LengthHeaderCodec::onMessage,&codec_,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
        }

        void start(){ server_.start(); }

        // +
        void setThreadNum(int numThreads){server_.setThreadNum(numThreads);}
        // 

    private:

        void onConnection(const TcpConnectionPtr& conn){
            cout << "ChatServer - " << conn->peerAddress().toHostPort() << " -> "
                << conn->localAddress().toHostPort() << " is "
                << (conn->connected() ? "UP" : "DOWN")<<endl;
            // +
            MutexLockGuard lock(mutex_);
            // 
            if(conn->connected()){
                connections_.insert(conn);
            } else {
                assert(connections_.count(conn)>0);
                connections_.erase(conn);
            }
        }

        void onStringMessage(const TcpConnectionPtr& conn,const string& message, Timestamp receiveTime){
            // + 
            MutexLockGuard lock(mutex_);
            // 
            for(auto it = connections_.begin(); it!= connections_.end() ;++it){
                codec_.send(*it,message);
            }
        }

        typedef std::set<TcpConnectionPtr> ConnnectionList;
        TcpServer server_;
        LengthHeaderCodec codec_;
        ConnnectionList connections_;
        // +
        MutexLock mutex_;
        // 
};

int main(int argc, char* argv[]){
    cout<<"pid == " << getpid()<<endl;
    if(argc  > 1){
        EventLoop loop;
        uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
        InetAddress serverAddr(port);
        ChatServer server(&loop,serverAddr);
        // +
        if(argc > 2){
            server.setThreadNum(atoi(argv[2]));
        }
        // 
        server.start();
        loop.loop();
    } else {
        cout << "Usage: "<<argv[0]<<" <port> "<<endl;
    }
}