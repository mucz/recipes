#include "codec.hh"
#include "Reactor/EventLoopThread.hh"
#include <set>
// client端，单线程
using namespace std;
using namespace std::placeholders;
class ChatClient : noncopyable{
    public:
        ChatClient(EventLoop* loop,InetAddress serverAddr)
        :   connection_(),
            client_(loop,serverAddr),
            codec_(bind(&ChatClient::onStringMessage,this,_1,_2,_3)),
            mutex_()
        {
            client_.setConnectionCallback(bind(&ChatClient::onConnection,this,_1));
            client_.setMessageCallback(bind(&LengthHeaderCodec::onMessage,&codec_,_1,_2,_3));
            client_.enableRetry();
        }
        void connect() { client_.connect();}
        void disconnect() { client_.disconnect(); }
        void write(const string_view& message){
            MutexLockGuard lock(mutex_);
            if(connection_){
                codec_.send(connection_,message);
            } 
        }
    private:
        void onConnection(const TcpConnectionPtr& conn){
            cout<< conn->localAddress().toHostPort() << "-> " << conn->peerAddress().toHostPort() <<" is "<<(conn->connected()? "UP":"DOWN")<<endl;
            MutexLockGuard lock(mutex_);
            if(conn->connected()){
                connection_ = conn;
            } else {
                connection_ .reset();
            }
        }

        void onStringMessage(const TcpConnectionPtr& conn,const string& message ,Timestamp receiveTime){
            cout<<message<<endl;
        }
        TcpConnectionPtr connection_; // guared_by mutex_
        TcpClient client_;
        LengthHeaderCodec codec_;
        MutexLock mutex_;
};

int main(int argc, char* argv[]){
    cout<<"pid == " << getpid()<<endl;
    if(argc  > 1){
        EventLoopThread loopThread;
        uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
        InetAddress serverAddr("127.0.0.1",port);
        ChatClient client(loopThread.startloop(),serverAddr);
        client.connect();
        string line;
        while(getline(cin,line)){
            client.write(line);
        }
        client.disconnect();
    } else {
        cout << "Usage: "<<argv[0]<<" <port> "<<endl;
    }
}