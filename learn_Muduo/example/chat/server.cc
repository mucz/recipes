#include "codec.hh"
#include <set>
// 一个聊天服务器的服务端,转发收到的消息给所有连接者
using namespace std;

class ChatServer : noncopyable{ 
    public:
        ChatServer(EventLoop* loop,const InetAddress& serverAddr)
        :   server_(loop,serverAddr),
            codec_(std::bind(&ChatServer::onStringMessage,
                   this,
                   std::placeholders::_1,
                   std::placeholders::_2,
                   std::placeholders::_3))
        {
            server_.setConnectionCallback(bind(&ChatServer::onConnection,this,std::placeholders::_1));
            server_.setMessageCallback(bind(&LengthHeaderCodec::onMessage,&codec_,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
        }

        void start(){ server_.start(); }

    private:

        void onConnection(const TcpConnectionPtr& conn){
            cout << "ChatServer - " << conn->peerAddress().toHostPort() << " -> "
                << conn->localAddress().toHostPort() << " is "
                << (conn->connected() ? "UP" : "DOWN")<<endl;
            if(conn->connected()){
                connections_.insert(conn);
            } else {
                assert(connections_.count(conn)>0);
                connections_.erase(conn);
            }
        }

        void onStringMessage(const TcpConnectionPtr& conn,const string& message, Timestamp receiveTime){
            for(auto it = connections_.begin(); it!= connections_.end() ;++it){
                codec_.send(*it,message);
            }
        }

        typedef std::set<TcpConnectionPtr> ConnnectionList;
        TcpServer server_;
        LengthHeaderCodec codec_;
        ConnnectionList connections_;
};

int main(int argc, char* argv[]){
    cout<<"pid == " << getpid()<<endl;
    if(argc  > 1){
        EventLoop loop;
        uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
        InetAddress serverAddr(port);
        ChatServer server(&loop,serverAddr);
        server.start();
        loop.loop();
    } else {
        cout << "Usage: "<<argv[0]<<" <port> "<<endl;
    }
}