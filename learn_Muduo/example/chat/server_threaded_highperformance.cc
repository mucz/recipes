#include "codec.hh"
#include "Thread/ThreadLocalSingleton.hh"
#include <set>
// 在server_threaded.cc的基础上借助thread local变量+thread初始化函数，减少线程间共享的对象，实现多线程高效转发
using namespace std;
using namespace std::placeholders;


class ChatServer : noncopyable{ 
    public:
        ChatServer(EventLoop* loop,const InetAddress& serverAddr)
        :   server_(loop,serverAddr),
            codec_(std::bind(&ChatServer::onStringMessage,
                   this,
                   std::placeholders::_1,
                   std::placeholders::_2,
                   std::placeholders::_3))
        {
            server_.setConnectionCallback(bind(&ChatServer::onConnection,this,std::placeholders::_1));
            server_.setMessageCallback(bind(&LengthHeaderCodec::onMessage,&codec_,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
        }

        void start(){
            server_.setThreadInitCallback(bind(&ChatServer::threadInit,this,_1)); 
            server_.start();
        }

        void setThreadNum(int numThreads){server_.setThreadNum(numThreads);}

    private:
        typedef std::set<TcpConnectionPtr> ConnectionList;

        void onConnection(const TcpConnectionPtr& conn){
            cout << "ChatServer - " << conn->peerAddress().toHostPort() << " -> "
                << conn->localAddress().toHostPort() << " is "
                << (conn->connected() ? "UP" : "DOWN")<<endl;

            if(conn->connected()){
                LocalConnections::instance().insert(conn);
            } else {
                LocalConnections::instance().erase(conn);
            }
        }

        void onStringMessage(const TcpConnectionPtr& conn,const string& message, Timestamp receiveTime){
            auto f = std::bind(&ChatServer::distributeMessage,this,message);
            MutexLockGuard lock(mutex_);
            for(auto it = loops_.begin(); it != loops_.end(); ++it){
                (*it)->queueInLoop(f);
            }
        }

        void distributeMessage(const string& message){
            for(auto it = LocalConnections::instance().begin(); it!=LocalConnections::instance().end(); ++it){
                codec_.send(*it,message);
            }
        }
        void threadInit(EventLoop* loop){ //每个线程初始化线程局部变量 + 主线程登记子线程(通过threadInit获得EventLoop* )
            assert(LocalConnections::pointer() == NULL);
            LocalConnections::instance();
            assert(LocalConnections::pointer() != NULL);
            MutexLockGuard lock(mutex_);
            loops_.insert(loop);
        }

        TcpServer server_;
        LengthHeaderCodec codec_;
        typedef ThreadLocalSingleton<ConnectionList> LocalConnections;
        MutexLock mutex_;
        std::set<EventLoop*> loops_;
};

int main(int argc, char* argv[]){
    cout<<"pid == " << getpid()<<endl;
    if(argc  > 1){
        EventLoop loop;
        uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
        InetAddress serverAddr(port);
        ChatServer server(&loop,serverAddr);
        if(argc > 2){
            server.setThreadNum(atoi(argv[2]));
        }
        server.start();
        loop.loop();
    } else {
        cout << "Usage: "<<argv[0]<<" <port> "<<endl;
    }
}