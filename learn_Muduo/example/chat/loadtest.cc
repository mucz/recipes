#include "codec.hh"
#include "Reactor/EventLoopThreadPool.hh"
// 测试不同server的收到信息的延迟
//  `./loadtest <port> <connections> [Threads]`
// 创建[Threads]个线程，向本机<port>口发送共<connections>个连接请求，所有连接都建立之后，发送一条消息到server，所有客户端都收到该消息的时间反映了最大的延迟
using namespace std;
using namespace std::placeholders;


int g_connections = 0;
AtomicInt32 g_aliveConnections;
AtomicInt32 g_messagesReceived;
Timestamp g_startTime;
std::vector<Timestamp> g_receiveTime;
EventLoop* g_loop;
std::function<void()> g_statistic;
class ChatClient : noncopyable{
    public:
        ChatClient(EventLoop* loop,InetAddress serverAddr)
        :   connection_(),
            client_(loop,serverAddr),
            codec_(bind(&ChatClient::onStringMessage,this,_1,_2,_3)),
            loop_(loop)
        {
            client_.setConnectionCallback(bind(&ChatClient::onConnection,this,_1));
            client_.setMessageCallback(bind(&LengthHeaderCodec::onMessage,&codec_,_1,_2,_3));
        }
        void connect() { client_.connect();}
        void disconnect() { client_.disconnect(); }
        Timestamp receiveTime() const {return receiveTime_;}

    private:
        void onConnection(const TcpConnectionPtr& conn){
            // cout<< conn->localAddress().toHostPort() << "-> " << conn->peerAddress().toHostPort() <<" is "<<(conn->connected()? "UP":"DOWN")<<endl;

            if(conn->connected()){
                connection_ = conn;
                if(g_aliveConnections.incrementAndGet() == g_connections){
                    cout<<"all connected! "<<endl;
                    loop_->runAfter(10.0,bind(&ChatClient::send,this));
                }
            } else {
                connection_ .reset();
            }
        }

        void onStringMessage(const TcpConnectionPtr& conn,const string& message ,Timestamp receiveTime){
            receiveTime_ = loop_->pollReturnTime();
            int received = g_messagesReceived.incrementAndGet();
            if(received == g_connections){
                Timestamp endTime = Timestamp::now();
                cout<<"all recieved "<<g_connections<<" in "<<timeDifference(endTime,g_startTime)<<endl;
                g_loop->queueInLoop(g_statistic);
            }
        }

        void send(){
            g_startTime = Timestamp::now();
            codec_.send(connection_,"hello");
            cout<<"sent"<<endl;
        }

        TcpConnectionPtr connection_;
        TcpClient client_;
        LengthHeaderCodec codec_;
        EventLoop* loop_;
        Timestamp receiveTime_;
};

void statistic(const std::vector<std::unique_ptr<ChatClient>>& clients){
    cout<<"Statistic "<<clients.size()<<endl;
    std::vector<double> seconds(clients.size());
    for(size_t i = 0; i<clients.size() ;++i){
        seconds[i] = timeDifference(clients[i]->receiveTime(),g_startTime);
    }
    std::sort(seconds.begin(), seconds.end());
    for (size_t i = 0; i < clients.size(); i += std::max(static_cast<size_t>(1), clients.size()/20)) {
        printf("%6zd%% %.6f\n", i*100/clients.size(), seconds[i]);
    }
    if (clients.size() >= 100){
        printf("%6d%% %.6f\n", 99, seconds[clients.size() - clients.size()/100]);
    }
    printf("%6d%% %.6f\n", 100, seconds.back());
    exit(1);
}

int main(int argc, char* argv[]){
    cout<<"pid = "<<getpid()<<endl;
    if(argc > 2){
        uint16_t port = static_cast<uint16_t>(atoi(argv[1]));
        InetAddress serverAddr("127.0.0.1",port);
        g_connections = atoi(argv[2]);
        int threads = 0;
        if(argc >3 ){
            threads = atoi(argv[3]);
        }

        EventLoop loop;
        g_loop = &loop;
        EventLoopThreadPool loopPool(&loop,"chat-loadtest");
        loopPool.setThreadNum(threads);
        loopPool.start();

        g_receiveTime.reserve(g_connections);
        std::vector<std::unique_ptr<ChatClient>> clients(g_connections);
        g_statistic = std::bind(statistic,std::ref(clients));
        for(int i = 0; i<g_connections; ++i){
            clients[i]=(make_unique<ChatClient>(loopPool.getNextLoop(),serverAddr));
            clients[i]->connect();
            usleep(200);
        }
        loop.loop();
    } else {
        printf("Usage: %s <port> <connections> [threads] \n",argv[0]);
    }
}