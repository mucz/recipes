// download.cc
// 通过命令行参数指定要发送的文件，在2021端口监听，每当有新连接进来，就把文件完整的发给对方
// 特性：
//      并发性
//      内存消耗只与并发连接数有关，与文件大小无关，
//      客户可以随时断开，没有内存泄漏或崩溃
#include "TCP/TcpClient.hh"
#include "TCP/TcpConnection.hh"
#include "TCP/TcpServer.hh"
#include "Reactor/EventLoop.hh"
#include <iostream>
#include <string>
using namespace std;
void onHighWaterMark(const TcpConnectionPtr& conn, size_t len){ 
   cout<<"HighWaterMark" <<len<<endl;
}

const int kBufSize = 64* 1024;
const char* g_file = NULL;
typedef std::shared_ptr<FILE> FilePtr;

void onConnection(const TcpConnectionPtr& conn){ 
  cout << "FileServer - " << conn->peerAddress().toHostPort() << " -> "
           << conn->localAddress().toHostPort() << " is "
           << (conn->connected() ? "UP" : "DOWN")<<endl;
  if (conn->connected())
  {
    cout << "FileServer - Sending file " << g_file
             << " to " << conn->peerAddress().toHostPort();
    conn->setHighWaterCallback(onHighWaterMark, kBufSize+1);

    FILE* fp = ::fopen(g_file, "rb");
    if (fp)
    {
      FilePtr ctx(fp, ::fclose);
      conn->setContext(ctx);
      char buf[kBufSize];
      size_t nread = ::fread(buf, 1, sizeof buf, fp);
      conn->send(buf, static_cast<int>(nread));
    }
    else
    {
      conn->shutdown();
      cout << "FileServer - no such file"<<endl;
    }
  }
}

void onWriteComplete(const TcpConnectionPtr& conn){
    const FilePtr& fp = std::any_cast<const FilePtr&>(conn->getContext());
    char buf[kBufSize];
    size_t nread = ::fread(buf,1,sizeof(buf),fp.get());
    if(nread>0){
        conn->send(buf,static_cast<int> (nread));
    } else {
        conn->shutdown();
        cout<<"FileServer - done"<<endl;
    }
}

int main(int argc, char* argv[]){
    cout<<"pid = "<<getpid()<<endl;
    if(argc>1){
        g_file = argv[1];
        EventLoop loop;
        InetAddress listenAddr(2021);
        TcpServer server(&loop,listenAddr);
        server.setConnectionCallback(onConnection);
        server.setWriteCompleteCallback(onWriteComplete);
        server.start();
        loop.loop();
    } else {
        cout<<"Usage:"<<argv[0]<<" <file_to_transfer> "<<endl;
    }
}