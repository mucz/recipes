// A thread-safe counter
// ! 实际项目中，使用原子操作更加合理，这里锁只是为了举例
#include "Thread/Mutex.hh"

class Counter : noncopyable{
private:
    int64_t value_;
    mutable MutexLock mutex_; // enable use mutex_ in const member func.
public:
    Counter(): value_(0) {}
    int64_t value() const ;
    int64_t getAndIncrease();
};

int64_t Counter::value() const{
    MutexLockGuard lock(mutex_);
    return value_;
}

int64_t Counter::getAndIncrease() {
    MutexLockGuard lock(mutex_);
    auto ret = value_++;
    return ret;
}