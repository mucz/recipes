#ifndef TIMESTAMP_HH
#define TIMESTAMP_HH
#include <stdint.h>
#include <sys/time.h> 
#include <stdio.h>
#include <string>
#include <inttypes.h>

class Timestamp{
private:
    int64_t microSecondsSinceEpoche_;
public:
    Timestamp();
    Timestamp(int64_t microSecondsSinceEpocheArg);
    int64_t microSecondsSinceEpoche() const {return microSecondsSinceEpoche_; }
    static Timestamp now();
    static const int kMicroSecondsPerSecond = 1000 * 1000;

    inline bool operator< (const Timestamp rhs) const 
    { return microSecondsSinceEpoche() < rhs.microSecondsSinceEpoche(); }
    inline bool operator== (const Timestamp rhs) const 
    { return microSecondsSinceEpoche() == rhs.microSecondsSinceEpoche(); } 

    std::string toString() const;
};

inline Timestamp addTime(Timestamp timestamp, double seconds){
    int64_t delta = static_cast<int64_t>(seconds * Timestamp::kMicroSecondsPerSecond);
    return Timestamp(timestamp.microSecondsSinceEpoche() + delta);
};

inline double timeDifference(Timestamp high,Timestamp low){
    int64_t diff = high.microSecondsSinceEpoche() - low.microSecondsSinceEpoche();
    return static_cast<double>(diff) / Timestamp::kMicroSecondsPerSecond;
}
#endif //TIMESTAMP_HH