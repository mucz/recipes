#include "Poller.hh"
#include <algorithm>

Poller::Poller(EventLoop* loop): ownerLoop_(loop){ }

Timestamp Poller::poll(int timeoutMs, ChannelList* activeChannels){
    int numEvents = ::poll(&*pollfds_.begin(),pollfds_.size(),timeoutMs);
    Timestamp now(Timestamp::now());
    if(numEvents > 0){
        // numEvents events happended
        fillActiveChannles(numEvents, activeChannels);
    } else if (numEvents == 0){
        // nothing happens 
    } else {
        fprintf(stderr,"poll() error");
    }
    return now;
};

void Poller::fillActiveChannles(int numEvents, ChannelList* activeChannels) const { 
    for(auto itr_fd = pollfds_.begin(); itr_fd != pollfds_.end() && numEvents > 0 ; ++ itr_fd){
        if(itr_fd->revents > 0){
            --numEvents;
            const auto ch = channels_.find(itr_fd->fd);
            assert(ch != channels_.end());
            auto* channel = ch->second;
            channel->set_revents(itr_fd->revents);
            activeChannels->push_back(channel);
        }
    }
}

void Poller::updateChannel(Channel* channel){
    assertInLoopThread();
    if(channel->index() < 0){ // 尚未注册,加入pollfds_中
        assert(channels_.find(channel->fd()) == channels_.end());
        struct pollfd pfd;
        pfd.fd = channel->fd();
        pfd.events = channel->events();
        pfd.revents = 0;
        pollfds_.push_back(pfd);
        int idx = pollfds_.size()-1;
        channel->set_index(idx);
        channels_[pfd.fd] = channel;
    } else { // 已经注册，更改注册信息
        assert(channels_.find(channel->fd()) != channels_.end());
        assert(channels_[channel->fd()] == channel);
        int idx = channel->index();
        assert(idx >= 0 && idx < static_cast<int> (pollfds_.size()));
        struct pollfd& pfd = pollfds_[idx];
        assert(pfd.fd == channel->fd() || pfd.fd < 0 );
        pfd.events = channel->events();
        pfd.revents = 0;
        if(channel->isNoneEvent()){ // ignore this pollfd
            pfd.fd = -pfd.fd-1;
        }
    }
}

void Poller::removeChannel(Channel* channel){
    assertInLoopThread();
    assert(channels_.find(channel->fd())!=channels_.end());
    assert(channels_[channel->fd()]==channel);
    assert(channel->isNoneEvent());// why?

    int idx = channel->index();
    assert(0 <= idx && idx < static_cast<int>(pollfds_.size()));
    size_t n = channels_.erase(channel->fd());
    assert(n==1); (void)n;
    if(static_cast<size_t>(idx) == pollfds_.size() - 1){
        pollfds_.pop_back();
    } else {
        int channelAtEnd = pollfds_.back().fd;
        std::swap<pollfd>(*(pollfds_.begin()+idx),*(pollfds_.end()-1));
        if(channelAtEnd < 0 ){ 
            channelAtEnd = -channelAtEnd-1;
        }
        channels_[channelAtEnd]->set_index(idx);
        pollfds_.pop_back();
    }
}