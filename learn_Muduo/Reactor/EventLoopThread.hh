// EventLoopThread.hh
#ifndef  EVENTLOOPTHREAD_HH
#define EVENTLOOPTHREAD_HH
#include "../Thread/Mutex.hh"
#include "../Thread/Thread.hh"
#include "noncopyable.hh"

class EventLoop;
// a thread contains a eventloop
class EventLoopThread : noncopyable{
public:
    typedef std::function<void(EventLoop*)> ThreadInitCallback;
    EventLoopThread(const ThreadInitCallback &cb = ThreadInitCallback());
    ~EventLoopThread();
    // starts the loop and returns the pointer to the loop
    EventLoop* startloop(); 

private:
    // bootstrap func for new thread, create and starts a loop
    void threadFunc();
    
    EventLoop* loop_;
    bool existing_;
    Thread thread_;
    MutexLock mutex_;
    Condition cond_;
    ThreadInitCallback initCallback_;
};
#endif // EVENTLOOPTHREAD_HH
