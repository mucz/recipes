#include "EventLoopThreadPool.hh"
#include "EventLoop.hh"
#include "EventLoopThread.hh"

EventLoopThreadPool::EventLoopThreadPool(EventLoop* baseLoop,const std::string nameArg)
    :   baseLoop_(baseLoop),
        name_(nameArg),
        started_(false),
        numThreads_(0),
        next_(0)
{
}

EventLoopThreadPool::~EventLoopThreadPool(){
}

void EventLoopThreadPool::start(const ThreadInitCallback& cb){
    assert(!started_);
    baseLoop_->assertInLoopThread();

    started_ = true;

    for(int i = 0 ; i < numThreads_; ++i){
        std::unique_ptr<EventLoopThread> t = std::make_unique<EventLoopThread>(cb);
        loops_.push_back(t->startloop());
        threads_.push_back(move(t));
    }
    if(cb && numThreads_ == 0){
        cb(baseLoop_);
    }
}

EventLoop* EventLoopThreadPool::getNextLoop(){
    baseLoop_->assertInLoopThread();
    EventLoop* loop = baseLoop_;
    if(!loops_.empty()){
        // round-robin
        loop = loops_[next_];
        ++next_;
        if(static_cast<size_t>(next_) >= loops_.size()){
            next_ = 0;
        }
    }
    return loop;
}