#ifndef EPOLLER
#define EPOLLER
#include <map> 
#include <vector> 
#include "Timestamp.hh"
#include "EventLoop.hh"
#include "noncopyable.hh"


struct epoll_event;
class Channel;

class Epoller : noncopyable{
public:
    typedef std::vector<Channel*> ChannelList;
    Epoller(EventLoop* loop);
    ~Epoller();
    
    Timestamp poll(int timeoutMs, ChannelList* activeChannels);

    void updateChannel(Channel* channel);
    void removeChannel(Channel* channel);

    void assertInLoopThread() {ownerLoop_->assertInLoopThread();}

private:
    static const int kInitEventListSize = 16;
    void fillActiveChannels(int numEvents,ChannelList* activeChannels) const;
    void update(int operation,Channel* channel);
    typedef std::vector<struct epoll_event> EventList;
    typedef std::map<int,Channel*> ChannelMap;

    EventLoop* ownerLoop_;
    int epollfd_;
    EventList events_;
    ChannelMap channels_;
};

#endif /* EPOLLER */
