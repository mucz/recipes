#ifndef EVENT_LOOP_HH
#define EVENT_LOOP_HH
// 一个什么也不干的EventLoop
#include "../Thread/Thread.hh"
#include <memory>
#include <poll.h>
#include <vector>
#include "TimerQueue.hh"

// class Poller;
class Epoller;
class Channel;

class EventLoop : noncopyable{
public: 
    typedef std::function<void()> Functor;
    typedef std::function<void()> TimerCallback;
    EventLoop();
    ~EventLoop();
    
    void loop();
    void quit();

    void assertInLoopThread(){
        if(!isInLoopThread()){
            fprintf(stderr,"Loop called not in the loop that it don't belong to\n");
            abortNotInLoopThread();
        }
    }
    bool isInLoopThread() const {return threadId_ == CurrentThread::tid(); }

    // for channel : update its config.
    void updateChannel(Channel* channel);
    // remove a channel
    void removeChannel(Channel* channel);
    // run a funtor in Loop's thread, allow cross-thread safety
    void runInLoop(const Functor& cb);
    // called by runInLoop, queue the functors to be called in loop's thread
    void queueInLoop(const Functor& cb);

    // Timer-related task
    std::weak_ptr<Timer> runAt(const Timestamp& time, const TimerCallback& cb);
    std::weak_ptr<Timer> runAfter(double delay, const TimerCallback& cb);
    std::weak_ptr<Timer> runEvery(double interval, const TimerCallback& cb);

    // one loop per thread 
    static EventLoop* getEventLoopOfCurrentThread();

    void cancel(std::weak_ptr<Timer>& timer_weak_p);

    Timestamp pollReturnTime() const {return pollReturnTime_;}
private:
    typedef std::vector<Channel*> ChannelList;
    // abort()
    void abortNotInLoopThread();
    // write to a channel to wakeup the loop to run the pending functors
    void wakeup();
    // handle the wakeup channel events, avoid buffer overflow
    void handleRead();
    // called every loop
    void doPendingFunctors();    

    bool looping_;
    bool quit_;
    const pid_t threadId_;
    Timestamp pollReturnTime_;
    std::unique_ptr<Epoller> poller_;
    ChannelList activeChannels_;

    std::unique_ptr<TimerQueue> timerQueue_;
    int wakeupFd_;  // fd of wakeup channel 
    std::unique_ptr<Channel> wakeupChannel_;  // allow wakeup() 
    std::vector<Functor> pendingFunctors_;
    bool callingPendingFunctors_;
    MutexLock mutex_;
    static const int kPollTimeMs = 10000; 
};


#endif // EVENT_LOOP_HH