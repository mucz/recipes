
#ifndef POLLER_HH
#define POLLER_HH
#include <vector>
#include <map>
#include <poll.h>
#include "Channel.hh"
#include "EventLoop.hh"

class Poller: noncopyable{ 
public:
    typedef std::vector<Channel*> ChannelList;
    Poller(EventLoop* loop);
    // poll 的封装，阻塞等待事件发生，等待结果返回
    Timestamp poll(int timeoutMs, ChannelList* activeChannels);
    // 维护pollfds_列表
    void updateChannel(Channel* channel);
    // 判断运行线程
    void assertInLoopThread() {ownerLoop_->assertInLoopThread();}
    // 移除一个Channel
    void removeChannel(Channel* channel);
private:
    // 将poll的结果写入activeChannels中
    void fillActiveChannles(int numEvents, ChannelList* activeChannels) const;
    typedef std::vector<struct pollfd> PollFdList;
    typedef std::map<int,Channel*> ChannelMap;

    EventLoop* ownerLoop_;
    PollFdList pollfds_;
    ChannelMap channels_;
};
#endif //POLLER_HH