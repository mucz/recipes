#ifndef CHANNEL_HH
#define CHANNEL_HH
// #include "EventLoop.hh"
#include <poll.h> 
#include <functional>
#include "Timestamp.hh"
#include "noncopyable.hh"
// One channl belongs to one IO Thread, and only takes care of one fd

class EventLoop;
class Channel : noncopyable{
public:
    typedef std::function<void()> EventCallback;
    typedef std::function<void(Timestamp)> ReadEventCallback;
    Channel(EventLoop* loop, int fd);

    // distrubute the IO event to related callback
    void handleEvent(Timestamp receiveTime);

    // set Event Callback
    void setWriteCallback(const EventCallback& cb) { writeCallback_ = cb; }
    void setErrorCallback(const EventCallback& cb) { errorCallback_ = cb; }
    void setReadCallback(const ReadEventCallback& cb){ readCallback_ = cb; }
    void setCloseCallback(const EventCallback& cb) {closeCallback_ = cb; }

    int fd() const { return fd_; }
    int events() const { return events_; }
    void set_revents(int revt) { revents_ = revt; }
    bool isNoneEvent() const {return events_ == kNoneEvent; }

    void enableReading(){ events_ |= kReadEvent; update();}
    void enableWriting(){ events_ |= kWriteEvent; update();}
    void disableWriting() { events_ &= ~kWriteEvent;update();}
    void disableAll() { events_ = kNoneEvent; update(); }
    bool isWriting() const {return events_&kWriteEvent;}

    // for poller
    int index() { return index_; }
    void set_index(int idx){ index_ = idx;}

    EventLoop* ownerLoop() { return loop_;}

    void remove();

private:
    // make EventLoop update the config. of this channel
    void update();

    static const int kNoneEvent;
    static const int kReadEvent;
    static const int kWriteEvent;

    EventLoop* loop_;
    const int fd_;
    int events_;
    int revents_;
    int index_;
    bool eventHandling_;
    ReadEventCallback readCallback_;
    EventCallback writeCallback_;
    EventCallback errorCallback_;
    EventCallback closeCallback_;
};
#endif // CHANNEL_HH