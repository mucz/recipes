// EventLoopThread.cc
#include "EventLoopThread.hh" 
#include "EventLoop.hh"

EventLoopThread::EventLoopThread(const ThreadInitCallback& cb)
: loop_(nullptr),
  existing_(false),
  thread_(std::bind(&EventLoopThread::threadFunc,this)),
  mutex_(),
  cond_(mutex_),
  initCallback_(cb)
  {
  }

EventLoopThread::~EventLoopThread()
{
    // ? existing_ = true;
    loop_->quit();
    // ! loop_ is now an invalid ptr 
    thread_.join(); // waits till thread_ is finised
}

EventLoop* EventLoopThread::startloop(){
    assert(!thread_.started());
    thread_.start();
    // loop should be returned after intialisation
    {
        MutexLockGuard lock(mutex_);
        while(loop_ == nullptr){
            cond_.wait();
        }
    }
    return loop_;
}

void EventLoopThread::threadFunc(){ // main func of the subthread created
    EventLoop loop;
    if(initCallback_){ // allow main thread to do something with the sub-loop-thread
        initCallback_(&loop);
    }
    {
        MutexLockGuard lock(mutex_);
        loop_ = &loop;
        cond_.notifyAll();
    }
    loop.loop();
    MutexLockGuard lock(mutex_);
    loop_ = nullptr;
}