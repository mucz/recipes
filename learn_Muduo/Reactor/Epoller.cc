#include "Epoller.hh"
#include "Channel.hh"
#include <poll.h>
#include <sys/epoll.h>

static_assert(EPOLLIN == POLLIN);
static_assert(EPOLLPRI == POLLPRI);
static_assert(EPOLLOUT == POLLOUT);
static_assert(EPOLLRDHUP == POLLRDHUP);
static_assert(EPOLLERR == POLLERR);
static_assert(EPOLLHUP == POLLHUP);


const int kNew = -1;
const int kAdded  = 1;
const int kDeleted = 2;


Epoller::Epoller(EventLoop* loop)
    :   ownerLoop_(loop),
        epollfd_(::epoll_create1(EPOLL_CLOEXEC)),
        events_(kInitEventListSize)
        {
            if(epollfd_<0){
                fprintf(stderr,"Epoller::Epoller eppllfd_<0\n");
                abort();
            }
        }

Epoller::~Epoller(){
    ::close(epollfd_);
}

Timestamp Epoller::poll(int timeoutMs, ChannelList* activeChannels){
    int numEvents = ::epoll_wait(epollfd_,&*events_.begin(),static_cast<int>(events_.size()),timeoutMs);
    int savedErrno = errno;
    Timestamp now(Timestamp::now());
    if(numEvents > 0){
        // numEvents events happended
        fillActiveChannels(numEvents, activeChannels);
        if(static_cast<size_t>(numEvents)== events_.size()){
            events_.resize(events_.size()*2);
        }
    } else if (numEvents == 0){
        // nothing happens 
    } else {
        fprintf(stderr,"epoll() error: numEvents == %d ",numEvents);
        if(savedErrno == EBADF) 
            fprintf(stderr,"errno = EBADF\n");
        if(savedErrno == EFAULT)
            fprintf(stderr,"errno = EFAULT\n");
        if(savedErrno == EINTR)
            fprintf(stderr,"errno = EINTR\n");
        if(savedErrno == EINVAL)
            fprintf(stderr,"errno = EINVAL\n");
    }
    return now;
}

void Epoller::fillActiveChannels(int numEvents, ChannelList* activeChannels) const {
    assert(static_cast<size_t>(numEvents) <=events_.size());
    for(int i = 0; i<numEvents; ++i){
        Channel* channel = static_cast<Channel*> (events_[i].data.ptr);
        int fd = channel->fd();
        const auto it = channels_.find(fd);
        assert(it != channels_.end());
        assert(it->second == channel);
        channel->set_revents(events_[i].events);
        activeChannels->push_back(channel);
    }
}

void Epoller::updateChannel(Channel* channel){
    assertInLoopThread();
    const int index = channel->index();
    if(index == kNew || index == kDeleted){
        // a new one, add with EPOLL_CTL_ADD
        int fd = channel->fd(); 
        if(index == kNew){
            assert(channels_.find(fd) == channels_.end());
            channels_[fd]=channel;
        } else {
            assert(channels_.find(fd) != channels_.end());
            assert(channels_[fd] == channel);
        }
        channel->set_index(kAdded);
        update(EPOLL_CTL_ADD,channel);
    } else {
        // update existing one with EPOLL_CTL_MOD/DEL
        int fd = channel->fd(); (void)fd;
        assert(channels_.find(fd) != channels_.end());
        assert(channels_[fd] == channel);
        assert(index == kAdded);
        if(channel->isNoneEvent()){
            update(EPOLL_CTL_DEL,channel);
            channel->set_index(kDeleted);
        } else {
            update(EPOLL_CTL_MOD,channel);
        }
    }
} 

void Epoller::removeChannel(Channel* channel){
    assertInLoopThread();
    int fd = channel->fd();
    assert(channels_.find(fd) != channels_.end());
    assert(channels_[fd] == channel);
    assert(channel->isNoneEvent());
    int index = channel->index();
    assert(index == kAdded || index == kDeleted);
    size_t n = channels_.erase(fd);
    (void)n;
    assert(n==1);
    if(index == kAdded){
        update(EPOLL_CTL_DEL,channel); 
    }
    channel->set_index(kNew);
}

void Epoller::update(int operation,Channel* channel){
    struct epoll_event event;
    bzero(&event, sizeof(event));
    event.events = channel->events();
    event.data.ptr = channel;
    int fd = channel->fd();
    if(::epoll_ctl(epollfd_,operation,fd,&event)<0){
        if(operation == EPOLL_CTL_DEL){
            fprintf(stderr,"epoll_ctl op = EPOLLCTL_DEL, fd= %d\n",fd);
        } else {
            fprintf(stderr,"epoll_ctl op != EPOLLCTL_DEL, fd = %d\n",fd);
        }
    }
}