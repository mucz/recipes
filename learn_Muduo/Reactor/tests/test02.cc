// 负面测试，无法在loop被创建的线程之外调用loop
#include "EventLoop.hh"

EventLoop* g_loop;

void threadFunc(){
    g_loop->loop();
}

int main(){
    EventLoop loop;
    g_loop = &loop;
    Thread t(threadFunc);
    t.start();
    t.join();
    return 1;
}