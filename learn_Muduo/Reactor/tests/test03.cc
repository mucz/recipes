// 测试各个线程能正确储存该线程所拥有的loop的指针
#include "EventLoop.hh" 
extern __thread EventLoop* t_loopInThisThread;

void ThreadFunc(){
    // printf("sub() : pid = %d, tid = %d\n", 
            // getpid(),CurrentThread::tid());
    printf("loop in sub thread %d before creating loop: %p\n",CurrentThread::tid(),t_loopInThisThread);
    EventLoop loop;
    // loop.loop();
    printf("loop in sub thread %d after creating loop: %p\n",CurrentThread::tid(),t_loopInThisThread);
}

int main(){
    // printf("main() : pid = %d, tid = %d\n", 
            // getpid(),CurrentThread::tid());
    printf("loop in main thread %d before creating loop: %p\n",CurrentThread::tid(),t_loopInThisThread);
    EventLoop loop;
    Thread thread(ThreadFunc);
    thread.start();
    // loop.loop();
    printf("loop in main thread %d after creating loop: %p\n",CurrentThread::tid(),t_loopInThisThread);
}