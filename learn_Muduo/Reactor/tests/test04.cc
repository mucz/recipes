// 负面测试：一个线程无法创建两个Eventloop对象
#include "EventLoop.hh"


int main(){
    printf("main() : pid = %d, tid = %d\n", 
            getpid(),CurrentThread::tid());
    EventLoop loop;
    EventLoop loop2;
    pthread_exit(NULL);
}