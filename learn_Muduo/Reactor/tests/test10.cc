// test10.cc
// ! 需要Ctrl-C主动终止此程序
// 测试EvenLoopThreadPool, 需要配合CPU占用率工具使用，创建三个子线程，每个都空循环，主进程也进入空循环
// 最终应当有4个线程满占用率
#include "EventLoop.hh"
#include "EventLoopThread.hh"
#include "EventLoopThreadPool.hh"
#include <stdio.h>

void runInThread(){
    printf("runInThread() : pid= %d, tid = %d\n",getpid(),CurrentThread::tid());
    while(1){
        int i = 1;
        i++;
    }
}

int main(){
    printf("main() : pid = %d, tid = %d\n",getpid(),CurrentThread::tid());

    EventLoop baseloop;
    EventLoopThreadPool pool(&baseloop);
    pool.setThreadNum(3);
    pool.start();
    for(int i = 1; i<=3 ;i++){
        auto loop = pool.getNextLoop();
        loop->runInLoop(runInThread);
    }
    runInThread();
    printf("exit main()\n");


}