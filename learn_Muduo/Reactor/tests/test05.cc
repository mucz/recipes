// 单次触发的定时器，测试Poller和Channel
#include "EventLoop.hh"
#include <string.h>
#include "Channel.hh"
#include <sys/timerfd.h>

EventLoop* g_loop;
void timeout(Timestamp receiveTime){
    printf("%s timeout! \n ",receiveTime.toString().c_str());
    g_loop->quit();
}

int main(){
    EventLoop loop;
    g_loop = &loop;
    int timerfd = ::timerfd_create(CLOCK_MONOTONIC,TFD_NONBLOCK | TFD_CLOEXEC);
    Channel ch(&loop,timerfd);
    ch.setReadCallback(timeout);
    ch.enableReading();

    struct itimerspec howlong;
    bzero(&howlong, sizeof(howlong));
    howlong.it_value.tv_sec = 1;
    ::timerfd_settime(timerfd,0,&howlong,NULL);
    loop.loop();
    ::close(timerfd);
}