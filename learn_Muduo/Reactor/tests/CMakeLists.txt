include_directories(${PROJECT_SOURCE_DIR})

add_executable(Reactor_test1 test01.cc)
target_link_libraries(Reactor_test1 Reactor_lib)

add_executable(Reactor_test2 test02.cc)
target_link_libraries(Reactor_test2 Reactor_lib)

add_executable(Reactor_test3 test03.cc)
target_link_libraries(Reactor_test3 Reactor_lib)

add_executable(Reactor_test4 test04.cc)
target_link_libraries(Reactor_test4 Reactor_lib)

add_executable(Reactor_test5 test05.cc)
target_link_libraries(Reactor_test5 Reactor_lib)

add_executable(Reactor_test6 test06.cc)
target_link_libraries(Reactor_test6 Reactor_lib)

add_executable(Reactor_test7 test07.cc)
target_link_libraries(Reactor_test7 Reactor_lib)

add_executable(Reactor_test8 test08.cc)
target_link_libraries(Reactor_test8 Reactor_lib)

add_executable(Reactor_test9 test09.cc)
target_link_libraries(Reactor_test9 Reactor_lib)

add_executable(Reactor_test10 test10.cc)
target_link_libraries(Reactor_test10 Reactor_lib)

add_executable(Reactor_test11 test11.cc)
target_link_libraries(Reactor_test11 Reactor_lib)
