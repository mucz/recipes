// 测试子线程的创建，不同线程有不同的tid
#include "EventLoop.hh"

pid_t main_tid = 0;
pid_t sub_tid = 0;
void threadFunc(){
    printf("threadFunc() : pid = %d, tid = %d\n ", 
            getpid(),CurrentThread::tid());
    sub_tid = CurrentThread::tid();
    EventLoop loop;
    loop.loop();
}

int main(){
    printf("main() : pid = %d, tid = %d\n", 
            getpid(),CurrentThread::tid());
    main_tid = CurrentThread::tid();
    assert(main_tid != sub_tid);
    EventLoop loop;
    Thread thread(threadFunc);
    thread.start();
    
    loop.loop();
    pthread_exit(NULL);
}