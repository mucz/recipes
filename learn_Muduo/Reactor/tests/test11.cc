// testof TimerQueue::cancel
// right output : once 1 every 2 once 3 
// ! need to quit manually with Ctrl-C
#include "EventLoop.hh"
#include <stdio.h>

int cnt = 0;
EventLoop* g_loop;

void printTid(){
    printf("pid = %d, tid = %d\n",getpid(),CurrentThread::tid());
    printf("now %s\n",Timestamp::now().toString().c_str());
}

void print(const char* msg){
    printf("msg %s %s \n",Timestamp::now().toString().c_str(),msg);
}

int main(){
    printTid();
    EventLoop loop;
    g_loop = &loop;
    printf("main\n");

    // get some weak_ptr of timer
    auto timer_wp1 = loop.runAfter(1,std::bind(print,"once1"));
    auto timer_wp2 = loop.runAfter(4,std::bind(print,"once4"));
    auto timer_wp3 = loop.runAfter(3,std::bind(print,"once3"));
    // auto timer_wp4 = loop.runAfter(3.5,std::bind(print,"once3.5"));
    auto timer_wp5 = loop.runEvery(2,std::bind(print,"every2"));
    // auto timer_wp6 = loop.runEvery(3,std::bind(print,"every3"));

    // cancel them
    // cancel an expired timer
    loop.runAfter(1.5,std::bind(&EventLoop::cancel,&loop,timer_wp1));
    // cancel a non-expired timer
    loop.runAfter(1,std::bind(&EventLoop::cancel,&loop,timer_wp2));
    // cancel a periodic timer
    loop.runAfter(3.5,std::bind(&EventLoop::cancel,&loop,timer_wp5));

    loop.loop();
    printf("main loop exists");
    sleep(1);

}