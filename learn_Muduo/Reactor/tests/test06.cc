// 检测标准输入的Poller，测试Poller和Channel
#include "EventLoop.hh"
#include <string.h>
#include "Channel.hh"
#include <sys/timerfd.h>

EventLoop* g_loop;
void stdin_read_callback(Timestamp receiveTime){
    printf("%s stdin readable! \n ",receiveTime.toString().c_str());
    g_loop->quit();
}

int main(){
    EventLoop loop;
    g_loop = &loop;
    Channel ch(&loop,STDIN_FILENO); // 0 
    ch.setReadCallback(stdin_read_callback);
    ch.enableReading();

    loop.loop();
}