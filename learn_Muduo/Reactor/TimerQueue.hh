#ifndef TIMERQUEUE
#define TIMERQUEUE
#include "noncopyable.hh"
#include <set>
#include <memory>
#include <vector>
#include "Timer.hh"
#include "Channel.hh"
#include <sys/timerfd.h> 
#include <utility>
#include <strings.h>
#include <optional>

class EventLoop;

class TimerQueue : noncopyable
{
public:
    typedef std::pair<Timestamp,std::shared_ptr<Timer>> Entry;
    typedef std::set<Entry> TimerSet;
    typedef std::vector<Entry> TimerVec;
    typedef Timer::TimerCallback TimerCallback;

    TimerQueue(EventLoop* loop);
    ~TimerQueue();

    // called when timerfd alarms
    void handleRead();
    // move out all expired timers
    TimerVec getExpired(Timestamp now);
    // reset all repeated timer
    void reset(TimerVec& expired, Timestamp now);
    // insert a timer
    bool insert(std::shared_ptr<Timer>& timer); 
    
    // Schedules the callback to be run at given time
    // repeats if @c interval > 0.0
    std::weak_ptr<Timer> addTimer(const TimerCallback& cb, Timestamp when, double interval);
    // Timer should be add in-thread since EventLoop is cross-thread object
    void addTimerInLoop(std::shared_ptr<Timer>& timer);

    void cancel(std::weak_ptr<Timer>& timer_weak_p);

    void cancelInLoop(std::weak_ptr<Timer>& timer_weak_p);
private:
    EventLoop* loop_;
    const int timerfd_;
    Channel timerfdChannel_;
    TimerSet timers_;
};


#endif /* TIMERQUEUE */
