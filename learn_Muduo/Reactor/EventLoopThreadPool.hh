#ifndef EVENTLOOPTHREADPOOL
#define EVENTLOOPTHREADPOOL
#include <vector>
#include "noncopyable.hh"
#include <functional>
#include <memory>

class EventLoop;
class EventLoopThread;
class EventLoopThreadPool : noncopyable{
public:
    typedef std::function<void(EventLoop*)> ThreadInitCallback;

    EventLoopThreadPool(EventLoop* baseloop, const std::string nameArg = "ThreadPool");
    ~EventLoopThreadPool();
    void setThreadNum(int numThreads) { numThreads_ = numThreads; }

    void start(const ThreadInitCallback& cb = ThreadInitCallback());
    EventLoop* getNextLoop();

    bool started() const { return started_; }
    const std::string& name() const {return name_; }

private:
    EventLoop* baseLoop_;
    std::string name_;
    bool started_;
    int numThreads_;
    int next_;
    std::vector<EventLoop*> loops_; // for getNextLoop()
    std::vector<std::unique_ptr<EventLoopThread>> threads_; // 生命周期管理

};
#endif /* EVENTLOOPTHREADPOOL */
