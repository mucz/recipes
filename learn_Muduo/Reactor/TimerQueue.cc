#include "TimerQueue.hh"
#include "Channel.hh"
#include "EventLoop.hh"

int createTimerfd(){
    int timerfd = ::timerfd_create(CLOCK_MONOTONIC,TFD_NONBLOCK | TFD_CLOEXEC);
    if(timerfd < 0){
        fprintf(stderr,"Failed in timerfd_create!\n");
    }
    return timerfd;
}


// helper for resetTimerfd
struct timespec howMuchTimeFromNow(Timestamp when)
{
    int64_t microSeconds = when.microSecondsSinceEpoche() - Timestamp::now().microSecondsSinceEpoche();
    microSeconds = microSeconds < 100 ? 100 : microSeconds;
    struct timespec ts;
    ts.tv_sec = static_cast<time_t> (microSeconds/Timestamp::kMicroSecondsPerSecond);
    ts.tv_nsec = static_cast<long> ((microSeconds % Timestamp::kMicroSecondsPerSecond) * 1000);
    return ts;
}

// resetTimerfd to get an event at $expiration
void resetTimerfd(int timerfd, Timestamp expiration){
    struct itimerspec newValue;
    // struct itimerspec oldValue;
    bzero(&newValue,sizeof(newValue));
    // bzero(&oldValue,sizeof(oldValue));
    newValue.it_value = howMuchTimeFromNow(expiration);
    // int ret = ::timerfd_settime(timerfd,0,&newValue,&oldValue);
    int ret = ::timerfd_settime(timerfd,0,&newValue,nullptr);
    if(ret){
        fprintf(stderr,"resetTimerfd:timerdf_settime() error : %d \n",ret);
        abort();
    }
}

void readTimerfd(int timerfd,Timestamp now){
    uint64_t howmany;
    ssize_t n = ::read(timerfd,&howmany,sizeof(howmany));
    if(n != sizeof(howmany)){
        fprintf(stderr,"TimerQueue::handleRead() reads %ld bytes instead of 8!\n",n);
        abort();
    }
}

TimerQueue::TimerQueue(EventLoop* loop)
: loop_(loop),
  timerfd_(createTimerfd()),
  timerfdChannel_(loop,timerfd_),
  timers_()
{
    timerfdChannel_.setReadCallback(std::bind(&TimerQueue::handleRead, this));
    timerfdChannel_.enableReading();
}

TimerQueue::~TimerQueue(){
    ::close(timerfd_);
}

void TimerQueue::handleRead(){
    loop_->assertInLoopThread();
    Timestamp now(Timestamp::now());
    readTimerfd(timerfd_,now);
    TimerVec expired = getExpired(now);

    for(auto it = expired.begin(); it != expired.end(); ++it){
        it->second->run();
    }

    reset(expired,now);
}

std::weak_ptr<Timer> TimerQueue::addTimer(const TimerCallback& cb,Timestamp when, double interval){
    auto timer(std::make_shared<Timer>(cb,when,interval));
    std::weak_ptr<Timer> timer_weak_p = timer;
    loop_->runInLoop(
        std::bind(
                    &TimerQueue::addTimerInLoop,
                    this,
                    timer
                )
            );
    return timer_weak_p;
}

void TimerQueue::addTimerInLoop(std::shared_ptr<Timer>& timer){
    loop_->assertInLoopThread();
    bool earliestChanged = insert(timer);
    if(earliestChanged){
        resetTimerfd(timerfd_,timer->expiration());
    }
}

TimerQueue::TimerVec TimerQueue::getExpired(Timestamp now){
    TimerVec ret;
    TimerQueue::Entry sentry = std::make_pair(now,nullptr);
    auto it = timers_.lower_bound(sentry);
    assert(it == timers_.end() || now < it->first);
    std::copy(timers_.begin(),it,back_inserter(ret));
    timers_.erase(timers_.begin(),it);
    return ret;
}

void TimerQueue::reset(TimerQueue::TimerVec& expired, Timestamp now){
    std::optional<Timestamp> nextExpire;
    for(auto it = expired.begin(); it!=expired.end(); ++it){
        if(it->second->repeat()){
            it->second->restart(now);
            insert(it->second);
        }
    }
    if(!timers_.empty()){
        nextExpire = timers_.begin()->second->expiration();
    }
    if(nextExpire.has_value()){ // 改成optionnal
       resetTimerfd(timerfd_,nextExpire.value());
    }
}

bool TimerQueue::insert(std::shared_ptr<Timer>& timer){
    bool earliestChanged = false; // whether the timer to insert will be the earliest timer called
    auto when = timer->expiration();
    if(timers_.empty() || when < timers_.begin()->first ){
        earliestChanged = true;
    }
    auto result = timers_.insert({when,timer});
    assert(result.second);
    return earliestChanged;
}

void TimerQueue::cancel(std::weak_ptr<Timer>& timer_weak_p){
    loop_->runInLoop(std::bind(&TimerQueue::cancelInLoop,this,timer_weak_p));
}

void TimerQueue::cancelInLoop(std::weak_ptr<Timer>& timer_weak_p){
    loop_->assertInLoopThread();
    std::shared_ptr<Timer> timer(timer_weak_p.lock());
    if(timer){
        assert(timers_.count(make_pair(timer->expiration(),timer))!=0);
        timers_.erase(make_pair(timer->expiration(),timer));
        assert(timer.use_count()==1);
    } else{

    } 
}