#include "EventLoop.hh"
// #include "Poller.hh"
#include "Epoller.hh"
#include <assert.h>
#include <sys/eventfd.h>
#include <signal.h>


__thread EventLoop* t_loopInThisThread = 0;

static int creatEventfd(){
    int evtfd = ::eventfd(0, EFD_NONBLOCK | EFD_CLOEXEC);
    if(evtfd < 0){
        fprintf(stderr,"EventLoop: Failed in creating eventfd\n");
        abort();
    }
    return evtfd;
}

EventLoop::EventLoop()
: looping_(false),
  quit_(false),
  threadId_(CurrentThread::tid()),
  poller_(std::make_unique<Epoller>(this)),
  activeChannels_(),
  timerQueue_(std::make_unique<TimerQueue>(this)),
  wakeupFd_(creatEventfd()),
  wakeupChannel_(std::make_unique<Channel>(this,wakeupFd_)),
  pendingFunctors_(),
  callingPendingFunctors_(false),
  mutex_()
{
   if(t_loopInThisThread) {
       fprintf(stderr,"Another EventLoop exists in this thread!\n");
   } else {
       t_loopInThisThread = this;
   }
   wakeupChannel_->setReadCallback(std::bind(&EventLoop::handleRead,this));
   wakeupChannel_->enableReading();
}

EventLoop::~EventLoop(){
    assert(!looping_);
    ::close(wakeupFd_);
    t_loopInThisThread = nullptr; 
}

void EventLoop::loop(){ 
    assert(!looping_);
    assertInLoopThread();
    looping_ = true;
    quit_ = false;  
    while (!quit_)
    {
        activeChannels_.clear();
        pollReturnTime_ = poller_->poll(kPollTimeMs, &activeChannels_);
        for(auto it = activeChannels_.begin() ; it != activeChannels_.end(); it++){
            (*it)->handleEvent(pollReturnTime_);
        } 
        doPendingFunctors();
    }
    
    looping_ = false;
}

void EventLoop::quit(){
    quit_ = true;
}

void EventLoop::updateChannel(Channel* channel){
    assert(channel->ownerLoop() == this);
    assertInLoopThread(); // eventloop is a cross-thread object 
    poller_->updateChannel(channel);
}

void EventLoop::abortNotInLoopThread(){
    abort();
}

EventLoop* EventLoop::getEventLoopOfCurrentThread(){
    return t_loopInThisThread;
}

void EventLoop::runInLoop(const Functor& cb){
    if(isInLoopThread()){
        cb();
    } else {
        queueInLoop(cb);
    }
}

void EventLoop::queueInLoop(const Functor& cb){ 
    {
        MutexLockGuard lock(mutex_);
        pendingFunctors_.push_back(cb);
    }
    if(!isInLoopThread() || callingPendingFunctors_){
        wakeup();
    }
}

void EventLoop::wakeup(){
    uint64_t one = 1;
    ssize_t n = ::write(wakeupFd_,&one,sizeof(one));
    if(n != sizeof(one)){
        fprintf(stderr,"EventLoop::wakeup() writes %ld bytes instead of 8!\n",n);
    }
}

void EventLoop::handleRead(){
    uint64_t one = 1;
    ssize_t n = ::read(wakeupFd_,&one,sizeof(one));
    if(n!= sizeof(one)){
        fprintf(stderr,"EventLoop::handleRead() reads %ld bytes instead of 8!\n",n);
    }
}

void EventLoop::doPendingFunctors(){
    std::vector<Functor> functors;
    callingPendingFunctors_ = true;
    {
        MutexLockGuard lock(mutex_);
        functors.swap(pendingFunctors_);
    }
    for(const auto& f : functors){
        f();
    }
    callingPendingFunctors_ = false;
}

std::weak_ptr<Timer> EventLoop::runAt(const Timestamp& time, const TimerCallback& cb){
    return timerQueue_->addTimer(cb,time,0.0);
}

std::weak_ptr<Timer> EventLoop::runAfter(double delay, const TimerCallback& cb){
    Timestamp time(addTime(Timestamp::now(),delay));
    return runAt(time,cb);
}

std::weak_ptr<Timer> EventLoop::runEvery(double interval,const TimerCallback& cb){
    Timestamp time(addTime(Timestamp::now(),interval));
    return timerQueue_->addTimer(cb,time,interval);
}

void EventLoop::removeChannel(Channel* channel){
    assert(channel->ownerLoop() == this);
    assertInLoopThread();
    poller_->removeChannel(channel);
}

void EventLoop::cancel(std::weak_ptr<Timer>& timer_weak_p){
    timerQueue_->cancel(timer_weak_p);
}
// 如果一个 socket 在接收到了 RST packet 之后，程序仍然向这个 socket 写入数据，那么就会产生SIGPIPE信号
// 默认情况下这个信号会终止整个进程，为了避免这种情况，让进程忽略SIGPIPE信号
class IgnoreSIGPIPE{
public:
    IgnoreSIGPIPE(){
        ::signal(SIGPIPE,SIG_IGN);
    }
};
IgnoreSIGPIPE initObj;