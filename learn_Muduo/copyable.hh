#ifndef COPYABLE
#define COPYABLE

class copyable {
    protected:
    copyable() = default;
    ~copyable() = default;
};

#endif /* COPYABLE */
