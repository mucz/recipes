// 一个用Mutex替换读写锁的例子
// 基于 copy-on-other-reading的思想

#include "Thread/Mutex.hh"
#include <string>
#include <map>
#include <vector>
#include <memory>
#include <assert.h>
class CustomerData : noncopyable{
public:
    CustomerData() : mutex_(),data_(){}
    int query(const std::string& customer, const std::string & stock) const;
private:
    typedef std::pair<std::string,int> Entry;
    typedef std::vector<Entry> EntryList;
    typedef std::map<std::string,EntryList> Map;
    typedef std::shared_ptr<Map> MapPtr;

    void update(const std::string& customer, const EntryList& entries);

    static int findEntry(const EntryList& entries, const std::string& stock);

    MapPtr getData() const {
        MutexLockGuard lock(mutex_);
        return data_;
    }
    
    mutable MutexLock mutex_;
    MapPtr data_;
};

void CustomerData::update(const std::string& customer, const EntryList& entries){
    MutexLockGuard lock(mutex_);
    if(!data_.unique()){ // other Thread are reading
                         // make a copy as the new one and write to it
                         // "copy-on-other-reading"
        MapPtr newData = std::make_shared<Map>(*data_);
        data_.swap(newData);
    }
    assert(data_.unique());
    (*data_)[customer]=entries;
};

int CustomerData::query(const std::string& customer, const std::string & stock) const {
    MapPtr data = getData();
    auto it_entries = data->find(customer);
    if(it_entries!=data->end())
        return findEntry(it_entries->second,stock);
    else 
        return -1;
};


int CustomerData::findEntry(const EntryList& entries, const std::string& stock){
    auto itr = lower_bound(entries.begin(),entries.end(),stock);
    if(itr != entries.end()){
        return itr->second;
    } else {
        return -1;
    }
};