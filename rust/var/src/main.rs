fn main() {
    println!("Hello, world!");
    let x : u32 = 5;
    println!("The value of x is: {}",x);
    // wrong x = 6;
    let x = x +1;
    println!("The value of x is: {}",x);
    {
        let x = x*2;
        println!("The value of x in the inner scope is: {}",x);
    }
    println!("The value of x in the outte scope is: {}",x);

}
