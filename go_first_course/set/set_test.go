package set_test

import "testing"

func TestArrayInit(t *testing.T) {
	var arr [3]int
	t.Log(arr[1], arr[2])

	arr2 := [4]int{1, 2, 3, 4}
	arr3 := [...]int{1, 2, 3, 4, 5}
	t.Log(arr2, arr3)
}

func TestArrayGothrough(t *testing.T) {
	arr := [...]int{1, 2, 3, 4, 5}

	for i := 0; i < len(arr); i++ {
		t.Log(arr[i])
	}

	for idx, e := range arr {
		t.Log(idx, e)
	}

	for _, e := range arr {
		t.Log(e)
	}
}

func TestSlice(t *testing.T) {
	var arr [3]int // array
	var s0 []int   //slice
	t.Log(arr)
	t.Log(len(s0), cap(s0))
	s0 = append(s0, 1)
	t.Log(len(s0), cap(s0))

	s1 := []int{1, 2, 3, 4}
	t.Log(len(s1), cap(s1))

	s2 := make([]int, 3, 5)
	t.Log(len(s2), cap(s2))
	// len(s2) = 3, cap(s2) = 5
	t.Log(s2[0], s2[1], s2[2]) // ok
	// t.Log(s2[0], s2[1], s2[2],s2[3])) // wrong, len = 3, can't read 4th element
}

func TestSliceGrowingShrinking(t *testing.T) {
	s := []int{}
	for i := 0; i < 10; i++ {
		s = append(s, i)
		t.Log(len(s), cap(s))
	}
	for i := 0; i < 10; i++ {
		s = s[0 : len(s)-1]   // pop back
		t.Log(len(s), cap(s)) // won't shrink
	}
}

func TestMap(t *testing.T) {
	m := map[string]int{"one": 1, "two": 2, "three": 3}
	m1 := map[string]int{}

	m["four"] = 4
	m1["five"] = 5

	m_reserved := make(map[string]int, 10 /* Initial Capacity */)

	m_reserved["six"] = 6

	t.Log(m)
	t.Log(m1)
	t.Log(m_reserved)

}

func TestMapAccessNotExisingKey(t *testing.T) {
	m1 := map[string]int{}
	t.Log(m1["one"]) // one don't exist in map, dangerous!
	m1["zero"] = 0
	t.Log(m1["zero"]) // can't tell "zero" whether exist since 0 is return ,dangerous

	// right practice
	if v, ok := m1["zero"]; ok {
		// "zero" exist in map m1
		t.Log("key zero exist in map, value", v)
	} else {
		// "zero" don't exist in map
		t.Log("key zero don't exist in map")
	}
}

func TestMapGothrough(t *testing.T) {
	m1 := map[int]int{1: 1, 2: 4, 3: 9}
	for k, v := range m1 {
		t.Log(k, " : ", v)
	}
}
