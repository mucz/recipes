package main

import (
	"fmt"
	"github.com/lucasepe/codename"
	"net"
	"sync"
	"tcp-cs-demo/frame"
	"tcp-cs-demo/packet"
	"time"
)

func main() {
	var wg sync.WaitGroup
	var num int = 5
	wg.Add(5)

	for i := 0; i < num; i++ {

		go func(i int) {
			defer wg.Done()
			startClient(i)
		}(i + 1)

	}

	wg.Wait()
}

func startClient(i int) {
	conn, err := net.Dial("tcp", ":8888")
	if err != nil {
		fmt.Println("dial error:", err)
		return
	}
	defer conn.Close()

	fmt.Printf("[client %d]: dial done\n")

	rng, err := codename.DefaultRNG()
	if err != nil {
		panic(err)
	}

	frameCodec := frame.NewMyFrameCodec()

	var counter int
	quit := make(chan struct{})
	done := make(chan struct{})

	// reader
	go func() {
		for {
			// quit non-block
			// ->quit->done
			select {
			case <-quit:
				done <- struct{}{}
				return
			default:
			}

			conn.SetReadDeadline(time.Now().Add(time.Second * 1))
			ackFramePayLoad, err := frameCodec.Decode(conn)
			if err != nil {
				// if err = Timeout
				if e, ok := err.(net.Error); ok {
					if e.Timeout() {
						continue
					}
				}

				// else : panic
				panic(err)
			}

			p, err := packet.Decode(ackFramePayLoad)
			submitAck, ok := p.(*packet.SubmitAck)
			if !ok {
				panic("not submitack")
			}
			fmt.Printf("[client %d]: the result of submit ack[%s] is %d\n", i, submitAck.ID, submitAck.Result)
		}
	}()

	// writer
	for {
		counter++
		id := fmt.Sprintf("%08d", counter)
		payload := codename.Generate(rng, 4)
		s := &packet.Submit{
			ID:      id,
			Payload: []byte(payload),
		}

		// encode -> packet
		framePayload, err := packet.Encode(s)
		if err != nil {
			panic(err)
		}

		fmt.Printf("[client %d]: send submit id = %s, payload=%s, frame length = %d\n",
			i, s.ID, s.Payload, len(framePayload)+4)

		// encode -> frame && send
		err = frameCodec.Encode(conn, framePayload)
		if err != nil {
			panic(err)
		}

		time.Sleep(1 * time.Second)
		if counter >= 10 {
			quit <- struct{}{} // stop reader
			<-done             // reader done
			fmt.Printf("[client %d]: exit ok\n", i)
			return
		}
	}
}
