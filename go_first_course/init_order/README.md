
# 演示各个包的init的执行顺序:深度优先
    main依赖1和2,1和2依赖3
# 同一个包内, const -> var -> init

# init 函数可以有多个


- ` go mod init initorder `  # 注意这里的名字应与代码中import的名字相同
- ` go mod tidy ` 
- ` go build main.go `
- ` ./main `