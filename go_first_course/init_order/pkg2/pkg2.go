
package pkg2 

import(
	"fmt"

	_ "initorder/pkg3"
)

var(
	_ = constinitcheck()
	v1 = variableinit("v1")
	v2 = variableinit("v2")
)

const (
	c1 = "c1"
	c2 = "c2"
)

func constinitcheck() string {
	if c1 != ""{
		fmt.Println("pkg2 : const c1 has been initialized")
	} 
	if c2 != ""{
		fmt.Println("pkg2 : const c2 has been initialized")
	}
	return ""
}

func variableinit(name string) string {
	fmt.Printf("pkg2 : var %s has been initialized\n",name)
	return name
}

func init(){
	fmt.Println("pkg2: first init func invoked")
}

func init(){
	fmt.Println("pkg2: second init func invoked")
}

func main(){
	// do nothing
}