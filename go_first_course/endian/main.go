package main

import (
	"encoding/binary"
	"fmt"
"unsafe"
)

func IsLittleEndian()  bool{
	var value int32 = 1 // 占4byte 转换成16进制 0x00 00 00 01
	// 大端(16进制)：00 00 00 01
	// 小端(16进制)：01 00 00 00
	pointer := unsafe.Pointer(&value)
	pb := (*byte)(pointer)
	if *pb != 1{
		return false
	}
	return true
}

func BigEndianAndLittleEndianByLibrary()  {
	var value uint32 = 10
	by := make([]byte,4)
	binary.BigEndian.PutUint32(by,value)
	
	fmt.Println("转换成大端后 ",by)
	fmt.Println("使用大端字节序输出结果：",binary.BigEndian.Uint32(by))
	little := binary.LittleEndian.Uint32(by)
	big := binary.BigEndian.Uint32(by)
	fmt.Println("大端字节序使用小端输出结果：",little)
	fmt.Println("大端字节序使用大端输出结果：",big)
}

func main()  {
	fmt.Println(IsLittleEndian())
	BigEndianAndLittleEndianByLibrary()
}