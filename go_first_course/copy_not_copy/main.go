package main

import "fmt"

func main() {
	var s = make([]int, 0, 5)
	s = append(s, 0)
	s2 := &s

	printSlice(s)   // 0
	printSlice(*s2) // 0

	*s2 = append(*s2, 0)
	printSlice(s)   // 0 0
	printSlice(*s2) // 0 0

	s3 := s
	s3 = append(s3, 0)
	printSlice(s)  // 0 0
	printSlice(s3) // 0 0 0

	s = append(s, 1)
	printSlice(s)  // 0 0 1
	printSlice(s3) // 0 0 1

	for i := 0; i < 5; i++ {
		s3 = append(s3, 0)
	}
	printSlice(s)  // 0 0 1
	printSlice(s3) // 不同的地址

	ss := []int{1, 2, 3, 4}
	printSlice(ss)
	doSomething(ss)
	// ss 不变
	printSlice(ss)

}

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %p %v\n", len(s), cap(s), s, s)
}

func doSomething(s []int) {
	// s 是一个复制
	printSlice(s)
	s = s[1:]
	for i := 0; i < 5; i++ {
		s = append(s, 0)
	}
	// s 指向的地址发生变化
	printSlice(s)
}
