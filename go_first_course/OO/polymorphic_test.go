package go_test_oo

import (
	"fmt"
	"testing"
)

type Code string
type Programmer_ interface {
	WriteHelloWorld() Code
}

type GoProgrammer_ struct {
}

func (p *GoProgrammer_) WriteHelloWorld() Code {
	return "fmt.Println(\"Hello World!\")"
}

type JavaProgrammer_ struct {
}

func (p *JavaProgrammer_) WriteHelloWorld() Code {
	return "System.out.Println(\"Hello World!\")"
}

func writeFirstProgram(p Programmer_) {
	fmt.Printf("%T %v\n", p, p.WriteHelloWorld())
}

func TestPolymorphism(t *testing.T) {
	// goProg1 := GoProgrammer{}    // ! wrong, interface should be a pointer
	goProg := &GoProgrammer_{} // right
	// goProg2 := new(GoProgrammer) // right

	javaProg := new(JavaProgrammer_)
	writeFirstProgram(goProg)
	writeFirstProgram(javaProg)
}
