package go_test_oo

import (
	"fmt"
	"testing"
)

type Pet struct {
}

func (p *Pet) Speak() {
	fmt.Print("...")
}

func (p *Pet) SpeakTo(host string) { //* LSP 里氏替换原则
	p.Speak()
	fmt.Println(" ", host)
}

//  ------------------------
type Dog_combination struct {
	p *Pet // ! 组合，需要自己重新实现方法或者增加一个wrapper间接调用
}

// wrapper
func (d *Dog_combination) Speak() {
	d.p.Speak()
}

// 这里重新实现SpeakTo
func (d *Dog_combination) SpeakTo(host string) {
	d.Speak()
	fmt.Println(" ", host)
}

// -----------------------

type Dog struct {
	Pet // ! 匿名嵌入
}

func (d *Dog) Speak() { // ! 并不能实现 method overwrite
	fmt.Print("Wang!")
}

// -----------------------
func TestDog(t *testing.T) {
	// var dog_1 Pet = new(Dog) // wrong, no implicite convertion

	// var dog_2 *dog = new(Dog)
	// var p = (*Pet)(dog) // wrong, no Inheritance in Go, even use explicite conversion

	dog := new(Dog) // correct

	dog.SpeakTo("Chao") // ! 调用pet的Speak而不是dog的Speak，并没能实现覆写
}
