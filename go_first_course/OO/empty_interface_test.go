package go_test_oo

import (
	"fmt"
	"testing"
)

// ! 空接口 类似 void*
// ! 提供了一定的泛型能力
// ! 比较危险的行为，失去了全部的编译器保护

func DoSomething(p interface{}) {
	if i, ok := p.(int); ok { // * 类型断言
		fmt.Println("Integer", i)
	}
	if s, ok := p.(string); ok { // * 类型断言
		fmt.Println("stirng", s)
	}
	fmt.Println("Unknow Type")

	// type switch 与上面的代码相同
	switch v := p.(type) {
	case int:
		fmt.Println("Integer", v)
	case string:
		fmt.Println("String", v)
	default:
		fmt.Println("Unknow Type")
	}

}

func TestEmptyInterfaceAssertion(t *testing.T) {
	DoSomething(10)
	DoSomething("10")
}
