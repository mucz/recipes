package go_test_oo

import "testing"

type Programmer interface {
	WriteHelloWorld() string
}

type GoProgrammer struct {
}

func (g *GoProgrammer) WriteHelloWorld() string { //* 方法签名一样就满足接口
	return "fmt.Println(\"Hello World\")"
}

func TestClient(t *testing.T) {
	var p Programmer // interface
	// ! interface 对应某一个实现的指针
	p2 := new(GoProgrammer)
	t.Logf("p is a %T, p2 is a %T", p, p2) // pointer* & pointer*

	p = new(GoProgrammer)
	t.Logf("p is a %T, p2 is a %T", p, p2)

	t.Log(p.WriteHelloWorld())
}
