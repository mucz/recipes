package string_test

import "testing"

// string is a CONST SLICE of bytes
func TestString(t *testing.T) {
	var s string
	t.Log(s)

	s = "hello"
	t.Log(s, "len", len(s))

	s = "你好"
	t.Log(s, "len", len(s)) // len != 字符数

	s = "\xE5\xAB\xFF\x05"
	t.Log(s, "len", len(s)) // len = 4 = bytes数

	// s[1] = '3' // invalid string is const
}

func TestUnicodeUTF8(t *testing.T) {
	// Unicode ： 字符集，编码规则
	// UTF8 ： Unicode的一种存储实现

	s := "中"
	t.Log(s, "len", len(s))

	c := []rune(s)
	t.Log(c, "len", len(c))
	t.Logf("中 unicode %x", c[0]) // 编码 = 4E2D
	t.Logf("中 UTF8 %x", s)       // 存储 = 3EB8AD -> 所以len(s) == 3
}

func TestStringToRune(t *testing.T) {
	s := "一段字符串 A String"
	for _, c := range s {
		t.Logf("%[1]c %[1]d %[1]x", c) // 因为转换成rune了，输出的是rune而不是byte, 是编码而不是存储码
	}
}
