package string_test

import (
	"strconv"
	"strings"
	"testing"
)

func TestStringFn(t *testing.T) {
	s := "A,B,C"
	parts := strings.Split(s, ",")
	for _, part := range parts {
		t.Log(part)
	}
	t.Log(strings.Join(parts, "-"))
}

func TestItoa(t *testing.T) {
	s := strconv.Itoa(10)
	t.Log("str" + s)
}

func TestAtoi(t *testing.T) {
	s := "10"
	if i, err := strconv.Atoi(s); err == nil {
		t.Log(i + 10) // 10 + 10 = 20
	}
}
