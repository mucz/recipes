package main

import (
	"fmt"
	"time"
)

func deadloop() {
	for {
		// https://time.geekbang.org/column/article/476643
		// goroutine的调度时间点是所有函数，或者信号抢占
		// 这里没有函数，所以不会通过函数进入调度器，而是通过时间
	}
}

func main() {
	go deadloop()
	for {
		time.Sleep(time.Second * 4)
		fmt.Println("I got scheduled!")
	}
}
