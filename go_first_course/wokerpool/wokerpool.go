package workerpool

import (
	"errors"
	"fmt"
	"sync"
)

const (
	defaultCapacity = 3
	maxCapacity     = 10
)

type Task func()
type Pool struct {
	capacity int

	active chan struct{}
	tasks  chan Task

	wg   sync.WaitGroup
	quit chan struct{}
}

func (p *Pool) run() {
	idx := 0
	for {
		select {
		case <-p.quit:
			return
		case p.active <- struct{}{}:
			idx++
			p.newWorker(idx) // new a goroutine
		}
	}
}

func (p *Pool) newWorker(i int) {
	p.wg.Add(1)
	go func() {

		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("woker[%03d]: recover panic[%s] and exit\n", i, err)
				<-p.active
			}
			p.wg.Done()
		}()

		fmt.Printf("woker [%03d]: start\n", i)

		for {
			select {
			case <-p.quit:
				fmt.Printf("worker[%03d]: exit\n", i)
				<-p.active
				return
			case t := <-p.tasks:
				fmt.Printf("worker[%03d]: receive a task\n", i)
				t()
			}
		}
	}()
}

var ErrWorkerPoolFreed = errors.New("workerpool freed")

func (p *Pool) Schedule(t Task) error {
	select {
	case <-p.quit:
		return ErrWorkerPoolFreed
	case p.tasks <- t:
		return nil
	}

}

func New(cap int) *Pool {
	if cap <= 0 {
		cap = defaultCapacity
	}
	if cap > maxCapacity {
		cap = maxCapacity
	}
	p := &Pool{
		capacity: cap,
		tasks:    make(chan Task),
		quit:     make(chan struct{}),
		active:   make(chan struct{}, cap),
	}
	fmt.Printf("wokerpool start \n")

	go p.run()
	return p
}

func (p *Pool) Free() {
	//	ver 1
	//for {
	//	select{
	//	case p.quit<-struct{}{}:
	//
	//	default:
	//		return
	//	}
	//}

	// ver2
	close(p.quit)
	p.wg.Wait()
	fmt.Print("Workerpool freed!\n")

}
