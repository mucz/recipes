# API

模拟的是真实世界的一个书店的图书管理后端服务。这个服务为平台前端以及其他客户端，提供针对图书的 CRUD（创建、检索、更新与删除）的基于 HTTP 协议的 API。

HTTPS方法   URI        　   API含义
POST       /book            创建
POST       /book/<id>       更新
GET        /book            返回所有图书信息
GET        /book/<id>       返回特定图书信息
DELETE     /book/<id>       删除特定图书

使用json数据格式作为HTTP的body

# 文件结构

├── cmd/
│   └── bookstore/         // 放置bookstore main包源码
│       └── main.go
├── go.mod                 // module bookstore的go.mod
├── go.sum
├── internal/              // 存放项目内部包的目录
│   └── store/
│       └── memstore.go     
├── server/                // HTTP服务器模块
│   ├── middleware/
│   │   └── middleware.go
│   └── server.go          
└── store/                 // 图书数据存储模块
    ├── factory/
    │   └── factory.go
    └── store.go