package frame

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
)

type StreamFrameCodec interface {
	Encode(io.Writer, FPayload) error
	Decode(io.Reader) (FPayload, error)
}

type FPayload []byte

var (
	ErrShortWrite = errors.New("short write")
	ErrShortRead  = errors.New("short read")
)

type myFrameCodec struct{}

func NewMyFrameCodec() StreamFrameCodec {
	return &myFrameCodec{}
}

func (p *myFrameCodec) Encode(w io.Writer, payload FPayload) error {
	var f = payload
	var totalLen = int32(len(f)) + 4

	err := binary.Write(w, binary.BigEndian, &totalLen)
	if err != nil {
		return err
	}

	n, err := w.Write(f)
	if err != nil {
		return err
	}
	if n != len(payload) {
		return ErrShortWrite
	}
	return nil
}

func (p *myFrameCodec) Decode(r io.Reader) (FPayload, error) {
	var totalLen int32
	err := binary.Read(r, binary.BigEndian, &totalLen)
	if err != nil {
		return nil, err
	}

	if totalLen-4 <= 0 {
		panic(fmt.Errorf("wrong len %v", totalLen))
	}

	buf := make([]byte, totalLen-4)
	n, err := io.ReadFull(r, buf)
	if err != nil {
		return nil, err
	}

	if n != int(totalLen-4) {
		return nil, ErrShortRead
	}
	return buf, nil
}
