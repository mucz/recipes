package frame

import (
	"bytes"
	"encoding/binary"
	"math/rand"
	"testing"
	"time"
)

func TestNewMyFrameCodec(t *testing.T) {
	codec := NewMyFrameCodec()
	if codec == nil {
		t.Errorf("want non-nil, actual nil")
	}
}

func TestMyFrameCodec_Decode(t *testing.T) {
	TestMyFrameCodec_Encode(t)

	codec := NewMyFrameCodec()
	rand.Seed(time.Now().UnixNano())
	const letterBytes = "abcdefghijklmnopqrstuvwzxyz"
	for i := 0; i < 128; i++ {
		buf := make([]byte, 0, 128)
		rw := bytes.NewBuffer(buf)

		b := make([]byte, i)
		for j := range b {
			b[j] = letterBytes[rand.Intn(len(letterBytes))]
		}
		var str string = string(b)

		err := codec.Encode(rw, []byte(str))
		if err != nil {
			t.Errorf("want nil,actual %s", err.Error())
		}

		ans, err := codec.Decode(rw)
		if err != nil {
			t.Errorf("want nil,actual %s", err.Error())
		}
		if string(ans) != str {
			t.Errorf("want %s, actual %s", str, string(ans))
		}
	}

}

func TestMyFrameCodec_Encode(t *testing.T) {
	codec := NewMyFrameCodec()
	rand.Seed(time.Now().UnixNano())

	const letterBytes = "abcdefghijklmnopqrstuvwzxyz"

	for i := 1; i < 126; i++ {

		buf := make([]byte, 0, 128)
		rw := bytes.NewBuffer(buf)

		b := make([]byte, i)
		for j := range b {
			b[j] = letterBytes[rand.Intn(len(letterBytes))]
		}
		var str string = string(b)

		err := codec.Encode(rw, []byte(str))
		if err != nil {
			t.Errorf("want nil,actual %s", err.Error())
		}

		var totalLen int32
		err = binary.Read(rw, binary.BigEndian, &totalLen)
		if err != nil {
			t.Errorf("want nil, actual %s", err.Error())
		}
		if totalLen != int32(4+len(str)) {
			t.Errorf("want %v, actual %v", int32(4+len(str)), totalLen)
		}

		left := rw.Bytes()
		if string(left) != str {
			t.Errorf("want %s,actual %s", str, string(left))
		}
	}

}
