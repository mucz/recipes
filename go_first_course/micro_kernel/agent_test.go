package micro_kernel

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"
)

type DemoCollector struct {
	evtReceiver EventReceiver
	agtCtx      context.Context
	stopChan    chan struct{}
	name        string
	content     string
}

func NewCollector(name string, content string) *DemoCollector {
	return &DemoCollector{
		stopChan: make(chan struct{}),
		name:     name,
		content:  content,
	}
}

func (c *DemoCollector) Init(evtReceiver EventReceiver) error {
	fmt.Println("init Collector", c.name)
	c.evtReceiver = evtReceiver
	return nil
}

func (c *DemoCollector) Start(agtCtx context.Context) error {
	fmt.Println("init Collector", c.name)
	for {
		select {
		case <-agtCtx.Done():
			c.stopChan <- struct{}{}
			break
		default:
			// ! main logic
			time.Sleep(time.Second * 1)
			// ! send event to receiever
			c.evtReceiver.OnEvent(Event{c.name, c.content})
		}
	}
}

func (c *DemoCollector) Stop() error {
	fmt.Println("stop Collector", c.name)
	select {
	case <-c.stopChan:
		return nil
	case <-time.After(time.Second * 1):
		return errors.New("failed to stop : timeout")
	}

}

func (c *DemoCollector) Destory() error {
	fmt.Println("destroy Collector", c.name)
	return nil
}

func TestAgent(t *testing.T) {
	agt := NewAgent(100)
	c1 := NewCollector("c1", "1")
	c2 := NewCollector("c2", "2")
	agt.registerCollector("c1", c1)
	agt.registerCollector("c2", c2)

	// ! check normal start routine
	if err := agt.start(); err != nil {
		fmt.Printf("start error %v \n", err)
	}

	// ! check wrong state notification
	fmt.Println(agt.start())

	time.Sleep(time.Second * 1)
	agt.stop()
	agt.destroy()
}
