package micro_kernel

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"sync"
)

const (
	Waiting = iota
	Running
)

var WrongStateError = errors.New("can not take the operation int the current state")

// ! A errors wrapper
type CollectorsError struct {
	CollectorsError []error
}

func (ce CollectorsError) Error() string {
	var strs []string
	for _, err := range ce.CollectorsError {
		strs = append(strs, err.Error())
	}
	return strings.Join(strs, ";")
}

type Event struct {
	Source  string
	Content string
}

type EventReceiver interface {
	OnEvent(evt Event)
}

// ! A plugin
type Collector interface {
	Init(evtReceiver EventReceiver) error
	Start(agtCtx context.Context) error
	Stop() error
	Destory() error
}

// ! implementation of EventReceiver
// ! Main thread
type Agent struct {
	collectors map[string]Collector
	evtBuf     chan Event
	cancel     context.CancelFunc
	ctx        context.Context
	state      int
}

func NewAgent(sizeEvtBuf int) *Agent {
	agt := Agent{
		collectors: map[string]Collector{},
		evtBuf:     make(chan Event, sizeEvtBuf),
		state:      Waiting,
	}
	return &agt
}

func (agt *Agent) registerCollector(name string, collector Collector) error {
	if agt.state != Waiting {
		return WrongStateError
	}
	agt.collectors[name] = collector
	return collector.Init(agt)
}

func (agt *Agent) startCollectors() error {
	var err error
	var errs CollectorsError // * guarded_by_mutex
	var mutex sync.Mutex

	for name, collector := range agt.collectors {
		// ! Create a new goroutine to Start the collector
		go func(name string, collector Collector, ctx context.Context) {
			defer func() {
				mutex.Unlock()
			}()
			err = collector.Start(ctx)

			mutex.Lock()
			if err != nil {
				errs.CollectorsError = append(errs.CollectorsError,
					errors.New(name+" : "+err.Error()))
			}

		}(name, collector, agt.ctx)
	}

	if len(errs.CollectorsError) == 0 {
		return nil
	} else {
		return errs
	}
}

func (agt *Agent) stopCollectors() error {
	var err error
	var errs CollectorsError // * guarded_by_mutex

	for name, collector := range agt.collectors {
		err = collector.Stop()

		if err != nil {
			errs.CollectorsError = append(errs.CollectorsError,
				errors.New(name+" : "+err.Error()))
		}
	}

	if len(errs.CollectorsError) == 0 {
		return nil
	} else {
		return errs
	}
}

func (agt *Agent) destroyCollectors() error {
	var err error
	var errs CollectorsError // * guarded_by_mutex
	for name, collector := range agt.collectors {
		err = collector.Destory()
		if err != nil {
			errs.CollectorsError = append(errs.CollectorsError,
				errors.New(name+" : "+err.Error()))
		}
	}

	if len(errs.CollectorsError) == 0 {
		return nil
	} else {
		return errs
	}

}

func (agt *Agent) start() error {
	// * check and init agt
	if agt.state != Waiting {
		return WrongStateError
	}
	agt.state = Running
	agt.ctx, agt.cancel = context.WithCancel(context.Background())

	go agt.EventProcessGoroutine()
	return agt.startCollectors()
}

func (agt *Agent) stop() error {
	if agt.state != Running {
		return WrongStateError
	}
	agt.state = Waiting
	agt.cancel()
	return agt.stopCollectors()
}

func (agt *Agent) destroy() error {
	if agt.state != Waiting {
		return WrongStateError
	}
	return agt.destroyCollectors()
}

func (agt *Agent) OnEvent(evt Event) {
	// * store events
	agt.evtBuf <- evt
}

func (agt *Agent) EventProcessGoroutine() {
	var evtSeg [10]Event
	for {
		// * read events
		for i := 0; i < 10; i++ {
			select {
			case evtSeg[i] = <-agt.evtBuf:
			case <-agt.ctx.Done():
				return
			}
		}
		// * handle events
		fmt.Println(evtSeg)
	}
}
