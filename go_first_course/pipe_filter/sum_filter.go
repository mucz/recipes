package pipe_filter

import "errors"

var SumFilterWrongFormatError = errors.New("input data should be []int")

type SumFilter struct {
}

func NewSumFilter() *SumFilter {
	return &SumFilter{}
}

func (sf *SumFilter) Process(data Request) (Response, error) {
	// 为了范型，接口都是用的空接口，所以需要手动类型检查
	// ! type assert
	elems, ok := data.([]int)
	if !ok {
		return nil, SumFilterWrongFormatError
	}

	// * Bussiness logic
	ret := 0
	for _, elem := range elems {
		ret += elem
	}
	return ret, nil
}
