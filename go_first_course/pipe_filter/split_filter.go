package pipe_filter

import (
	"errors"
	"strings"
)

var SplitFilterWrongFormatError = errors.New("input data should be string")

type SplitFilter struct {
	delimiter string
}

func NewSplitFilter(delimiter string) *SplitFilter {
	return &SplitFilter{delimiter}
}

func (sf *SplitFilter) Process(data Request) (Response, error) {
	// ! type assert
	str, ok := data.(string)
	if !ok {
		return nil, SplitFilterWrongFormatError
	}

	//  * Bussiness logic
	parts := strings.Split(str, sf.delimiter)
	return parts, nil
}
