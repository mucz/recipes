package pipe_filter

import "testing"

func TestStraightPipeline_Process(t *testing.T) {
	spliter := NewSplitFilter(",")
	converter := NewToIntFilter()
	sum := NewSumFilter()
	sp := NewStraightPipeline("test_pipeline", spliter, converter, sum)
	ret, err := sp.Process("1,2,3,4,5")
	if err != nil {
		t.Fatal(err)
	}
	if ret != (1 + 2 + 3 + 4 + 5) {
		t.Fatalf("answer wrong!")
	}
}
