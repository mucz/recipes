package pipe_filter

type StraightPipeline struct {
	Name    string
	Filters *[]Filter
}

// ? 实用trick
// ? 多参数->结构体成员
// ? multi-args -> member of struct
func NewStraightPipeline(name string, filters ...Filter) *StraightPipeline {
	return &StraightPipeline{
		Name:    name,
		Filters: &filters,
	}
}

func (f *StraightPipeline) Process(data Request) (Response, error) {
	var ret interface{}
	var err error
	// ! 需要手动保证传入的Filter的顺序和参数类型对应
	for _, filter := range *f.Filters {
		ret, err = filter.Process(data)
		if err != nil {
			return ret, err
		}
		data = ret
	}
	return ret, err
}
