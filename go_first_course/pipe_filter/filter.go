package pipe_filter

// ! 空接口实现范型
// ! 代价： 缺少编译期检查->需要手动检查类型 && 缺少Ide提示，需要手动注释说明类型

type Request interface{}

type Response interface{}

type Filter interface {
	Process(date Request) (Response, error)
}
