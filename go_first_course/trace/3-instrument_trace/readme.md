
本文件夹内代码功能与2的完全相同，只是为了演示如何打包一组程序作为库使用
- go mod init **gitee.com/mucz/recipes/instrument_trace**
- package 名为 trace
- 去掉了测试用的main，改成了_test.go的形式,并且给出了应该显示的输出,即测试的正确答案

- 增加一个自动注入Trace函数的工具，在cmd/instrument中 

- 使用

``` bash
  make 
  ./instrument -w examples/demo/demo.go
```

- 提示ok后，可见demo.go的函数已被自动注入Trace函数，可以使用`go run demo.go`查看trace效果