package main

// 分析代码抽象语法树，向其中加入defer trace.Trace()() 的调用，实现打印堆栈，不重复加入

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	// import同库下的两个包
	"gitee.com/mucz/recipes/instrument_trace/instrumenter"
	myast "gitee.com/mucz/recipes/instrument_trace/instrumenter/ast"
)

var (
	wrote bool
)

func init() {
	flag.BoolVar(&wrote, "w", false, "write result to (source) file instead of stdout")
}

func usage() {
	fmt.Println("instrument [-w] xxx.go")
	flag.PrintDefaults()
}

func main() {
	fmt.Println(os.Args)
	flag.Usage = usage
	flag.Parse() // 解析命令行参数

	if len(os.Args) < 2 {
		usage()
	}

	var file string
	if len(os.Args) == 3 {
		file = os.Args[2]
	}
	if len(os.Args) == 2 {
		file = os.Args[1]
	}
	if filepath.Ext(file) != ".go" { // 对源文件扩展名进行扩展
		usage()
		return
	}

	var ins instrumenter.Instrumenter = myast.New("gitee.com/mucz/recipes/instrument_trace", "trace", "Trace")
	// 创建以ast方式实现Instrumenter接口的ast.instrumenter 实例
	newSrc, err := ins.Instrument(file) // 向go源文件中注入Trace函数
	if err != nil {
		panic(err)
	}
	if newSrc == nil {
		// no changes on source file
		fmt.Printf("no trace added for %s \n", file)
		return
	}

	if !wrote {
		fmt.Println(string(newSrc)) //输出到stdout
		return
	}

	// 写回源文件
	if err = ioutil.WriteFile(file, newSrc, 0666); err != nil {
		fmt.Printf("write %s error: %v\n", file, err)
		return
	}
	fmt.Printf("instrument trace for %s ok\n", file)
}
