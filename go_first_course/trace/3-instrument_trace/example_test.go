// test
package trace_test

import (
	trace "gitee.com/mucz/recipes/instrument_trace"
)

func a() {
	defer trace.Trace()()
	b()
}

func b() {
	defer trace.Trace()()
	c()
}

func c() {
	defer trace.Trace()()
	d()
}

func d() {
	defer trace.Trace()()
}

func ExampleTrace() {
	// 请注意格式，a()下面的注释格式一点也不能更改,否则会提示无测试
	a()
	// Output:
	// g[00001]:    ->gitee.com/mucz/recipes/instrument_trace_test.a
	// g[00001]:        ->gitee.com/mucz/recipes/instrument_trace_test.b
	// g[00001]:            ->gitee.com/mucz/recipes/instrument_trace_test.c
	// g[00001]:                ->gitee.com/mucz/recipes/instrument_trace_test.d
	// g[00001]:                <-gitee.com/mucz/recipes/instrument_trace_test.d
	// g[00001]:            <-gitee.com/mucz/recipes/instrument_trace_test.c
	// g[00001]:        <-gitee.com/mucz/recipes/instrument_trace_test.b
	// g[00001]:    <-gitee.com/mucz/recipes/instrument_trace_test.a

}
