// 实现自动注入Trace的helper
package instrumenter

// 接受源文件路径，返回注入Trace函数的文件内容和error类型值
// !注入Trace函数的实现方法可以有多种，面向接口编程
type Instrumenter interface {
	Instrument(string) ([]byte, error)
}
