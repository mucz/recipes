package main

import "runtime"

func Trace(name string) func() {
	println("enter: ", name)
	return func() {
		println("exit :", name)
	}
}

// right practice
func Trace_without_args() func() {
	pc, _, _, ok := runtime.Caller(1)
	if !ok {
		panic("not found Caller")
	}

	fn := runtime.FuncForPC(pc)
	name := fn.Name()

	println("enter:", name)
	return func() {
		println("exit :", name)
	}
}

func foo() {
	// defer Trace("foo")()  // 不加最后的() 会发生什么？
	defer Trace_without_args()()
	bar()
}

func bar() {
	// defer Trace("bar")()
	defer Trace_without_args()()
}

func main() {
	// defer Trace("main")()
	defer Trace_without_args()()
	foo()
}
