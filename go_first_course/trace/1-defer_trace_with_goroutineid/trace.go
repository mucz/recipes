package main

import (
	"bytes"
	"fmt"
	"runtime"
	"strconv"
	"sync"
)

// cant print goroutine id
func Trace_without_args() func() {
	pc, _, _, ok := runtime.Caller(1)
	if !ok {
		panic("not found Caller")
	}

	fn := runtime.FuncForPC(pc)
	name := fn.Name()

	println("enter:", name)
	return func() {
		println("exit :", name)
	}
}

// -----------------------------------
var goroutineSpace = []byte("goroutine ")

func curGoroutineID() uint64 {
	b := make([]byte, 64)
	b = b[:runtime.Stack(b, false)]
	// parse the 4707 out of "goroutine 4704 [ "
	b = bytes.TrimPrefix(b, goroutineSpace)
	i := bytes.IndexByte(b, ' ')
	if i < 0 {
		panic(fmt.Sprintf("No space found in %q", b))
	}
	b = b[:i]
	n, err := strconv.ParseUint(string(b), 10, 64)
	if err != nil {
		panic(fmt.Sprintf("Failed to parse gorouine ID out of %q: %v", b, err))
	}
	return n

}

func Trace_with_goroutineid() func() {
	pc, _, _, ok := runtime.Caller(1)
	if !ok {
		panic("not found Caller")
	}

	fn := runtime.FuncForPC(pc)
	name := fn.Name()

	gid := curGoroutineID()
	fmt.Printf("g[%05d]: enter: [%s]\n", gid, name)
	return func() {
		fmt.Printf("g[%05d]: exit : [%s]\n", gid, name)
	}
}

// -----------------------------------

func A1() {
	defer Trace_with_goroutineid()()
	B1()
}

func B1() {
	defer Trace_with_goroutineid()()
	C1()
}

func C1() {
	defer Trace_with_goroutineid()()
	D()
}

func D() {
	defer Trace_with_goroutineid()()
}

func A2() {
	defer Trace_with_goroutineid()()
	B2()
}

func B2() {
	defer Trace_with_goroutineid()()
	C2()
}

func C2() {
	defer Trace_with_goroutineid()()
	D()
}

func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		A2()
		wg.Done()
	}()

	wg.Wait()
	A1()
}
