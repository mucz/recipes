package type_test

import "testing"

func TestImplicit(t *testing.T) {
	var a int = 1
	var b int64 = 1
	// b = a // wrong ! no implicite cast in go
	t.Log(a, b)

	var a32 int32 = 1
	// b = a32 // wrong
	t.Log(a32, b)
}

type MyInt int64

func TestAlias(t *testing.T) {
	var c MyInt
	var a int64 = 1
	// c = a        // wrong ! no implicite cast
	c = MyInt(a) // correct explicite cast ok
	t.Log(a, c)
}

func TestPointer(t *testing.T) {
	a := 1
	aPtr := &a
	t.Log(a, aPtr)
	t.Logf("%T %T", a, aPtr)

	// aPtr = aPtr + 1 // invalid operation ! no pointer calcul
}

func TestString(t *testing.T) {
	var s string
	t.Log("*" + s + "*") // s initializes as void("") not nil!!!
	t.Log(len(s))        // len(s) = 0
	if s == "" {
		t.Log("string initialized as void string")
	}
	if s == nil { // wrong
		t.Log("string initialized as nil")
	}
}
