package function_test

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func returnMutliValues() (int, int) {
	return rand.Intn(10), rand.Intn(20)
}

// decorator
func timeSpent(inner func(op int) int) func(op int) int {
	return func(n int) int {
		start := time.Now()
		ret := inner(n)
		fmt.Println("time spent", time.Since(start).Seconds())
		return ret
	}
}

func slowFun(op int) int {
	time.Sleep(time.Second * 1)
	return op
}

func TestFn(t *testing.T) {
	a, _ := returnMutliValues()
	t.Log(a)

	tsSF := timeSpent(slowFun)
	t.Log(tsSF(10))
}
