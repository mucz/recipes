package function_test

import (
	"fmt"
	"testing"
)

func Clear() {
	fmt.Println("Clear Resouces")
}

func TestDefer(t *testing.T) {
	defer Clear() // will be done even panic happend
	fmt.Println("Start")
	panic("err")

	fmt.Println("Clear Resouces") // unreachable
}
