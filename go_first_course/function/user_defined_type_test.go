package function_test

import (
	"fmt"
	"testing"
	"time"
)

// * Alias
type IntConv func(op int) int // * user-defined 类型，用来省字

func timeSpent_(inner IntConv) IntConv {
	return func(n int) int {
		start := time.Now()
		ret := inner(n)
		fmt.Println("time spent:", time.Since(start).Seconds())
		return ret
	}
}

func slowFun_(op int) int {
	time.Sleep(time.Second * 1)
	return op
}

func TestFn_(t *testing.T) {
	tsSF := timeSpent_(slowFun_)
	t.Log(tsSF(10))
}
