package map_factory

import "testing"

func TestMapWithFunValue(t *testing.T) { // a simple factory of methods
	m := map[int]func(op int) int{} //value is a func
	m[1] = func(op int) int { return op }
	m[2] = func(op int) int { return op * op }
	m[3] = func(op int) int { return op * op * op }
	// t.Log(m[1], m[2], m[3])  // wrong
	t.Log(m[1](2), m[2](2), m[3](2))
}

func TestSet_based_on_Map(t *testing.T) {
	mySet := map[int]bool{}
	mySet[1] = true
	if mySet[1] {
		t.Log("1 exists in set")
	} else {
		t.Log("1 don't exist in set")
	}

	if mySet[3] {
		t.Log("3 exists in set")
	} else {
		t.Log("3 don't exist in set")
	}

	// len
	t.Log("set size:", len(mySet))

	// delete
	delete(mySet, 1)
	t.Log("set size:", len(mySet))
	if mySet[3] {
		t.Log("3 exists in set")
	} else {
		t.Log("3 don't exist in set")
	}
}
