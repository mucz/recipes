package workerpool

//! workerpool but with functional option

import (
	"errors"
	"fmt"
	"sync"
)

const (
	defaultCapacity = 3
	maxCapacity     = 10
)

var (
	ErrWorkerPoolFreed      = errors.New("workerpool freed")
	ErrorNoIdleWorkerInPool = errors.New("workerpool full")
)

type Option func(*Pool)

func WithBlock(block bool) Option {
	return func(p *Pool) {
		p.block = block
	}
}

func WithPreAllocWorkers(preAlloc bool) Option {
	return func(p *Pool) {
		p.preAlloc = preAlloc
	}
}

type Task func()
type Pool struct {
	capacity int
	preAlloc bool

	active chan struct{}
	tasks  chan Task
	block  bool

	wg   sync.WaitGroup
	quit chan struct{}
}

func (p *Pool) returnTask(t Task) {
	go func() {
		p.tasks <- t
	}()
}
func (p *Pool) run() {
	idx := len(p.active)

	if !p.preAlloc {
	loop:
		for t := range p.tasks {
			p.returnTask(t)
			select {
			case <-p.quit:
				return
			case p.active <- struct{}{}:
				idx++
				p.newWorker(idx)
			default:
				break loop
			}
		}
	}

	for {
		select {
		case <-p.quit:
			return
		case p.active <- struct{}{}:
			idx++
			p.newWorker(idx) // new a goroutine
		}
	}
}

func (p *Pool) newWorker(i int) {
	p.wg.Add(1)
	go func() {

		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("woker[%03d]: recover panic[%s] and exit\n", i, err)
				<-p.active
			}
			p.wg.Done()
		}()

		fmt.Printf("woker [%03d]: start\n", i)

		for {
			select {
			case <-p.quit:
				fmt.Printf("worker[%03d]: exit\n", i)
				<-p.active
				return
			case t := <-p.tasks:
				fmt.Printf("worker[%03d]: receive a task\n", i)
				t()
			}
		}
	}()
}

func (p *Pool) Schedule(t Task) error {
	select {
	case <-p.quit:
		return ErrWorkerPoolFreed
	case p.tasks <- t:
		return nil
	default:
		if p.block {
			p.tasks <- t
			return nil
		}
		return ErrorNoIdleWorkerInPool
	}
}

func New(cap int, opts ...Option) *Pool {
	if cap <= 0 {
		cap = defaultCapacity
	}
	if cap > maxCapacity {
		cap = maxCapacity
	}
	p := &Pool{
		capacity: cap,
		tasks:    make(chan Task),
		quit:     make(chan struct{}),
		active:   make(chan struct{}, cap),
	}
	// configure pool
	for _, opt := range opts {
		opt(p)
	}

	fmt.Printf("wokerpool start \n")

	if p.preAlloc {
		for i := 0; i < p.capacity; i++ {
			p.newWorker(i + 1)
			p.active <- struct{}{}
		}
	}
	go p.run()
	return p
}

func (p *Pool) Free() {
	//	ver 1
	//for {
	//	select{
	//	case p.quit<-struct{}{}:
	//
	//	default:
	//		return
	//	}
	//}

	// ver2
	close(p.quit)
	p.wg.Wait()
	fmt.Print("Workerpool freed!\n")

}
