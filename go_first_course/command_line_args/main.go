package main

// echo command line args
import (
	"fmt"
	"os"
)

func main() {
	// method 1
	var s, sep string
	for i := 1; i < len(os.Args); i++ {
		s += sep + os.Args[i]
		sep = " "
	}
	fmt.Println(s)

	// method 2
	s = ""
	sep = ""
	for _, arg := range os.Args[1:] {
		s += sep + arg
		sep = " "

		fmt.Println(s)

		// method 3
		for i, arg := range os.Args[1:] {
			fmt.Printf("%d : %s\n", i, arg)
		}
	}
}
