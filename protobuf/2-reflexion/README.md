- 当一个proto协议中定义多种message的时候，如何反序列化？
- 关键问题在于如何创建出一个对应的对象？
  - 错的方法:
    ```c++
    if(type_name == "answer"){
        Answer answer;
        answer.ParseFromString(str);
    } else if (type_name == "query"){
        ...
    } else if ...
    ```
  - 本例程演示正确的方法