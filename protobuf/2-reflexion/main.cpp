#include <iostream>
#include <string>
#include <assert.h>
#include <typeinfo>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/message.h>
#include "query.pb.h"

using namespace std;
using namespace google::protobuf;

typedef tutorial::Query T;
int main(int argc, char* argv[]){
    // { // mock
        string type_name = T::descriptor()->full_name();
        cout<<type_name<<endl; 
    // }

    // ?接收端只知道type_name, 如何创建出对应的实例从而解析？

    // !第一步:根据type_name得到描述符
    const Descriptor* descriptor = DescriptorPool::generated_pool()->FindMessageTypeByName(type_name);

    // { // test
        assert(descriptor == T::descriptor());
        cout <<"FindMessageTypeByName() =" << descriptor << endl;
        cout << "T::descriptor()    = " << T::descriptor()<<endl;
        cout <<endl;
    // }

    // !第二步:根据描述符，从工厂处得到原型
    const Message* prototype = MessageFactory::generated_factory()->GetPrototype(descriptor);

    // { // test
        assert(prototype == &T::default_instance());
        cout<< "GetPrototype()  =  "<< prototype <<endl;
        cout<< "T::default_instance() = "<<&T::default_instance()<<endl;
        cout<<endl;
    // } 
    
    // !第三步:利用工厂模式，从原型处再得到一个实例，这里动态转形是没有必要的
    T* new_obj = dynamic_cast<T*>(prototype->New());

    // { // test
        assert(new_obj);
        assert(new_obj != prototype);
        assert(typeid(*new_obj) == typeid(T::default_instance()));
        cout << "prototype->New() = "<<new_obj <<endl;
        cout <<endl;
    // }

    // !第四步:新的实例是new出来的，用户有责任释放（或者使用智能指针接管）
    delete new_obj;
}