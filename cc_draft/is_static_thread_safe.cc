#include <thread>
#include <iostream>

int create(int a );
int main(int argc, char **argv)
{

    // ? workerAB用来演示static局部变量在初始化时是线程安全的
    std::thread workerB([]() {
    //  ! std::cout not thread safe
    //    std::cout << "B start time: " << std::chrono::system_clock::now().time_since_epoch().count() << std::endl;
        printf("B start time : %d\n ",std::chrono::system_clock::now().time_since_epoch().count());
        int b  = create(1);       
        printf("B: %d\n ",b);
    });
    std::thread workerA([]() {
        printf("A start time : %d\n ",std::chrono::system_clock::now().time_since_epoch().count());
        int a = create(1); //?　如果C++11之前，这里还没有完成初始化，会不知道发生什么，但是C++11之后会在这里隐式阻塞，等待单例初始化完成
        printf("A: %d\n ",a);
    });
    workerA.join();
    workerB.join();

    create(-2); // 归零

    // ? wokerCD用来演示在初始化之后，依旧是不安全的，没有保护
    std::thread workerC([]() {
        for(int i = 1; i<10000; i++){
            int b  = create(1);       
            // printf("C: %d\n ",b);
        }
    });
    std::thread workerD([]() {
        for(int i = 1; i<10000; i++){
            int b  = create(-1);        
            // printf("D: %d\n ",b);
        }
    });

    workerC.join();
    workerD.join();
    // ? final value 不是0 说明存在数据异常,即在初始化之后没有锁的保护
    printf("final value : %d\n",create(0));
}

    
    


int create(int a)
{
    static struct M {
      int value;
      M(): value(0) {
          printf("M created\n");
          std::this_thread::sleep_for(std::chrono::seconds(5));
      }
    } m;

    // printf(" time : %d\n ",std::chrono::system_clock::now().time_since_epoch().count());
    int ret = m.value;
    m.value+=a;
    return ret;
}