#include <iostream> 
using namespace std;
int func(){
    int a = 1;
    cout<<"Func1 Called!"<<endl;
    return 0;
}

int func2(){
    int b = 2;
    cout<<"Func2 Called!"<<endl;
    return b;
}

int main(){
    
    // 两个函数都会被调用，result会被赋予第二个值
    int result = (func(), func2());
    cout<< "result == "<< result << endl;

    // 只有第一个会被调用,第二个调用无效！
    int result2 = func(),func2();
    cout<< "result == "<< result << endl;

    return result;
}