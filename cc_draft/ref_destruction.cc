#include <iostream>
#include <unistd.h>

using namespace std;

class foo {
public:
    foo(int v):a(v) {
        cout<<"foo created = "<<a<<endl;
    }

    ~foo(){ 
        cout<<"foo destructed = "<<a<<endl;
    }

    void show(){ 
        cout<<"foo val = "<<a<<endl;
    }

private:
    int a{};
};

foo& return_ref(){
    auto f = new foo(2);
    return *f;
}

foo& return_stack_ref(){
    auto f = foo(3);
    return f;
}

int main(){
    foo a(1);
    auto &ref = a;
    ref.show();

    auto ref_2 = return_ref();
    ref_2.show();
    sleep(2);

    auto ref_3 = return_stack_ref();
    ref_3.show(); // seg fault
}
