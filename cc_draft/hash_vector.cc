#include <iostream>
#include <map>
#include <set>
#include <unordered_set>
#include <vector>
using namespace std;

struct VectorHash {
    size_t operator()(const std::vector<int>& v) const {
        std::hash<int> hasher;
        size_t seed = 0;
        for (int i : v) {
            seed ^= hasher(i) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        }
        return seed;
    }
};

int main(){
   set<vector<int>> sets; 
   sets.insert({1,2,3,4,5});
   sets.insert({4,5,2,4,5});
   cout << sets.size() << " " <<sets.max_size()<<endl;

//    not ok
    // unoredered_set<vector<int>> hash_sets;
   
//    ok
   using MySet = std::unordered_set<std::vector<int>, VectorHash>;
   MySet hash_sets;
   hash_sets.insert({1,2,3,4,5});
   hash_sets.insert({4,5,2,4,5});
   cout << hash_sets.size() << " " <<hash_sets.max_size()<<endl;
}