#include <vector>
#include <iostream>
#include <algorithm> // Needed for swap

using namespace std;

int partition(vector<int>& nums, int low, int high){
    // [low,high]
    int pivot = nums[high];
    int left  = low-1; // ! low -1 , 保证left在i左面
    for(int i = low; i <= high-1; i++){ // ! high - 1
        if(nums[i] <= pivot){
            left++;  // ! 先加后移动
            swap(nums[left],nums[i]);
        }  
    }
    swap(nums[left+1],nums[high]); // ! left+1
    return left+1;
}

void quickSort(vector<int>& nums, int low,int high){
    if(low<high){
        int pi = partition(nums,low,high);
        quickSort(nums,low,pi-1);
        quickSort(nums,pi+1,high);
    }
}


int main(){
    vector<int> nums{5,2,3,1,4,7,10,12,-1,33,222,-33};
    // vector<int> nums{1,2,3,4,5,6,6,7,8,9,10};
    // vector<int> nums{10,9,8,7,6,5,4,3,2,1};

    int size = nums.size();
    for(int x : nums){
        cout<< x << " ";
    }
    cout<<endl;

    quickSort(nums, 0, size-1);

    for(int x : nums){
        cout<< x << " ";
    }
    cout<<endl;
	return 0;
}
