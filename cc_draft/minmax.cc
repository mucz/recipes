#include <vector>
#include <algorithm>
#include <numeric>
#include <functional>


// 这里使用的算法复杂度是3n/2-2(如果认为加法没有复杂度，只算比较数量） 
// 或者2n-3（如果认为加法和比较都算1，除法不算)
// 普通的扫描方法的复杂度是2n-2
// 只是为了演示，并没有太大实际意义
using namespace std;

int main(){
    vector<int> arr{1,2,3,4,5};
    function<pair<int,int>(int,int)> minmax = [&](int low,int high){
        if(high-low <= 1)   {
            if(arr[low]<arr[high])
                return make_pair(arr[low],arr[high]);
            return make_pair(arr[high],arr[low]);
        } else {
            int mid = (low+high)/2 ;
            pair<int,int> a1 = minmax(low,mid);
            pair<int,int> a2 = minmax(mid+1,high);
            int l1 = a1.first;
            int h1 = a1.second;
            int l2 = a2.first;
            int h2 = a2.second;
            int l = min(l1,l2);
            int h = max(h1,h2);
            return make_pair(l,h);
        }
    };
    auto ans = minmax(0,arr.size()-1);
    fprintf(stdout, "min%d max%d\n",ans.first,ans.second);
};