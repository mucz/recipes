
int main(){
    // dangerous!
    {
        int *p;
        if(true)
        {
            int x;
            p = &x;
        }

        *p = 3;//runtime error
    }

    { // seg fault
        int a[10];
        a[11] = 1;
    }

    { // segfault 
        int *ptr = 0;
        *ptr = 0;
    }

}