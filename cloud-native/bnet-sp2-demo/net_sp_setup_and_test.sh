# !/bin/bash
# ! A script to deploy a dummy Client-Router-Router-Service Network with K8s 
# ! assume that cluster already exists
# ! default shell in container : bash

source ./env.sh
source ./check_error.sh

# create pods
echo -e " -- updating pods ...\n "
kubectl delete -f $DEP_FILE  2>/dev/null
kubectl apply -f $DEP_FILE   

# wait for the deployment completion
echo -e " "
echo -e " -- waiting for the deployment completion ...\n"
kubectl rollout status deployment c-router >/dev/null
kubectl rollout status deployment s-router >/dev/null
kubectl rollout status deployment server   >/dev/null
kubectl rollout status deployment client   >/dev/null
echo -e " -- deployed !\n"

# get pods names and ips
export C_Router_Name=$(kubectl get pod | grep c-router |tail -1| cut -d' ' -f1)
export C_Name=$(kubectl get pod | grep client | tail -1| cut -d' ' -f1)
export S_Router_Name=$(kubectl get pod | grep s-router |tail -1| cut -d' ' -f1)
export S_Name=$(kubectl get pod | grep server | tail -1| cut -d' ' -f1)

export C_Router_IP=$(kubectl get pod $C_Router_Name -o go-template --template '{{.status.podIP}}{{"\n"}}')
export S_Router_IP=$(kubectl get pod $S_Router_Name -o go-template --template '{{.status.podIP}}{{"\n"}}')
export S_IP=$(kubectl get pod $S_Name -o go-template --template '{{.status.podIP}}{{"\n"}}')
export C_IP=$(kubectl get pod $C_Name -o go-template --template '{{.status.podIP}}{{"\n"}}')

# check ping connectivity
echo -e " -- checking connectivity ... \n "
check_connectivity

# configure pods' routing rules
echo -e " -- changing route table ... \n"
kubectl exec $C_Name -- ip route add $S_IP via $C_Router_IP
kubectl exec $S_Name -- ip route add $C_IP via $S_Router_IP

# check ping connectivity
echo -e " -- checking connectivity ...\n "
check_connectivity

# check http connectivity
check_http_connectivity

# check netem
echo -e " -- checking routing rules ... "
echo -e "    client fetching a web page from server ...  \n "
echo -e "    client \u2192 c-router \u2192 server "
echo -e "       \u2196                  \u2199 "
echo -e "           \u2190 s-router \u2190    "
echo -e " "

echo -n " -- Fetch Time WITHOUT delay on router:     "
kubectl exec $C_Name -- bash -c "TIMEFORMAT='%3R' && time (curl $S_IP:8000 1>/dev/null 2>/dev/null)" 

# add 2 secs delay to client-side-router's adapter 
kubectl exec $C_Router_Name -- tc qdisc add dev eth0 root netem delay 2000ms

echo -n " -- Fetch Time WITH 2000ms delay on C_router: "
kubectl exec $C_Name -- bash -c "TIMEFORMAT='%3R' && time (curl $S_IP:8000 1>/dev/null 2>/dev/null)" 

# add 2 secs delay to server-side-router's adapter 
kubectl exec $C_Router_Name -- tc qdisc delete dev eth0 root netem delay 0
kubectl exec $S_Router_Name -- tc qdisc add dev eth0 root netem delay 2000ms
echo -n " -- Fetch Time WITH 2000ms delay on S_router: "
kubectl exec $C_Name -- bash -c "TIMEFORMAT='%3R' && time (curl $S_IP:8000 1>/dev/null 2>/dev/null)" 

# add 1 sec delay to 2 routers on each sides
kubectl exec $C_Router_Name -- tc qdisc add dev eth0 root netem delay 1000ms
kubectl exec $S_Router_Name -- tc qdisc replace dev eth0 root netem delay 1000ms
echo -n " -- Fetch Time WITH 1000ms delay on both routers: "
kubectl exec $C_Name -- bash -c "TIMEFORMAT='%3R' && time (curl $S_IP:8000 1>/dev/null 2>/dev/null)" 

kubectl exec $C_Router_Name -- tc qdisc delete dev eth0 root netem delay 0
kubectl exec $S_Router_Name -- tc qdisc delete dev eth0 root netem delay 0