#!/bin/bash
# build the docker image for bnet-sp2

IMAGE_NAME="test_ubuntu"
DOCKER_USER_NAME="mucz"

docker build -t $IMAGE_NAME .
docker tag $IMAGE_NAME $DOCKER_USER_NAME/$IMAGE_NAME
docker login
docker push $DOCKER_USER_NAME/$IMAGE_NAME
docker pull $DOCKER_USER_NAME/$IMAGE_NAME