#!/bin/bash
# ! helper funcs for net_sp_setup_and_test.sh

function check_error {
    if [ "$?" -ne 0 ]; then
        echo " - ***** FAILED *****"
    else 
        echo " - PASSED "
    fi
}

function check_connectivity {
    echo -n "Checking connectivitiy from client $C_IP to client router $C_Router_IP"
    kubectl exec $C_Name -- ping -W 1 -c 1 $C_Router_IP > /dev/null 2>&1
    check_error
    echo -n "Checking connectivitiy from client $C_IP to server router $S_Router_IP"
    kubectl exec $C_Name -- ping -W 1 -c 1 $S_Router_IP  >/dev/null 2>&1
    check_error
    echo -n "Checking connectivitiy from client $C_IP to server        $S_IP"
    kubectl exec $C_Name -- ping -W 1 -c 1 $S_IP >/dev/null 2>&1
    check_error
    echo -n "Checking connectivitiy from server $S_IP to server router $S_Router_IP"
    kubectl exec $S_Name -- ping -W 1 -c 1 $S_Router_IP  >/dev/null 2>&1
    check_error
    echo -n "Checking connectivitiy from server $S_IP to client router $C_Router_IP"
    kubectl exec $S_Name -- ping -W 1 -c 1 $C_Router_IP  >/dev/null 2>&1
    check_error
    echo -e " "
}

function check_http_connectivity {
    echo  -n "Checking HTTP connectivity from client $C_IP to server $S_IP"
    # run a http server on pod 'server' on port 8000
    kubectl exec $S_Name -- python3 -m http.server 8000 & > /dev/null
    sleep 1
    kubectl exec $C_Name -- curl $S_IP:8000 > /dev/null 2>&1
    check_error 
    echo -e " "
}