# !/bin/bash
# add up-link delay using tc/netem to all pods in all given deployments

show_usage(){
    echo "Usage $0 [deployment names...]"
    echo "Please make sure that pods are Running"
    exit 1
}

if [ -z "$1" ] ; then
    show_usage
fi

while [ -n "$1" ] ; do
    echo ">>> Handling deployment $1"
    ./add_delay_to_dep.sh "$1"
    echo " "
    shift
done