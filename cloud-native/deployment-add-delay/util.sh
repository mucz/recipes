# !/bin/bash

rand(){ 
    max=$1 
    num=$RANDOM
    echo $(($num%$max))
}  


check_error() {
    if [ "$?" -ne 0 ]; then
        echo " - ***** FAILED *****"
    else 
        echo " - PASSED "
    fi
}