# !/bin/bash
# add up-link delay using tc/netem to all pods in a certain deployment
# TODO : add argument support for 
#      :  delay time 
#      :  other tc/netem features
#      :  ping test on/off


source ./util.sh

show_usage(){
    echo " * Usage $0 [deployment name]"
    echo " * Please make sure that pods are Running"
    exit 1
}

# * check agrs
if [ -z "$1" ] ; then
    show_usage
fi
DEP_NAME=$1
shift

# * get pods names
echo " - Finding pods under Deployment [$DEP_NAME]..."
# FIXME: name in name : if 2 deplyments called a_dep and a_dep_updated coexists, 
# this scripts can't work with a_dep since all pods under a_dep_updated will be 
# considered under a_dep too.
PODS_NAMES=$(kubectl get pod | grep -w $DEP_NAME | cut -d' ' -f1)
PODS=($PODS_NAMES)

# if no pods
if [ ${#PODS[@]} -eq 0 ]; then
    echo " * Error: Found no Pods under Deployment [$DEP_NAME] ! "
    exit 1
fi 

# show pods name
echo " - Found ${#PODS[@]} pods"
for i in "${!PODS[@]}" ; do
    echo "  · $i : ${PODS[i]}"
done


# check pods are running
# TODO: find a better way? maybe with kubectl get pod -o go-template 
echo ""
echo " - Checking pods state..."
for i in "${!PODS[@]}" ; do
    kubectl get pod ${PODS[i]} | grep -w "Running" >/dev/null
    if [ "$?" -ne 0 ]; then 
        echo " * Error: Pod <${PODS[i]}> is not Running, Please make sure all pods are running normally "
        exit 1
    fi
done
echo " - All pods are running, OK !"
echo " "

# * make sure tc is installed, then add delay using tc/netem
# ! this should be IDEMPOTENT since pods are EPHEMERAL
echo " - Adding delay"
echo " "
for i in "${!PODS[@]}" ; do 
    kubectl exec ${PODS[i]} -- tc qdisc replace dev eth0 root netem delay 60ms 20ms
    if [ "$?" -ne 0 ]; then
        echo ""
        echo " * Error: Please install tc for deployment : [$DEP_NAME] !"
        echo " * Try add \"RUN apt-get clean && apt-get -y update && apt-get -y upgrade && apt-get install -y iproute2 iputils-ping\" to Dockerfile, then re-build, re-tag and re-push"
        exit 1
    fi
done
echo " - Done!"

# * test delay
# get two random pods
if [ ${#PODS[@]} -lt 2 ] ; then
    echo " At Least 2 pods are required to do ping test"
    exit 1
fi

# TODO: handle the case of 1-pod-deployment
random_number_1=$(rand ${#PODS[@]})
random_number_2=$(rand ${#PODS[@]})
while [ $random_number_1 == $random_number_2 ] ; do 
    random_number_2=$(rand ${#PODS[@]})
done
POD_1=${PODS[$random_number_1]}
POD_2=${PODS[$random_number_2]}
echo " - Pick two random pods : <$POD_1> and <$POD_2> "

POD_1_IP=$(kubectl get pod $POD_1 -o go-template --template '{{.status.podIP}}{{"\n"}}')
POD_2_IP=$(kubectl get pod $POD_2 -o go-template --template '{{.status.podIP}}{{"\n"}}')

# check connectivity
echo " - Ping from <$POD_1> to <$POD_2>"
echo "---------------------------------------------------"
kubectl exec $POD_1 -- ping -W 1 -c 3 $POD_2_IP 
check_error
echo "---------------------------------------------------"

# TODO: make the check automatic
# remove delay to verify that traffic does be shaped
echo " - Remove up-link delay in <$POD_1> and <$POD_2>, ping will take less time ! "
kubectl exec $POD_1 -- tc qdisc replace dev eth0 root netem delay 0ms 
kubectl exec $POD_2 -- tc qdisc replace dev eth0 root netem delay 0ms 
echo " - Ping from <$POD_1> to <$POD_2>"
echo "---------------------------------------------------"
kubectl exec $POD_1 -- ping -W 1 -c 3 $POD_2_IP 
echo "---------------------------------------------------"

# restore delay
kubectl exec $POD_1 -- tc qdisc replace dev eth0 root netem delay 60ms 20ms
kubectl exec $POD_2 -- tc qdisc replace dev eth0 root netem delay 60ms 20ms