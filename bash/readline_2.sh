# !/bin/bash
# A version with filename as arg and arg check
show_usage(){
    echo "Usage $0 <filename>"
    exit 1
}

if [ -z "$1" ]; then
    show_usage
fi

cat "$1" | while read line 
do
    echo $line 
done