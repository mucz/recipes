# !/bin/bash
# 和_3 类似但引入了IFS
show_usage(){
    echo "Usage $0 <filename>"
    exit 1
}

if [ -z "$1" ]; then
    show_usage
fi

# 把分隔符换成回车，默认是回车和空格
# 这样下面的line就只以回车换行
IFS='
'

for line in $(cat "$1") # == for line in `cat "$1"`
do 
    echo $line 
done 