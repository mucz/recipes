# !/bin/bash

show_usage(){
    echo "Usage $0 <filename>"
    exit 1
}

if [ -z "$1" ]; then
    show_usage
fi

for line in `cat "$1"` # ` is not '
do 
    echo $line 
done 