package main

// todo : add cli color
// todo : better err handling
// todo : add more config (sniff, etc...)

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/fatih/color"
	"github.com/songgao/water"
	"github.com/songgao/water/waterutil"
	"github.com/urfave/cli"
	// 	"github.com/somebody's/deque"
)

// global vars
var ip_rule_table_idx int = 10
var sniff bool = false

// todo : make error handling better
func rediect_traffic_to_tun(src_device_name string, tun_name string, tun_ip string) (*water.Interface, error) {
	config := water.Config{
		DeviceType: water.TUN,
	}
	config.Name = tun_name
	// create tun dev
	ifce, err := water.New(config)
	if err != nil {
		color.Red(err.Error() + "- Fail to create tun dev: " + tun_name)
		os.Exit(1)
	}

	// make every tun device use different ip rule table
	ip_rule_table_idx++

	// make tun dev up
	if err := exec.Command("ip", "link", "set", tun_name, "up").Run(); err != nil {
		color.Red(err.Error() + "- Fail to set tun up: " + tun_name)
		os.Exit(1)
	}
	// give an ip to tun dev
	if err := exec.Command("ip", "addr", "add", tun_ip, "dev", tun_name).Run(); err != nil {
		color.Red(err.Error() + "- Fail to set tun ip: " + tun_ip)
		os.Exit(1)
	}
	// disable rp_filter
	if err := exec.Command("sysctl", "net.ipv4.conf.all.rp_filter=0").Run(); err != nil {
		color.Red(err.Error() + "- Fail to disable rp_filter: " + "all")
		os.Exit(1)
	}
	if err := exec.Command("sysctl", "net.ipv4.conf."+tun_name+".rp_filter=0").Run(); err != nil {
		color.Red(err.Error() + "- Fail to disable rp_filter: " + tun_name)
		os.Exit(1)
	}
	// add ip traffice rule : src_device -> table [ip_rule_table_idx]
	if err := exec.Command("ip", "rule", "add", "dev", src_device_name, "table", strconv.Itoa(ip_rule_table_idx)).Run(); err != nil {
		color.Red(err.Error() + "- Fail to add ip rule: " + src_device_name + " -> " + strconv.Itoa(ip_rule_table_idx))
		os.Exit(1)
	}
	// ip rule table [ip_rule_table_idx] : all traffic goto dev tun
	if err := exec.Command("ip", "route", "add", tun_ip, "dev", tun_name, "table", strconv.Itoa(ip_rule_table_idx)).Run(); err != nil {
		color.Red(err.Error() + "- Fail to add ip route: " + tun_ip + " -> " + tun_name + " table " + strconv.Itoa(ip_rule_table_idx))
		os.Exit(1)
	}
	// todo : tun_ip might not end with /32 /24...
	if err := exec.Command("ip", "route", "add", "default", "via", tun_ip[0:len(tun_ip)-3], "table", strconv.Itoa(ip_rule_table_idx)).Run(); err != nil {
		color.Red(err.Error() + "- Fail to add ip route: " + "default via " + tun_ip[0:len(tun_ip)-3] + " table " + strconv.Itoa(ip_rule_table_idx))
		os.Exit(1)
	}

	return ifce, nil
}

func main() {
	var ip0 string
	var ip1 string
	var eth0 string
	var eth1 string

	app := &cli.App{
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "ip0",
				Value:       "10.0.1.11/32",
				Usage:       "ip for tun0",
				Destination: &ip0,
			},
			&cli.StringFlag{
				Name:        "ip1",
				Value:       "10.0.3.11/32",
				Usage:       "ip for tun1",
				Destination: &ip1,
			},
			&cli.StringFlag{
				Name:        "eth0",
				Value:       "enp0s8",
				Usage:       "name of eth0, whose traffic will be redirected to tun0",
				Destination: &eth0,
			},
			&cli.StringFlag{
				Name:        "eth1",
				Value:       "enp0s9",
				Usage:       "name of eth1, whose traffic will be redirected to tun1",
				Destination: &eth1,
			},
			&cli.BoolFlag{
				Name:        "sniff",
				Usage:       "show packet info",
				Destination: &sniff,
			},
		},
		Action: func(c *cli.Context) error {
			fmt.Println("ip for tun0", ip0)
			fmt.Println("ip for tun1", ip1)
			fmt.Println("devname of ingress ", eth0)
			fmt.Println("devname of egress  ", eth1)
			if ip0 == ip1 {
				color.Red("ip0 and ip1 should be different!")
				return errors.New("Error : ip0 == ip1")
			}
			if eth0 == eth1 {
				color.Red("ingress and egress should be different!")
				return errors.New("Error : eth0 == eth1")
			}

			// check ip
			// check dev name
			// check dev exist
			// check sudo
			return nil
		},
	}

	// parse args
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

	// enable ip forwarding
	if err := exec.Command("sysctl", "net.ipv4.ip_forward=1").Run(); err != nil {
		color.Red(err.Error() + "- Fail to enable ip forwarding")
		os.Exit(1)
	}

	// eth0 -> tun0
	ifce, err := rediect_traffic_to_tun(eth0, "tun0", ip0)
	if err != nil {
		color.Red(err.Error() + "- Fail to rediect traffic to tun0")
		os.Exit(1)
	}
	// eth1 -> tun1
	ifce2, err := rediect_traffic_to_tun(eth1, "tun1", ip1)
	if err != nil {
		color.Red(err.Error() + "- Fail to rediect traffic to tun1")
		os.Exit(1)
	}

	// tun0 <-> tun1
	color_up := color.New(color.FgGreen)
	color_down := color.New(color.FgCyan)
	go tun_ferry(ifce, ifce2, color_up)
	go tun_ferry(ifce2, ifce, color_down)

	// wait till done
	sig := make(chan os.Signal, 3)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGABRT, syscall.SIGHUP)
	<-sig
}

// ! method 1: ifceRead and ifceWrite are too slow
// read from tun, and write to c
func ifceRead(ifce *water.Interface, c chan []byte) {
	packet := make([]byte, 1500)
	for {
		size, err := ifce.Read(packet)
		if err != nil {
			color.Red(err.Error() + "- Fail to read from tun : " + ifce.Name())
			os.Exit(1)
		}
		if waterutil.IsIPv6(packet[:size]) {
			continue
		}
		c <- packet[:size]
	}
}

// read from c, and write to tun
func ifceWrite(c chan []byte, ifce *water.Interface) {
	for {
		select {
		case data := <-c:
			_, err := ifce.Write(data)
			if err != nil {
				color.Red(err.Error() + "- Fail to write to tun : " + ifce.Name())
				os.Exit(1)
			}
		}
	}
}

// method 2
func tun_ferry(ifce *water.Interface, ifce2 *water.Interface, cl *color.Color) {
	packet := make([]byte, 1500)
	// 这里考虑用一个栈空间的queue局部储存？
	for {
		// 能否改造成超时必然返回的？
		size, err := ifce.Read(packet)

		if waterutil.IsIPv6(packet[:size]) {
			continue
		}

		if sniff {
			cl.Println("read " + strconv.Itoa(size) + " bytes" + " from tun : " + ifce.Name())
			cl.Println(" -> from [" + waterutil.IPv4Source(packet[:size]).String() + "] to [" + waterutil.IPv4Destination(packet[:size]).String() + "]")
		}

		if err != nil {
			color.Red(err.Error() + "- Fail to read from tun : " + ifce.Name())
			os.Exit(1)
		}
		// 根据链路文件判断机会,直到全部发送?
		if delivery_opportunity() {
			size_written, err := ifce2.Write(packet[:size])
			if sniff {
				cl.Println("write " + strconv.Itoa(size) + " bytes" + " to tun : " + ifce2.Name())
			}

			if err != nil {
				color.Red(err.Error() + "- Fail to write to tun : " + ifce2.Name())
				os.Exit(1)
			}
			if size_written != size {
				color.Red("short write to tun : " + ifce2.Name())
				os.Exit(1)
			}
		}
	}
}

func delivery_opportunity() bool {
	// todo : add delivery opportunity
	t := time.Now()
	if t != time.Now() {
		return true
	}
	return true
}

// method 3, might be better ?
// shaRed queue/deque of packet + mutex lock
// 2 goroutine : one for read, one for write
// 本质与channel并无不同
func read_and_queue(ifce *water.Interface) {
	packet := make([]byte, 1500)
	for {
		size, err := ifce.Read(packet)
		if err != nil {
			color.Red(err.Error())
			os.Exit(1)
		}
		if waterutil.IsIPv6(packet[:size]) {
			continue
		}

		//queue
		// lock()
		// queue(packet[:size])
		// unlock()
	}
}

func readqueue_and_send(ifce *water.Interface) {
	for {
		//dequeue
		// lock()
		// data := dequeue()
		// unlock()

		// wait_until_oppotunity()

		// _, err := ifce.Write(data)
		// if err != nil {
		// color.Red(err.Error())
		// os.Exit(1)
		// }
	}
}
