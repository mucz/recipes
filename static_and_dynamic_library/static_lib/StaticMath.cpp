#include "StaticMath.h"

StaticMath::StaticMath(void){
    std::cout<<"Initialise"<<std::endl;
}
StaticMath::~StaticMath(void){
    std::cout<<"Object dies"<<std::endl;
}
double StaticMath::add(double a, double b){
    return a+b;
}
double StaticMath::sub(double a, double b){
    return a-b;
}

double StaticMath::mul(double a, double b){
    return a*b;
}

double StaticMath::div(double a, double b){
    return a/b;
}

 
void StaticMath::print(){
    std::cout<<"print from StaticMath"<<std::endl;
}
