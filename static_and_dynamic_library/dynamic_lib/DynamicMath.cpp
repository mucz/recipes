#include "DynamicMath.h"

DynamicMath::DynamicMath(void){
    std::cout<<"Initialise"<<std::endl;
}
DynamicMath::~DynamicMath(void){
    std::cout<<"Object dies"<<std::endl;
}
double DynamicMath::add(double a, double b){
    return a+b;
}
double DynamicMath::sub(double a, double b){
    return a-b;
}

double DynamicMath::mul(double a, double b){
    return a*b;
}

double DynamicMath::div(double a, double b){
    return a/b;
}

 
void DynamicMath::print(){
    std::cout<<"print from DynamicMath"<<std::endl;
}
